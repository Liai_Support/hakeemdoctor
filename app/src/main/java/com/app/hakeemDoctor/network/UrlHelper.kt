package com.app.hakeemDoctor.network

import com.app.hakeemDoctor.BuildConfig
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.app.AppController
import com.google.android.gms.maps.model.LatLng


object UrlHelper {
    private const val BASE = BuildConfig.Base
    private const val BASE_URL = BASE + "provider/"
    const val SOCKETURL = BuildConfig.SocketURL

    const val CREATE_ACCOUNT = BASE_URL + "createAccount"
    const val LOGIN = BASE_URL + "login"

    const val CHATDETAILS = BASE_URL + "chatDetailList"
    const val GETTESTLIST = BASE_URL +"getTestNames"
    const val GETLABLIST = BASE_URL + "labList"


    const val GET_OTP = BASE_URL + "getOTP"
    const val CHECK_OTP = BASE_URL + "checkOTP"
    const val ENTER_NEW_PASSWORD = BASE_URL + "enterNewPassword"

    const val CHECK_SOCIAL_LOGIN = BASE_URL + "checkSocialLogin"

    const val UPDATE_DEVICE_TOKEN = BASE_URL + "updateDeviceToken"
    const val APPSETTINGS = BASE_URL + "appSetting"
    const val HOMEDASHBOARD = BASE_URL + "homeDashboard"
    const val GOOGLE_LOGIN = BASE_URL + "googleLogin"
    const val GET_PROFILE = BASE_URL + "getMyProfileDetails"
    const val UPDATE_PROFILE = BASE_URL + "updateProviderProfile"

    const val LIST_OF_BOOKINGS = BASE_URL + "getListOfBookings"
    const val ACCEPT_BOOKING = BASE_URL + "acceptBooking"
    const val CANCEL_BOOKING = BASE_URL + "cancelBooking"

    const val TRACK_PROVIDER = BASE_URL + "trackProvider"
    const val STATRVISIT = BASE_URL + "updateBookingStartVisitStatus"
    const val COMPLETEVISIT = BASE_URL + "updateBookingCompleteVisitStatus"
    const val SHOWREVIEW = BASE_URL + "showMyReviews"
    const val CONSULTATION_HISTORY = BASE_URL + "getMyConsultationHistory"
    const val UPDATEPAYMENTCONFIRMATION = BASE_URL + "updateBookingPaymentVerify"
    const val WALLETHISTORY = BASE_URL + "walletHistory"
    const val WALLETHISTORYDATE = BASE_URL + "walletHistoryDate"
    const val PROVIDERUPDATELATLNG = BASE_URL + "providerUpdateLatlong"

    const val REJECTREQUEST = BASE_URL + "rejectIncomingBooking"
    const val ACCEPTREQUEST = BASE_URL + "acceptIncomingBooking"
    const val UPDATEREACHEDDESTINATION = BASE_URL + "updateBookingReachedDestinationStatus"
    const val PAYADMIN = BASE_URL + "payAdmin"
    const val ADDNOTES = BASE_URL + "addNotes"
    const val VIEWLABREPORT = BASE_URL + "getLabTestReports"


    const val CREATECALL = BASE_URL + "createCall"
    const val ACCEPTCALL = BASE_URL + "acceptCall"
    const val ENDCALL = BASE_URL + "endCall"

    const val GETMYNOTIFICATION = BASE_URL + "getMyNotifications"
    const val DELETEMYNOTIFICATION = BASE_URL + "deleteMyNotifications"

    //socket
    const val UPDATEPROVIDERLOCATION = "updateProviderLatLon"
    const val SENDMESSAGE = "sendmessage"
    const val GET_ONLINE = "get_online"
    const val RECIVE_MESSAGE = "recievemessage"


    const val GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/"
    const val GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?"
    const val GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?"
    const val GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?"
    const val GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?"
    const val GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?"

    const val VERSION_CHECK = BASE_URL +"versionCheck"

    fun getAddress(
        latitude: Double,
        longitude: Double
    ): String? {
        val lat = latitude.toString()
        val lngg = longitude.toString()
        return (GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + lat + ","
                + lngg + "&sensor=true&key=" + AppController.getInstance().resources.getString(
            R.string.map_api_key
        ))
    }

    fun getDirectionsUrl(origin: LatLng, dest: LatLng): String {
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude
        val sensor = "sensor=false"
        val mode = "mode=driving"
        val parameters = "$str_origin&$str_dest&$sensor&$mode"
        val output = "json"
        return "$GOOGLE_API_DIRECTION_BASE_URL$parameters&key=" + AppController.getInstance().resources.getString(
            R.string.map_api_key
        )
    }
}