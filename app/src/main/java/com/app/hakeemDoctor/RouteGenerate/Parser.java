package com.app.hakeemDoctor.RouteGenerate;

import java.util.List;

//. by Haseem Saheed
public interface Parser {
    List<Route> parse() throws RouteException;
}