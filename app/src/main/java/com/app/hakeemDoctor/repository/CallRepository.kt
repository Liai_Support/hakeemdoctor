package com.app.hakeemDoctor.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemDoctor.interfaces.ApiResponseCallback
import com.app.hakeemDoctor.models.ChatResponse
import com.app.hakeemDoctor.models.CommonResponse
import com.app.hakeemDoctor.models.CreateCallResponseModel
import com.app.hakeemDoctor.network.Api
import com.app.hakeemDoctor.network.ApiInput

import com.google.gson.Gson
import org.json.JSONObject

class CallRepository private constructor() {

    companion object {

        var callRepository: CallRepository? = null

        fun getInstance(): CallRepository {
            if (callRepository == null) {
                callRepository = CallRepository()
            }
            return callRepository as CallRepository
        }
    }

    fun createCall(apiInput: ApiInput): LiveData<CreateCallResponseModel>? {

        var responseModel: MutableLiveData<CreateCallResponseModel>? = MutableLiveData()

        Api.postMethod(apiInput, object : ApiResponseCallback {

            override fun setResponseSuccess(jsonObject: JSONObject) {
                var gson = Gson()
                var response: CreateCallResponseModel =
                    gson.fromJson(jsonObject.toString(), CreateCallResponseModel::class.java)
                responseModel?.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CreateCallResponseModel()
                response.error = "true"
                response.message = error
                responseModel?.value = response

            }
        })
        return responseModel
    }

    fun endCall(apiInput: ApiInput): LiveData<CommonResponse>? {

        var responseModel: MutableLiveData<CommonResponse>? = MutableLiveData()

        Api.postMethod(apiInput, object : ApiResponseCallback {

            override fun setResponseSuccess(jsonObject: JSONObject) {
                var gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                responseModel?.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                responseModel?.value = response

            }
        })
        return responseModel
    }

    fun acceptCall(apiInput: ApiInput): LiveData<CommonResponse>? {

        var responseModel: MutableLiveData<CommonResponse>? = MutableLiveData()

        Api.postMethod(apiInput, object : ApiResponseCallback {

            override fun setResponseSuccess(jsonObject: JSONObject) {
                var gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                responseModel?.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                responseModel?.value = response

            }
        })
        return responseModel
    }


    fun getChatDetails(apiParams: ApiInput): LiveData<ChatResponse> {
        var commonResponseModel: MutableLiveData<ChatResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ChatResponse = gson.fromJson(jsonObject.toString(), ChatResponse::class.java)
                commonResponseModel.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ChatResponse()
                response.message = error
                response.error = "true"
                commonResponseModel.value = response
            }

        })

        return commonResponseModel
    }


}