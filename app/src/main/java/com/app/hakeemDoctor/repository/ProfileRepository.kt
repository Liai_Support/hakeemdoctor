package com.app.hakeemDoctor.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemDoctor.interfaces.ApiResponseCallback
import com.app.hakeemDoctor.models.CommonResponse
import com.app.hakeemDoctor.models.ProfileDetailsResponse
import com.app.hakeemDoctor.network.Api
import com.app.hakeemDoctor.network.ApiInput
import com.google.gson.Gson
import org.json.JSONObject

class ProfileRepository private constructor() {

    companion object {
        private var repository: ProfileRepository? = null

        fun getInstance(): ProfileRepository {
            if (repository == null) {
                repository =
                    ProfileRepository()
            }
            return repository as ProfileRepository
        }
    }


    fun getProfileDetails(input: ApiInput): LiveData<ProfileDetailsResponse>? {

        val apiResponse: MutableLiveData<ProfileDetailsResponse> = MutableLiveData()

        Api.getMethod(input, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ProfileDetailsResponse =
                    gson.fromJson(jsonObject.toString(), ProfileDetailsResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    ProfileDetailsResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun updateProfile(input: ApiInput): LiveData<CommonResponse>? {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


}