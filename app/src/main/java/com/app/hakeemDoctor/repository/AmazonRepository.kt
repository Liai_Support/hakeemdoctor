package com.app.hakeemDoctor.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.amazonaws.SDKGlobalConfiguration
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.*
import com.amazonaws.regions.Region
import com.amazonaws.services.s3.AmazonS3Client
import com.app.hakeemDoctor.models.CommonResponse
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.UiUtils
import com.facebook.FacebookSdk.getApplicationContext
import org.json.JSONObject
import java.io.File


class AmazonRepository private constructor() {

    companion object {
        private var repository: AmazonRepository? = null

        fun getInstance(): AmazonRepository {
            if (repository == null) {
                repository =
                    AmazonRepository()
            }
            return repository as AmazonRepository
        }
    }


    fun uploadImageToAWS(
        context: Context,
        file: File
    ): LiveData<CommonResponse> {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        var fileName = file.name

        val s3Client: AmazonS3Client = getClient(context.applicationContext)

        TransferNetworkLossHandler.getInstance(context)
        val tuOptions = TransferUtilityOptions()
        tuOptions.transferThreadPoolSize = 10


        val transferUtility = TransferUtility.builder()
            .s3Client(s3Client)
            .context(getApplicationContext())
            .transferUtilityOptions(tuOptions)
            .build();

        val transferObserver: TransferObserver =
            transferUtility.upload(Constants.AWS.BUCKET_NAME, fileName, file)
        val finalFileName: String = fileName

        transferObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(
                id: Int,
                state: TransferState
            ) {

                if (state.toString().equals("COMPLETED", ignoreCase = true)) {


                    val url: String = Constants.AWS.BASE_S3_URL.toString() + finalFileName
                    UiUtils.showLog("Image Url ", url)
                    var jsonObject = JSONObject()
                    jsonObject.put(Constants.ApiKeys.IMAGEURL, url)

                    var commonResponse =
                        CommonResponse()
                    commonResponse.error = false
                    commonResponse.message = url
                    apiResponse.postValue(commonResponse)
                }
            }

            override fun onProgressChanged(
                id: Int,
                bytesCurrent: Long,
                bytesTotal: Long
            ) {
                try {
                    val percentage = (bytesCurrent / bytesTotal * 100).toInt()

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(id: Int, ex: Exception) {
                UiUtils.showLog("Image Url ", ex.message.toString())
                var commonResponse = CommonResponse()
                commonResponse.error = true
                commonResponse.message = ex.message
                apiResponse.postValue(commonResponse)
            }
        })
        return apiResponse
    }

    // get auth details for aws
    private fun getClient(context: Context): AmazonS3Client {
        System.setProperty(
            SDKGlobalConfiguration.ENFORCE_S3_SIGV4_SYSTEM_PROPERTY,
            "true"
        )

//        System.setProperty(
//            SDKGlobalConfiguration.ENFORCE_S3_SIGV4_SYSTEM_PROPERTY,
//            "true"
//        )
//
        val credentialsProvider =
            CognitoCachingCredentialsProvider(
                context.applicationContext, Constants.AWS.POOL_ID, Constants.AWS.REGION
            )
        return AmazonS3Client(
            credentialsProvider, Region.getRegion(Constants.AWS.REGION)
        )
    }
}