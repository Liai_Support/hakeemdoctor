package com.app.hakeemDoctor.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemDoctor.interfaces.ApiResponseCallback
import com.app.hakeemDoctor.models.AppSettingsResponse
import com.app.hakeemDoctor.models.CommonResponse
import com.app.hakeemDoctor.models.HomeDashBoardResponse
import com.app.hakeemDoctor.models.VersionCheckResponse
import com.app.hakeemDoctor.network.Api
import com.app.hakeemDoctor.network.ApiInput
import com.google.gson.Gson
import org.json.JSONObject

class HomeRepository private constructor() {

    companion object {
        private var repository: HomeRepository? = null

        fun getInstance(): HomeRepository {
            if (repository == null) {
                repository =
                    HomeRepository()
            }
            return repository as HomeRepository
        }
    }


    fun getHomeDetails(input: ApiInput): LiveData<HomeDashBoardResponse>? {

        val apiResponse: MutableLiveData<HomeDashBoardResponse> = MutableLiveData()

        Api.getMethod(input, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: HomeDashBoardResponse =
                    gson.fromJson(jsonObject.toString(), HomeDashBoardResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    HomeDashBoardResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun updateDeviceToken(apiParams: ApiInput) {

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {}
            override fun setErrorResponse(error: String) {}
        })

    }

    fun getAppSettings(apiParams: ApiInput): MutableLiveData<AppSettingsResponse>? {

        val apiResponse: MutableLiveData<AppSettingsResponse> = MutableLiveData()

        Api.getMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AppSettingsResponse =
                    gson.fromJson(jsonObject.toString(), AppSettingsResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AppSettingsResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateLocation(apiParams: ApiInput) {

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {

            }

            override fun setErrorResponse(error: String) {

            }
        })

    }

    fun callBookigResponse(apiParams: ApiInput): LiveData<CommonResponse> {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }
    fun getcheckversion(input: ApiInput): LiveData<VersionCheckResponse> {

        val apiResponse: MutableLiveData<VersionCheckResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: VersionCheckResponse =
                    gson.fromJson(jsonObject.toString(), VersionCheckResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = VersionCheckResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

}