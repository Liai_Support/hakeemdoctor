package com.app.hakeemDoctor.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemDoctor.interfaces.ApiResponseCallback
import com.app.hakeemDoctor.models.AutoCompleteResponse
import com.app.hakeemDoctor.network.Api
import com.app.hakeemDoctor.network.ApiInput
import com.google.gson.Gson
import org.json.JSONObject

class MapRepository private constructor() {

    companion object {
        private var repository: MapRepository? = null

        fun getInstance(): MapRepository {
            if (repository == null) {
                repository =
                    MapRepository()
            }
            return repository as MapRepository
        }
    }


    fun getAddress(input: ApiInput): LiveData<AutoCompleteResponse>? {

        val apiResponse: MutableLiveData<AutoCompleteResponse> = MutableLiveData()

        Api.getMethod(input, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AutoCompleteResponse =
                    gson.fromJson(jsonObject.toString(), AutoCompleteResponse::class.java)
                response.error = false
                response.errorMessage = ""
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AutoCompleteResponse()
                response.error = true
                response.errorMessage = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


}