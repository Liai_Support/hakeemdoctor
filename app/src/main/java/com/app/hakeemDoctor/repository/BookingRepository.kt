package com.app.hakeemDoctor.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.RouteGenerate.*
import com.app.hakeemDoctor.interfaces.ApiResponseCallback
import com.app.hakeemDoctor.models.*
import com.app.hakeemDoctor.network.Api
import com.app.hakeemDoctor.network.ApiInput
import com.app.hakeemDoctor.utils.SharedHelper
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import org.json.JSONObject

class BookingRepository private constructor() {

    companion object {
        private var repository: BookingRepository? = null

        fun getInstance(): BookingRepository {
            if (repository == null) {
                repository =
                    BookingRepository()
            }
            return repository as BookingRepository
        }
    }


    fun getBookingList(apiParams: ApiInput): LiveData<ListOfBookingResponse>? {
        val apiResponse: MutableLiveData<ListOfBookingResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ListOfBookingResponse =
                    gson.fromJson(jsonObject.toString(), ListOfBookingResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    ListOfBookingResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun acceptBooking(apiParams: ApiInput): LiveData<CommonResponse>? {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun gettestlistDetails(input: ApiInput): LiveData<GetTestListResponse>? {

        val apiResponse: MutableLiveData<GetTestListResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetTestListResponse =
                    gson.fromJson(jsonObject.toString(), GetTestListResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetTestListResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getlablistrepo(input: ApiInput): LiveData<GetLabListResponseModel>? {

        val apiResponse: MutableLiveData<GetLabListResponseModel> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetLabListResponseModel =
                        gson.fromJson(jsonObject.toString(), GetLabListResponseModel::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetLabListResponseModel()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun cancelBooking(apiParams: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getBookingDetails(apiParams: ApiInput): LiveData<ListOfBookingResponse>? {

        val apiResponse: MutableLiveData<ListOfBookingResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ListOfBookingResponse =
                    gson.fromJson(jsonObject.toString(), ListOfBookingResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    ListOfBookingResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getRouteDetails(
        context: Context,
        srcLatLng: LatLng?,
        desLatLng: LatLng?
    ): LiveData<Route>? {
        val liveData: MutableLiveData<Route> =
            MutableLiveData()
        val routing: Routing = Routing.Builder()
            .travelMode(AbstractRouting.TravelMode.DRIVING)
            .language(SharedHelper(context).language)
            .key(context.resources.getString(R.string.map_api_key))
            .withListener(object : RoutingListener {
                override fun onRoutingFailure(e: RouteException?) {
                    Log.d("respose", e.toString())
                }

                override fun onRoutingStart() {}
                override fun onRoutingSuccess(
                    route: List<Route>,
                    shortestRouteIndex: Int
                ) {
                    liveData.setValue(route[shortestRouteIndex])
                }

                override fun onRoutingCancelled() {}
            })
            .waypoints(srcLatLng, desLatLng)
            .build()
        routing.execute()
        return liveData
    }

    fun updateBookingStartVisitStatus(apiParams: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateBookingCompleteVisitStatus(apiParams: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getRating(apiParams: ApiInput): LiveData<ReviewResponse>? {

        val apiResponse: MutableLiveData<ReviewResponse> = MutableLiveData()

        Api.getMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ReviewResponse =
                    gson.fromJson(jsonObject.toString(), ReviewResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    ReviewResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getConsultationHistory(apiParams: ApiInput): LiveData<ConsultationHistoryResponse>? {

        val apiResponse: MutableLiveData<ConsultationHistoryResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ConsultationHistoryResponse =
                    gson.fromJson(jsonObject.toString(), ConsultationHistoryResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    ConsultationHistoryResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun confirmPaymentVerification(apiParams: ApiInput): LiveData<CommonResponse>? {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getTransactionList(apiParams: ApiInput): LiveData<TransactionHistoryResponse>? {


        val apiResponse: MutableLiveData<TransactionHistoryResponse> = MutableLiveData()

        Api.getMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: TransactionHistoryResponse =
                    gson.fromJson(jsonObject.toString(), TransactionHistoryResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    TransactionHistoryResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getWalletHistory(apiParams: ApiInput): LiveData<StatisResponse> {

        val apiResponse: MutableLiveData<StatisResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: StatisResponse =
                    gson.fromJson(jsonObject.toString(), StatisResponse::class.java)
                Log.d("fvfdb",""+jsonObject.toString());
                apiResponse.value = response
                Log.d("fvfdhhvb",""+apiResponse.value);

            }

            override fun setErrorResponse(error: String) {
                var response =
                    StatisResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun updateBookingDestination(apiParams: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun payAdmin(apiParams: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

   fun addNotes(apiParams: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object :
            ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response =
                    CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getNotificationData(input: ApiInput): LiveData<NotificationResponse> {

        val apiResponse: MutableLiveData<NotificationResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: NotificationResponse =
                    gson.fromJson(jsonObject.toString(), NotificationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = NotificationResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun clearNotification(apiParams: ApiInput): LiveData<CommonResponse>? {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun viewlabreport(input: ApiInput): LiveData<ViewLabReportResponse> {

        val apiResponse: MutableLiveData<ViewLabReportResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: ViewLabReportResponse =
                    gson.fromJson(jsonObject.toString(), ViewLabReportResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = ViewLabReportResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

}