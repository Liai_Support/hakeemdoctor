package com.app.hakeemDoctor.barchart.interfaces.dataprovider;


import com.app.hakeemDoctor.barchart.components.YAxis;
import com.app.hakeemDoctor.barchart.data.BarLineScatterCandleBubbleData;
import com.app.hakeemDoctor.barchart.utils.Transformer;

public interface BarLineScatterCandleBubbleDataProvider extends ChartInterface {

    Transformer getTransformer(YAxis.AxisDependency axis);
    boolean isInverted(YAxis.AxisDependency axis);
    
    float getLowestVisibleX();
    float getHighestVisibleX();

    BarLineScatterCandleBubbleData getData();
}
