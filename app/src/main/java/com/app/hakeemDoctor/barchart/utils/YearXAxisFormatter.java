package com.app.hakeemDoctor.barchart.utils;

import com.app.hakeemDoctor.barchart.components.AxisBase;
import com.app.hakeemDoctor.barchart.formatter.ValueFormatter;

import java.text.DateFormatSymbols;
import java.util.Locale;


@SuppressWarnings("unused")
public class YearXAxisFormatter extends ValueFormatter {

    private DateFormatSymbols dateFormatSymbols;

    public String[] mMonths;
    private String[] values;
    private String TAG = YearXAxisFormatter.class.getSimpleName();

    public YearXAxisFormatter(String localeMonth) {

        dateFormatSymbols = new DateFormatSymbols(new Locale(localeMonth));
        mMonths = new String[]{"S", "M", "T", "W", "TH", "F", "SA"};

        // take parameters to change behavior of formatter

    }


    @Override
    public String getAxisLabel(float value, AxisBase axis) {

        float percent = value / axis.mAxisRange;
        String returnValue = String.valueOf(value);
        try {
             returnValue = mMonths[(int) (mMonths.length * percent)];
        } catch (ArrayIndexOutOfBoundsException e) {

        }
        return returnValue;
    }


}
