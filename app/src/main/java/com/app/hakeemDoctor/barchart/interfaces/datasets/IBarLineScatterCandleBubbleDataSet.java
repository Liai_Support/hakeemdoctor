package com.app.hakeemDoctor.barchart.interfaces.datasets;


import com.app.hakeemDoctor.barchart.data.Entry;

public interface IBarLineScatterCandleBubbleDataSet<T extends Entry> extends IDataSet<T> {

    /**
     * Returns the color that is used for drawing the highlight indicators.
     *
     * @return
     */
    int getHighLightColor();
}
