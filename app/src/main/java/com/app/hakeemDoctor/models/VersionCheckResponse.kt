package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class VersionCheckResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null


    @SerializedName("data")
    var data: VersionCheck? = null

}

class VersionCheck : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var version: VersionData? = null

}

class VersionData : Serializable {

    @SerializedName("android_patient")
    var android_patient: String? = null


}