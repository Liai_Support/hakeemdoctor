package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ChatResponse : Serializable {

    @SerializedName("error")
    var error = ""
    @SerializedName("message")
    var message = ""
    @SerializedName("data")
    var data = ArrayList<ChatData>()


}

class ChatData : Serializable {

    @SerializedName("id")
    var id = 0
    @SerializedName("senderID")
    var senderID = 0
    @SerializedName("receiverID")
    var receiverID = ""
    @SerializedName("senderType")
    var senderType = ""
    @SerializedName("content")
    var content = ""
    @SerializedName("content_type")
    var contentType = ""
    @SerializedName("time")
    var time: String? = ""

}