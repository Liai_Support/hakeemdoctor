package com.app.hakeemDoctor.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.withContext
import java.io.Serializable



class ConsultationHistoryResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<ConsultationHistoryData>? = null


}

class ConsultationHistoryData : Serializable, Parcelable {

    @SerializedName("bookingId")
    var bookingId: Int? = null

    @SerializedName("bookingIds")
    var bookingIds: String? = null


    @SerializedName("booking_type")
    var booking_type: String? = null

    @SerializedName("is_ins")
    var is_ins: Int? = null

    @SerializedName("ins_status")
    var ins_status: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("bookingDate")
    var bookingDate: String? = null

    @SerializedName("bookingTime")
    var bookingTime: String? = null

    @SerializedName("bookingPeriod")
    var bookingPeriod: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("bookingLatitude")
    var bookingLatitude: String? = null

    @SerializedName("bookingLongitude")
    var bookingLongitude: String? = null

    @SerializedName("isFeatureBooking")
    var isFeatureBooking: String? = null

    @SerializedName("patientType")
    var patientType: String? = null

    @SerializedName("patientName")
    var patientName: String? = null

    @SerializedName("patientAge")
    var patientAge: String? = null

    @SerializedName("patientGender")
    var patientGender: String? = null

    @SerializedName("previousIssue")
    var previousIssue: String? = null

    @SerializedName("specialityName")
    var specialityName: String? = null

    @SerializedName("providerName")
    var providerName: String? = null

    @SerializedName("providerType")
    var providerType: String? = null

    @SerializedName("education")
    var education: String? = null

    @SerializedName("expierence")
    var expierence: String? = null

    @SerializedName("providerStatus")
    var providerStatus: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("noteContent")
    var noteContent: String? = null

    @SerializedName("fee")
    var fee: String? = null

    @SerializedName("extraFee")
    var extraFee: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("DOB")
    var DOB: String? = null

    @SerializedName("test_to_take")
    var test_to_take: ArrayList<TestData>? = null

    @SerializedName("suggested_labs")
    var suggested_labs: ArrayList<SlabData>? = null

    @SerializedName("medicine")
    var medicine: ArrayList<MedData>? = null

    constructor(parcel: Parcel) : this() {
        bookingId = parcel.readValue(Int::class.java.classLoader) as? Int
        bookingIds = parcel.readString()
       // providerId = parcel.readValue(Int::class.java.classLoader) as? Int
        status = parcel.readString()
        bookingDate = parcel.readString()!!
        bookingTime = parcel.readString()
        bookingPeriod = parcel.readString()
        location = parcel.readString()
        bookingLatitude = parcel.readString()
        bookingLongitude = parcel.readString()
        isFeatureBooking = parcel.readString()
        patientType = parcel.readString()
        patientName = parcel.readString()
        patientAge = parcel.readString()
        patientGender = parcel.readString()
        previousIssue = parcel.readString()
        specialityName = parcel.readString()
        providerName = parcel.readString()
        providerType = parcel.readString()
        education = parcel.readString()
        expierence = parcel.readString()
        providerStatus = parcel.readString()
        profilePic = parcel.readString()
        noteContent = parcel.readString()

        fee = parcel.readString()
        extraFee = parcel.readString()
//        isVirtualBooking = parcel.readValue(Int::class.java.classLoader) as? Int
//        averageRating = parcel.readString()
//        chat = parcel.readString()
//        stream = parcel.readString()
        // medicine = parcel.readValue(java.util.ArrayList::class.java.classLoader) as? ArrayList<MedData>
        // test_to_take = parcel.readValue(java.util.ArrayList::class.java.classLoader) as? ArrayList<TestData>
        // suggested_labs = parcel.readValue(java.util.ArrayList::class.java.classLoader) as? ArrayList<SlabData>
    }

    constructor()

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(bookingId)
        //parcel.writeValue(providerId)
        parcel.writeString(bookingIds)
        parcel.writeString(status)
        parcel.writeString(bookingDate)
        parcel.writeString(bookingTime)
        parcel.writeString(bookingPeriod)
        parcel.writeString(location)
        parcel.writeString(bookingLatitude)
        parcel.writeString(bookingLongitude)
        parcel.writeString(isFeatureBooking)
        parcel.writeString(patientType)
        parcel.writeString(patientName)
        parcel.writeString(patientAge)
        parcel.writeString(patientGender)
        parcel.writeString(previousIssue)
        parcel.writeString(specialityName)
        parcel.writeString(providerName)
        parcel.writeString(providerType)
        parcel.writeString(education)
        parcel.writeString(expierence)
        parcel.writeString(providerStatus)
        parcel.writeString(profilePic)
        parcel.writeString(noteContent)

        parcel.writeString(fee)
        parcel.writeString(extraFee)
        //parcel.writeValue(isVirtualBooking)
        //parcel.writeString(averageRating)
        //parcel.writeString(chat)
        //parcel.writeString(stream)
        //  parcel.writeArray(arrayOf(test_to_take))
        //  parcel.writeArray(arrayOf(medicine))
        //  parcel.writeArray(arrayOf(suggested_labs))
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ConsultationHistoryData> {
        override fun createFromParcel(parcel: Parcel): ConsultationHistoryData {
            return ConsultationHistoryData(parcel)
        }

        override fun newArray(size: Int): Array<ConsultationHistoryData?> {
            return arrayOfNulls(size)
        }
    }
}



class TestData : Serializable {
    @SerializedName("Test_name")
    var Test_name: String? = null

    @SerializedName("Test_name_arabic")
    var Test_name_arabic: String? = null
}

class MedData : Serializable {
    @SerializedName("medicine")
   // var medicine: String? = null
 var medicine:ArrayList<String>? = null


    @SerializedName("medicine_desc")
    var medicine_desc: ArrayList<String>? = null
}

class SlabData : Serializable{
    @SerializedName("lab_name")
    var lab_name: String? = null

    @SerializedName("lab_name_ar")
    var lab_name_ar: String? = null
}
