package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProfileDetailsResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<ProfileDetails>? = null


}


class ProfileDetails : Serializable {


    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null


    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("providerType")
    var providerType: String? = null


    @SerializedName("profilePic")
    var profilePic: String? = null


    @SerializedName("gender")
    var gender: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("education")
    var education: String? = null

    @SerializedName("registrationCouncil")
    var registrationCouncil: String? = null

    @SerializedName("expierence")
    var expierence: String? = null

    @SerializedName("specialityName")
    var specialityName: String? = null

    @SerializedName("hospital")
    var hospital: String? = null


}