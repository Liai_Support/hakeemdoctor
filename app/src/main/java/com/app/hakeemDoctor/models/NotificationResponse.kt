package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class NotificationResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<NotificationListData>? = null


}

class NotificationData : Serializable {

    @SerializedName("notificationDetails")
    var data: ArrayList<NotificationListData>? = null

}

class NotificationListData : Serializable {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("notificationUserId")
    var notificationUserId: String? = null

    @SerializedName("userType")
    var userType: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("body")
    var body: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("userId")
    var userId: String? = null

    @SerializedName("bookingId")
    var bookingId: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null


}