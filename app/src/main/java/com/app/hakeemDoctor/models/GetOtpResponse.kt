package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetOtpResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: GetOtpData? = null


}


class GetOtpData : Serializable {


    @SerializedName("otp")
    var otp: String? = null

    @SerializedName("email")
    var email: String? = null


}