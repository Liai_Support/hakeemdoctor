package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ListOfBookingResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<BookingData>? = null


}

class BookingData : Serializable {




    @SerializedName("patientId")
    var patientId: Int? = null

    @SerializedName("countryCode")
    var countryCode: Int? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("distance")
    var distance: String? = null

    @SerializedName("bookingId")
    var bookingId: Int? = null

    @SerializedName("bookingIds")
    var bookingIds: String? = null

    @SerializedName("is_ins")
    var is_ins: Int? = null

    @SerializedName("ins_status")
    var ins_status: String? = null

    @SerializedName("status")
    var status: String? = null

   @SerializedName("reachedDestination")
    var reachedDestination: String? = null

    @SerializedName("bookingDate")
    var bookingDate: String? = null

    @SerializedName("bookingTime")
    var bookingTime: String? = null

    @SerializedName("bookingPeriod")
    var bookingPeriod: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("bookingLatitude")
    var bookingLatitude: String? = null

    @SerializedName("bookingLongitude")
    var bookingLongitude: String? = null

    @SerializedName("isFeatureBooking")
    var isFeatureBooking: Boolean? = null

    @SerializedName("patientType")
    var patientType: String? = null

    @SerializedName("patientName")
    var patientName: String? = null

    @SerializedName("patientAge")
    var patientAge: String? = null

    @SerializedName("patientGender")
    var patientGender: String? = null

    @SerializedName("previousIssue")
    var previousIssue: String? = null

    @SerializedName("specialityName")
    var specialityName: String? = null

    @SerializedName("providerName")
    var providerName: String? = null

    @SerializedName("providerType")
    var providerType: String? = null

    @SerializedName("education")
    var education: String? = null

    @SerializedName("expierence")
    var expierence: String? = null

    @SerializedName("providerStatus")
    var providerStatus: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("fee")
    var fee: String? = null

    @SerializedName("userLatitude")
    var userLatitude: String? = null

    @SerializedName("userLongitude")
    var userLongitude: String? = null

    @SerializedName("isVirtualBooking")
    var isVirtualBooking: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("DOB")
    var DOB: String? = null


    @SerializedName("booking_type")
    var booking_type: String? = null


    @SerializedName("noteContent")
    var noteContent: String? = null


}