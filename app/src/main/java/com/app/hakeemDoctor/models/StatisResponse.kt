package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class StatisResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: StatSticData? = null
}

class StatSticData : Serializable {

    @SerializedName("history")
    var history: ArrayList<StatData>? = null

}

class StatData : Serializable {

    @SerializedName("amount")
    var amount: Float? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    var day: String = ""

}