package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class HomeDashBoardResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: HomeBoardData? = null

}

class HomeBoardData : Serializable {

    @SerializedName("bannerImages")
    var bannerImages: ArrayList<BannerImageData>? = null
    @SerializedName("speciality")
    var speciality: ArrayList<SpecialityImageData>? = null
}

class BannerImageData : Serializable {

    @SerializedName("id")
    var id: Int? = null
    @SerializedName("image")
    var image: String? = null
    @SerializedName("status")
    var status: String? = null
    @SerializedName("createdAt")
    var createdAt: String? = null
    @SerializedName("updatedAt")
    var updatedAt: String? = null

}

class SpecialityImageData : Serializable {
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("image")
    var image: String? = null
    @SerializedName("status")
    var status: String? = null
    @SerializedName("createdAt")
    var createdAt: String? = null
    @SerializedName("updatedAt")
    var updatedAt: String? = null
}