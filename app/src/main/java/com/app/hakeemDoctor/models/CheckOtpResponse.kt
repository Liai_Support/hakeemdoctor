package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CheckOtpResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: CheckOtpData? = null


}


class CheckOtpData : Serializable {

    @SerializedName("token")
    var token: String? = null

}