package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

class GetLabListResponseModel: Serializable {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data:ArrayList<LabData>? = null
}

class LabData : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("lab_name")
    var lab_name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null

    @SerializedName("available_test")
    var available_test: ArrayList<AvailableTest>?  = null

    @SerializedName("matched_test")
    var matched_test: ArrayList<MatchedTest>?  = null

    @SerializedName("isseleted")
    var isseleted: Boolean? = false
}


class AvailableTest : Serializable {
    @SerializedName("test_name_id")
    var test_name_id: Int? = null

    @SerializedName("Test_name")
    var Test_name: String? = null

    @SerializedName("Test_name_arabic")
    var Test_name_arabic: String? = null
}

class MatchedTest : Serializable {
    @SerializedName("test_name_id")
    var test_name_id: Int? = null

    @SerializedName("Test_name")
    var Test_name: String? = null

    @SerializedName("Test_name_arabic")
    var Test_name_arabic: String? = null
}

