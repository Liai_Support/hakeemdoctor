package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SocialLoginResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: SocialLoginData? = null


}


class SocialLoginData : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("token")
    var token: String? = null

    @SerializedName("loginType")
    var loginType: String? = null

    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("userExists")
    var userExists: Boolean? = null


}