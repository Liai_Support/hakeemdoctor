package com.app.hakeemDoctor.models

data class CurrentBookingListJson(
    val bookingId: Int,
    val bookingIds:String,
    val isVirtualBooking:Int,
    val profilePic:String,
    val providerName:String,
    val expierence:String,
    val previousIssue:String,
    val fee:String,
    val bookingDate:String,
    val bookingTime:String,
    val booking_type:String,
    val status:String,
    val specialityName:String,
    val is_ins:Int,
    val isFeatureBooking:Boolean,
    val bookingPeriod:String,
    val email:String,
    val DOB:String,
    val location:String,
    val bookingLatitude:String,
    val bookingLongitude:String,
    val patientId:Int,
    val patientName:String,
    val ins_status:String,
    val patientAge:String,
    val mobileNumber:String,
    val countryCode:Int,
    val name:String


)

