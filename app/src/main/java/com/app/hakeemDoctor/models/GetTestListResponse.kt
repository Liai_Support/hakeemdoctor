package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

class GetTestListResponse : Serializable {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<TestDetails>? = null
    //  var data: Result1? = null
}

class Result1 : Serializable {

    @SerializedName("results")
    var testDetails: ArrayList<TestDetails>? = null
}


class TestDetails : Serializable {


    @SerializedName("id")
    var id: Int? = null

    @SerializedName("Test_name")
    var ename: String? = null

    @SerializedName("Test_name_arabic")
    var aname: String? = null

    @SerializedName("isseleted")
    var isseleted: Boolean? = false

}
