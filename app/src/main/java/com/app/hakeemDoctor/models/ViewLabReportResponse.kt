package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ViewLabReportResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<ReportData>? = null



}


class ReportData : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("test")
    var test: String? = null

    @SerializedName("test_name")
    var test_name: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    @SerializedName("report")
    var report: String? = null

    @SerializedName("bookingId")
    var bookingId: Int? = null

}