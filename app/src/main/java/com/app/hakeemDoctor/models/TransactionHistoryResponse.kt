package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TransactionHistoryResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: TransactionData? = null

}

class TransactionData : Serializable {

    @SerializedName("history")
    var history: ArrayList<TransactionListData>? = null

    @SerializedName("availableCredits")
    var availableCredits: String? = null

    @SerializedName("availableEarnings")
    var availableEarnings: String? = null


}

class TransactionListData : Serializable {

    @SerializedName("id")
    var id: Int? = null
    @SerializedName("providerId")
    var providerId: Int? = null
    @SerializedName("bookingId")
    var bookingId: Int? = null
    @SerializedName("transactionId")
    var transactionId: String? = null
    @SerializedName("status")
    var status: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    @SerializedName("walletAmount")
    var walletAmount: String? = null


}