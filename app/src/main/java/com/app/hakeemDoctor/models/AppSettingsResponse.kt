package com.app.hakeemDoctor.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AppSettingsResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null


    @SerializedName("data")
    var data: AppData? = null
}

class AppData : Serializable {

    @SerializedName("payed")
    var payed: ArrayList<PayedData>? = null

    @SerializedName("pending")
    var pending: ArrayList<PayedData>? = null


    @SerializedName("configurations")
    var configurations: ArrayList<Configurations>? = null

    @SerializedName("isDeleted")
    var isDeleted: String? = null


}

class PayedData() : Serializable, Parcelable {

    @SerializedName("bookingId")
    var bookingId: Int? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("bookingDate")
    var bookingDate: String? = null

    @SerializedName("bookingTime")
    var bookingTime: String? = null

    @SerializedName("bookingPeriod")
    var bookingPeriod: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("isFeatureBooking")
    var isFeatureBooking: String? = null

    @SerializedName("patientType")
    var patientType: String? = null

    @SerializedName("patientName")
    var patientName: String? = null

    @SerializedName("patientAge")
    var patientAge: String? = null

    @SerializedName("patientGender")
    var patientGender: String? = null

    @SerializedName("previousIssue")
    var previousIssue: String? = null

    @SerializedName("specialityName")
    var specialityName: String? = null

    @SerializedName("providerName")
    var providerName: String? = null

    @SerializedName("providerType")
    var providerType: String? = null

    @SerializedName("education")
    var education: String? = null

    @SerializedName("expierence")
    var expierence: String? = null

    @SerializedName("providerId")
    var providerId: Int? = null

    @SerializedName("providerStatus")
    var providerStatus: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("fee")
    var fee: String? = null

    @SerializedName("extraFee")
    var extraFee: String? = null

    @SerializedName("averageRating")
    var averageRating: String? = null

    @SerializedName("bookingLatitude")
    var bookingLatitude: String? = null

    @SerializedName("bookingLongitude")
    var bookingLongitude: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("DOB")
    var DOB: String? = null

    constructor(parcel: Parcel) : this() {
        bookingId = parcel.readValue(Int::class.java.classLoader) as? Int
        status = parcel.readString()
        bookingDate = parcel.readString()
        bookingTime = parcel.readString()
        bookingPeriod = parcel.readString()
        location = parcel.readString()
        isFeatureBooking = parcel.readString()
        patientType = parcel.readString()
        patientName = parcel.readString()
        patientAge = parcel.readString()
        patientGender = parcel.readString()
        previousIssue = parcel.readString()
        specialityName = parcel.readString()
        providerName = parcel.readString()
        providerType = parcel.readString()
        education = parcel.readString()
        expierence = parcel.readString()
        providerId = parcel.readValue(Int::class.java.classLoader) as? Int
        providerStatus = parcel.readString()
        profilePic = parcel.readString()
        email = parcel.readString()
        countryCode = parcel.readString()
        mobileNumber = parcel.readString()
        fee = parcel.readString()
        extraFee = parcel.readString()
        averageRating = parcel.readString()
        bookingLatitude = parcel.readString()
        bookingLongitude = parcel.readString()
        name = parcel.readString()
        DOB = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(bookingId)
        parcel.writeString(status)
        parcel.writeString(bookingDate)
        parcel.writeString(bookingTime)
        parcel.writeString(bookingPeriod)
        parcel.writeString(location)
        parcel.writeString(isFeatureBooking)
        parcel.writeString(patientType)
        parcel.writeString(patientName)
        parcel.writeString(patientAge)
        parcel.writeString(patientGender)
        parcel.writeString(previousIssue)
        parcel.writeString(specialityName)
        parcel.writeString(providerName)
        parcel.writeString(providerType)
        parcel.writeString(education)
        parcel.writeString(expierence)
        parcel.writeValue(providerId)
        parcel.writeString(providerStatus)
        parcel.writeString(profilePic)
        parcel.writeString(email)
        parcel.writeString(countryCode)
        parcel.writeString(mobileNumber)
        parcel.writeString(fee)
        parcel.writeString(extraFee)
        parcel.writeString(averageRating)
        parcel.writeString(bookingLatitude)
        parcel.writeString(bookingLongitude)
        parcel.writeString(name)
        parcel.writeString(DOB)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PayedData> {
        override fun createFromParcel(parcel: Parcel): PayedData {
            return PayedData(parcel)
        }

        override fun newArray(size: Int): Array<PayedData?> {
            return arrayOfNulls(size)
        }
    }

}

class Configurations : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("objkey")
    var objkey: String? = null

    @SerializedName("objvalue")
    var objvalue: String? = null

    @SerializedName("status")
    var status: String? = null

}

