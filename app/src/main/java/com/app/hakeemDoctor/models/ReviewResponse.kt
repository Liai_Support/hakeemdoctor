package com.app.hakeemDoctor.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ReviewResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<ReviewData>? = null


}


class ReviewData : Serializable {

    @SerializedName("name")
    var name: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("bookingId")
    var bookingId: Int? = null

    @SerializedName("review")
    var review: String? = null

    @SerializedName("rating")
    var rating: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

}
