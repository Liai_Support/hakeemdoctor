package com.app.hakeemDoctor.interfaces

interface DialogCallBack {
    fun onPositiveClick()
    fun onNegativeClick()
}