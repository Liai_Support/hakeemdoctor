package com.app.hakeemDoctor.interfaces

interface ExtraFeeCallBack {
    fun onPositiveClick(value: Int)
    fun onNegativeClick()
}