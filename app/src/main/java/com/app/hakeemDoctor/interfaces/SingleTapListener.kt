package com.app.hakeemDoctor.interfaces

interface SingleTapListener {
    fun singleTap()
}