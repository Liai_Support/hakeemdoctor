package com.app.hakeemDoctor.interfaces

interface OnClickListener {
    fun onClickItem(position: Int)
}
