package com.app.hakeemDoctor.utils

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.app.hakeemDoctor.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar


object UiUtils {

    fun loadImage(imageView: ImageView?, imageUrl: String?) {
        if (imageUrl == null || imageView == null) {
            return
        }

        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.splash_image)
                    .error(R.drawable.splash_image)
            )
            .into(imageView)

    }

    fun loadImage(imageView: ImageView?, imageUrl: String?, placeHolder: Drawable) {
        if (imageUrl == null || imageView == null) {
            return
        }

        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(
                RequestOptions()
                    .placeholder(placeHolder)
                    .error(placeHolder)
            )
            .into(imageView)

    }

    fun getColoredAndBoldString(context: Context, content: String): SpannableStringBuilder {

        val stringBuilder = SpannableStringBuilder()
        val spannableString = SpannableString(content)
        val styleSpan = StyleSpan(Typeface.BOLD)
        val colorSpan = ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorPrimary))
        spannableString.setSpan(styleSpan, 0, content.length, 0)
        spannableString.setSpan(colorSpan, 0, content.length, 0)
        stringBuilder.append(spannableString)

        return stringBuilder
    }

    fun getBoldUnderlineString(context: Context, content: String): SpannableStringBuilder {

        val stringBuilder = SpannableStringBuilder()
        val spannableString = SpannableString(content)
        val styleSpan = StyleSpan(Typeface.BOLD)
        val colorSpan = ForegroundColorSpan(ContextCompat.getColor(context, R.color.textcolor_1))
        spannableString.setSpan(styleSpan, 0, content.length, 0)
        spannableString.setSpan(colorSpan, 0, content.length, 0)
        spannableString.setSpan(UnderlineSpan(), 0, content.length, 0)
        stringBuilder.append(spannableString)

        return stringBuilder
    }

    fun getColoredSting(context: Context, content: String): SpannableStringBuilder {

        val stringBuilder = SpannableStringBuilder()
        val spannableString = SpannableString(content)
        val colorSpan = ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorPrimary))
        spannableString.setSpan(colorSpan, 0, content.length, 0)
        stringBuilder.append(spannableString)

        return stringBuilder
    }

//    fun getBoldString(context: Context, content: String): SpannableStringBuilder {
//
//        val stringBuilder = SpannableStringBuilder()
//        val spannableString = SpannableString(content)
//        val styleSpan = StyleSpan(Typeface.BOLD)
//        //        ForegroundColorSpan colorSpan = new ForegroundColorSpan(context.getResources().getColor(R.color.splash_bg));
//        spannableString.setSpan(styleSpan, 0, content.length, 0)
//        //        spannableString.setSpan(colorSpan, 0, content.length(), 0);
//        stringBuilder.append(spannableString)
//
//        return stringBuilder
//    }

    fun setRating(content: Context, rating: Float, starContent: LinearLayout, size: Int) {

        var imageView: ImageView? = null
        try {
            starContent.removeAllViewsInLayout()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        for (j in 1..5) {
            imageView = ImageView(content)
            val layoutParams = LinearLayout.LayoutParams(size, size)
            if (1 < j) {
                layoutParams.setMargins(5, 0, 0, 0)
            }
            imageView.layoutParams = layoutParams
            val s = Math.round(rating)
            if (j <= s) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        content,
                        R.drawable.star_filled
                    )
                )
            } else {
//                imageView.setImageDrawable(ContextCompat.getDrawable(content,R.drawable.star_unfilled))
            }

            starContent.addView(imageView)
        }

    }

    fun showSnack(view: View, content: String) {
        Snackbar.make(view, content, Snackbar.LENGTH_SHORT).show()
    }

    fun showLog(TAG: String, content: String) {
        Log.d(TAG, content)
    }

    fun fetchAccentColor(context: Context): Int {
        val typedValue = TypedValue()
        val a: TypedArray =
            context.obtainStyledAttributes(typedValue.data, intArrayOf(R.attr.colorPrimary))
        val color = a.getColor(0, 0)
        a.recycle()
        return color
    }
}
