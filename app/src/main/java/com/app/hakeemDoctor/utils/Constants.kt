package com.app.hakeemDoctor.utils

import android.Manifest
import android.os.Build.VERSION_CODES.M
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.model.Region
import com.app.hakeemDoctor.BuildConfig
import com.app.hakeemDoctor.utils.Constants.AWS.BASE_S3_URL
import com.app.hakeemDoctor.utils.Constants.AWS.BUCKET_NAME
import com.app.hakeemDoctor.utils.Constants.AWS.ENDPOINT

object Constants {


    object SocketKey {
        const val ID = "id"
        const val LATUTUDE = "latitude"
        const val LONGITUDE = "longitude"


        const val SENDERID = "senderID"
        const val RECIVERID = "receiverID"
        const val CONTENT = "content"
        const val CONTENTTYPE = "content_type"
        const val TIME = "time"
        const val SENDERTYPE = "senderType"
        const val TYPE = "type"
        const val Bookingid = "bookingId"
    }

    object ApiKeys {

        const val RECIVERID = "receiverID"

        const val AUTHORIZATION = "authorization"
        const val LANG = "lang"
        const val STATUS = "status"
        const val ROLE = "role"
        const val PROVIDER = "provider"

        const val EMAIL = "email"
        const val COUNTRYCODE = "countryCode"
        const val MOBILENUMBER = "mobileNumber"
        const val PASSWORD = "password"
        const val NAME = "name"
        const val OTP = "otp"

        const val SOCIALTOKEN = "socialToken"
        const val TYPE = "type"

        const val OS = "os"
        const val FCMTOKEN = "fcmToken"
        const val ANDROID = "android"

        const val IMAGEURL = "imageUrl"
        const val PROFILEPIC = "profilePic"

        const val GENDER = "gender"
        const val EDUCATION = "education"
        const val REGISTERATION_COUNCIL = "registrationCouncil"
        const val EXPERIENCE = "expierence"
        const val CITY = "city"
        const val DATE = "date"
        const val BOOKINGID = "bookingId"

        const val PATIENTID = "patientId"
        const val EXTRAFEE = "extraFee"
        const val REASON = "reason"

        const val LATUTUDE = "latitude"
        const val LONGITUDE = "longitude"


        const val CHANNELNAME = "channelName"
        const val FROM = "from"
        const val TO = "to"
        const val CALLTYPE = "callType"
        const val ID = "id"

        const val AMOUNT = "amount"
        const val TRANSACTIONID = "transactionId"
        const val NOTESCONTENT = "noteContent"

    }

    object IntentKeys {
        const val HOMETYPE = "homeType"
        const val EMAIL = "email"
        const val NAME = "name"
        const val PROFILE_PICTURE = "profilepic"
        const val SOCIALTOKEN = "sociualToken"
        const val LOGINTYPE = "loginType"

        const val BOOKINGID = "bookingId"
        const val PATIENTID = "patientId"

        const val DATE = "date"
        const val DISPLAYMONTH = "displayMonth"
        const val PAYMENTDETAIL = "paymentDetails"

        const val DOCTORSID = "doctorsId"
        const val DOCTORNAME = "doctorsName"
        const val DOCTORIMAGE = "doctorsImage"

    }

    object SocialLogin {
        const val GOOGLE = "google"
        const val FACEBOOK = "facebook"
    }

    object Permission {

        const val CAMERA_PERMISSIONS = 201
        const val READ_STORAGE_PERMISSIONS = 202
        const val CAMERA_STORAGE_PERMISSIONS = 203
        const val LOCATION_PERMISSION = 204
        const val COURSE_LOCATION_PERSISSION = 205

        const val VIDEO_CALL_PERMISSION = 206
        const val AUDIO_CALL_PERMISSION = 207


        val CAMERA_PERM_LIST = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val READ_STORAGE_PERM_LIST = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        val COURSE_LOCATION_PERM_LIST = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION)
        val LOCATION_PERMISSION_PERMISSON_LIST = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        val AUDIO_CALL_PERMISSION_LIST = arrayOf(Manifest.permission.RECORD_AUDIO)
        val VIDEO_CALL_PERMISSION_LIST =
            arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA)
    }

    object RequestCode {

        const val RequestCode = "RequestCode"
        const val PLACEREQUESTCODE = 100
        const val CAMERA_INTENT = 101
        const val GALLERY_INTENT = 102

        const val ILLNESSREQUESTCODE = 104
        const val INSURANCEREQUESTCODE = 105

        const val GPS_REQUEST = 106
        const val PAYPAL_REQUEST_CODE = 108
    }

    //image Upload from amazon s3
    object AWS {
        const val POOL_ID: String = BuildConfig.POOL_ID
        const val BASE_S3_URL: String = BuildConfig.BASE_S3_URL
        const val ENDPOINT: String = BuildConfig.ENDPOINT
        const val BUCKET_NAME: String = BuildConfig.BUCKET_NAME
        val REGION = Regions.AP_SOUTH_1
    }


    object NotificationIds {

        const val CALL_NOTIFICATION_ID = 1
        const val INCOMING_CALL_CHANNEL_ID = "IncomingCall"
        const val INCOMING_CALL_CHANNEL_NAME = "IncomingCall"

        const val HANGUP_CALL_CHANNEL_ID = "HangupCall"
        const val HANGUP_CALL_CHANNEL_NAME = "HangupCall"

    }

    object NotificationActions {

        const val ACCEPT_CALL = "Accept"
        const val REJECT_CALL = "Reject"
        const val HANGUP_CALL = "Hang Up"
        const val END_CALL = "End Call"

        const val CALLDIVERTION = "CallDivertion"

    }

    object NotificationIntentValues {

        const val CHANNEL_ID = "channelid"
        const val FROM_ID = "fromid"
        const val TO_ID = "toid"
        const val NAME = "docName"
        const val IMAGE = "docImage"
        const val CALL_TYPE = "voiceVideo"
        const val CALL_FROM = "incomingOutgoing"
        const val ID = "id"
        const val CALLDIVERTION = "CallDivertion"
    }

    object ChatTypes {

        const val VIDEO_CALL = "video"
        const val VOICE_CALL = "voice"
        const val INCOMING_CALL = "incoming"
        const val OUTGOING_CALL = "outgoing"
        const val ENDCALL = "endcall"
    }

    object EventBusKeys {

        const val ACCEPT_CALL = "acceptcall"
        const val DOCTOR_ACCEPTED_CALL = "doctoracceptcall"
        const val DOCTOR_REJECTED_CALL = "doctorrejectcall"
        const val REJECT_CALL = "rejectCall"
        const val HANGUP_CALL = "hangupcall"
        const val END_CALL = "endcall"




    }


}