package com.app.hakeemDoctor.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import java.util.ArrayList;
import java.util.List;

public class AnimationHelper {


    private Polyline blackPolyline, greyPolyline;
    private static String TAG = AnimationHelper.class.getSimpleName();
    private List<LatLng> listLatLng = new ArrayList();
    private ValueAnimator animator;

    public void viewSlideUp(final View view, int duration) {
        TranslateAnimation textAnimation = new TranslateAnimation(0, 0, 500, 0);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        float fromAlpha = (float) 0.2;
        float toAlpha = (float) 1.0;
        AlphaAnimation fadeInAnimation = new AlphaAnimation(fromAlpha, toAlpha);
        fadeInAnimation.setDuration(duration);
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(textAnimation);
        s.addAnimation(fadeInAnimation);
        view.setAnimation(s);
        view.setVisibility(View.VISIBLE);
    }


    public void slideAndHide(final View view, int duration, int fromX, int toX, int fromY, int toY) {
        TranslateAnimation textAnimation = new TranslateAnimation(fromX, toX, fromY, toY);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(textAnimation);
        view.setAnimation(s);
    }


    public void slideAndSHow(final View view, int duration, int fromX, int toX, int fromY, int toY) {
        TranslateAnimation textAnimation = new TranslateAnimation(fromX, toX, fromY, toY);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(textAnimation);
        view.setAnimation(s);
        view.setVisibility(View.VISIBLE);
    }


    public void animatePolyLine(final Polyline blackPolyLine, List<LatLng> points) {
        this.blackPolyline = blackPolyLine;
        this.listLatLng = points;
        stopPolylineanimation();
        animator = ValueAnimator.ofInt(0, 100);
        animator.setDuration(2500);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                List<LatLng> latLngList = blackPolyLine.getPoints();
                int initialPointSize = latLngList.size();
                int animatedValue = (int) animator.getAnimatedValue();
                int newPoints = (animatedValue * listLatLng.size()) / 100;

                if (initialPointSize < newPoints) {
                    latLngList.addAll(listLatLng.subList(initialPointSize, newPoints));
                    blackPolyLine.setPoints(latLngList);
                }


            }
        });

        animator.addListener(polyLineAnimationListener);
        animator.start();
    }

    public void stopPolylineanimation() {
        try {
            animator.cancel();
        } catch (Exception ignored) {

        }
    }

    private Animator.AnimatorListener polyLineAnimationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {

        }

        @Override
        public void onAnimationEnd(Animator animator) {

            List<LatLng> blackLatLng = blackPolyline.getPoints();
            blackLatLng.clear();
            blackPolyline.setPoints(blackLatLng);
            animator.start();
        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {


        }
    };


    public void viewSlideFromLeft(final View view, int duration) {
        TranslateAnimation textAnimation = new TranslateAnimation(-1500, 0, 0, 0);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        view.startAnimation(textAnimation);
    }


    public void viewSlideFromRight(final View view, int duration) {
        TranslateAnimation textAnimation = new TranslateAnimation(1500, 0, 0, 0);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        view.startAnimation(textAnimation);
    }

    public interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }
}
