package com.app.hakeemDoctor.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.os.Build
import android.os.Handler
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.interfaces.DialogCallBack
import com.app.hakeemDoctor.interfaces.ExtraFeeCallBack
import com.app.hakeemDoctor.interfaces.OnClickListener
import com.app.hakeemDoctor.interfaces.SingleTapListener
import com.app.hakeemDoctor.models.PayedData
import com.app.hakeemDoctor.view.adapter.CommonListAdapter
import kotlin.math.roundToInt


object DialogUtils {

    var loaderDialog: Dialog? = null
    var noInternetDialog: Dialog? = null
    var showPaymentDialog: Dialog? = null
    var newBookingDialog: Dialog? = null
    private var isShowingDialog = false


    fun showPictureDialog(activity: Activity) {

        val alertBuilder = AlertDialog.Builder(activity)
        alertBuilder.setTitle(activity.getString(R.string.choose_your_option))
        val items =
            arrayOf(activity.getString(R.string.gallery), activity.getString(R.string.camera))

        alertBuilder.setItems(items) { _, which ->
            when (which) {
                0 -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(
                        Constants.Permission.READ_STORAGE_PERM_LIST,
                        Constants.Permission.READ_STORAGE_PERMISSIONS
                    )

                } else {
                    BaseUtils.openGallery(activity)
                }
                1 -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(
                        Constants.Permission.CAMERA_PERM_LIST,
                        Constants.Permission.CAMERA_STORAGE_PERMISSIONS
                    )

                } else {
                    BaseUtils.openCamera(activity)
                }
            }
        }

        val alert = alertBuilder.create()
        val window = alert.window
        if (window != null) {
            window.attributes.windowAnimations = R.style.DialogAnimation
        }
        alert.show()
    }

    fun showListDialog(
        context: Context,
        list: ArrayList<String>,
        headerVal: String,
        listener: OnClickListener
    ): Dialog {

        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)

        dialog.setContentView(R.layout.dialog_selection)

        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var listView = dialog.findViewById<RecyclerView>(R.id.listView)
        var header = dialog.findViewById<TextView>(R.id.header)

        header.text = headerVal

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, height)

        listView.layoutManager = LinearLayoutManager(context)
        listView.adapter = CommonListAdapter(
            context,
            list,
            object : OnClickListener {
                override fun onClickItem(position: Int) {
                    listener.onClickItem(position)
                    dialog.dismiss()
                }

            })

        dialog.show()

        return dialog
    }

    fun showAlert(
        context: Context,
        singleTapListener: SingleTapListener,
        content: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var contentView = dialog.findViewById<TextView>(R.id.content)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content

        ok.setOnClickListener {
            dialog.dismiss()
        }

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        dialog.show()

    }

    fun showAlertHeader(
        context: Context,
        singleTapListener: SingleTapListener,
        headerString: String,
        content: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var contentView = dialog.findViewById<TextView>(R.id.content)
        var header = dialog.findViewById<TextView>(R.id.header)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content
        header.text = headerString

        ok.setOnClickListener {
            dialog.dismiss()
        }

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        dialog.show()

    }

    fun showSuccessDialog(
        context: Context,
        singleTapListener: SingleTapListener,
        content: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_success_consultation)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        var message = dialog.findViewById<TextView>(R.id.message)
        message.text = content
        dialog.window?.setLayout(width, height)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        Handler().postDelayed({

            if (dialog.isShowing) {
                dialog.dismiss()
                singleTapListener.singleTap()
            }

        }, 3000)

        dialog.show()

    }


//    fun showDialog(
//        context: Context,
//        header: String,
//        content: String,
//        positiveText: String,
//        negativeText: String,
//        dialogCallback: DialogCallback
//    ) {
//
//        var alertDialog = AlertDialog.Builder(context)
//        alertDialog.setTitle(header)
//        alertDialog.setMessage(content)
//        alertDialog.setCancelable(false)
//        alertDialog.setPositiveButton(positiveText) { _, _ ->
//            dialogCallback.positiveClickListner()
//        }
//        alertDialog.setNegativeButton(negativeText) { _, _ ->
//            dialogCallback.negativeClickListner()
//        }
//        alertDialog.show()
//    }

    fun showLoader(context: Context) {

        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }

        loaderDialog = Dialog(context)
        loaderDialog?.setCancelable(false)
        loaderDialog?.setCanceledOnTouchOutside(false)
        loaderDialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val inflater = LayoutInflater.from(context)
        val view = inflater?.inflate(R.layout.dialog_loader, null)
        if (view != null) {
            loaderDialog!!.setContentView(view)
        }

       // loaderDialog!!.anim_view.playAnimation()
        loaderDialog!!.findViewById<com.airbnb.lottie.LottieAnimationView>(R.id.anim_view).playAnimation()

        if (!loaderDialog?.isShowing!!) {
            loaderDialog?.show()
        }

    }

    fun dismissLoader() {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }
    }

//    fun noInternetDialog(context: Context, singleTapListener: SingleTapListener) {
//
//        if (noInternetDialog != null) {
//            if (isShowingDialog) {
//
//            }
//        }
//
//        if (!isShowingDialog) {
//            noInternetDialog = Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
//            noInternetDialog?.setCancelable(false)
//            noInternetDialog?.setCanceledOnTouchOutside(false)
//            noInternetDialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                    WindowManager.LayoutParams.MATCH_PARENT)
//
//            val inflater = LayoutInflater.from(context)
//            val view = inflater?.inflate(R.layout.dialog_no_internet, null)
//            if (view != null) {
//                noInternetDialog!!.setContentView(view)
//            }
//
//            noInternetDialog!!.tryAgain.setOnClickListener {
//                if (NetworkUtils.isNetworkConnected(context)) {
//                    singleTapListener.singleTap()
//                    dismissInternetDialog(object : SingleTapListener {
//                        override fun singleTap() {
//                            singleTapListener.singleTap()
//                        }
//
//                    })
//                }
//            }
//
//
//            noInternetDialog?.show()
//            isShowingDialog = true
//        }
//    }
//
//    fun dismissInternetDialog(singleTapListener: SingleTapListener) {
//        if (noInternetDialog != null) {
//            if (isShowingDialog) {
//                noInternetDialog!!.dismiss()
//                singleTapListener.singleTap()
//                isShowingDialog = false
//            }
//        }
//    }

    fun checkGpsIsEnabled(context: Context): Boolean {
        val locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager

        return locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun showAlertDialog(
        context: Context,
        content: String,
        title: String,
        positiveText: String,
        negativeText: String,
        callBack: DialogCallBack
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_alert)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)


        var headerTextView = dialog.findViewById<TextView>(R.id.header)
        var contentTextView = dialog.findViewById<TextView>(R.id.content)

        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        headerTextView.text = title
        contentTextView.text = content

        cancel.text = negativeText
        ok.text = positiveText

        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            callBack.onPositiveClick()
            dialog.dismiss()
        }



        dialog.show()

    }

    fun showAlertDialogPayment(
        context: Context,
        content: String,
        title: String,
        positiveText: String,
        negativeText: String,
        callBack: DialogCallBack
    ) {

        showPaymentDialog?.let {
            if (it.isShowing) {
                return
            }
        }

        showPaymentDialog = Dialog(context)
        showPaymentDialog?.setCancelable(false)
        showPaymentDialog?.setCanceledOnTouchOutside(false)
        showPaymentDialog?.setContentView(R.layout.dialog_alert)
        showPaymentDialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        showPaymentDialog?.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)


        var headerTextView = showPaymentDialog?.findViewById<TextView>(R.id.header)
        var contentTextView = showPaymentDialog?.findViewById<TextView>(R.id.content)

        var cancel = showPaymentDialog?.findViewById<TextView>(R.id.cancel)
        var ok = showPaymentDialog?.findViewById<TextView>(R.id.ok)

        headerTextView?.text = title
        contentTextView?.text = content

        cancel?.text = negativeText
        ok?.text = positiveText

        cancel?.setOnClickListener {
            callBack.onNegativeClick()
            showPaymentDialog?.dismiss()
        }

        ok?.setOnClickListener {
            callBack.onPositiveClick()
            showPaymentDialog?.dismiss()
        }



        showPaymentDialog?.show()

    }

    fun showExtraFeeDialog(
        context: Context,
        positiveText: String,
        negativeText: String,
        callBack: ExtraFeeCallBack
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_extra_amount)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        var extraAmount = dialog.findViewById<EditText>(R.id.extraAmount)

        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var ok = dialog.findViewById<TextView>(R.id.ok)


        cancel.text = negativeText
        ok.text = positiveText

        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            if (extraAmount.text.toString().trim().isNotEmpty()) {
                callBack.onPositiveClick(extraAmount.text.toString().toInt())
                dialog.dismiss()
            }
        }



        dialog.show()

    }

    fun showSendMoneyDialog(
        context: Context,
        positiveText: String,
        negativeText: String,
        callBack: ExtraFeeCallBack
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_extra_amount)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        var extraAmount = dialog.findViewById<EditText>(R.id.extraAmount)
        var header = dialog.findViewById<TextView>(R.id.header)
        var content = dialog.findViewById<TextView>(R.id.content)

        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        extraAmount.hint = context.getString(R.string.enter_amount)
        header.text = context.getString(R.string.payment)
        content.text = context.getString(R.string.deposit_money_admin)

        cancel.text = negativeText
        ok.text = positiveText

        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            if (extraAmount.text.toString().trim().isNotEmpty() && extraAmount.text.toString().toInt() != 0) {
                callBack.onPositiveClick(extraAmount.text.toString().toInt())
                dialog.dismiss()
            }
        }



        dialog.show()

    }

    @SuppressLint("SetTextI18n")
    fun showNewBookingAlert(
        context: Context,
        callBack: DialogCallBack,
        data: PayedData,
        lat: String,
        lng: String
    ) {

        newBookingDialog?.let {
            if (it.isShowing) {
                return
            }
        }

        newBookingDialog = Dialog(context)
        newBookingDialog?.setCancelable(false)
        newBookingDialog?.setCanceledOnTouchOutside(false)
        newBookingDialog?.setContentView(R.layout.dialog_new_appoinment)
        newBookingDialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.95).roundToInt()

        newBookingDialog?.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        var patientName = newBookingDialog?.findViewById<TextView>(R.id.patientName)
        var yearsOld = newBookingDialog?.findViewById<TextView>(R.id.yearsOld)
        var location = newBookingDialog?.findViewById<TextView>(R.id.location)
        var kmAway = newBookingDialog?.findViewById<TextView>(R.id.kmAway)
        var date = newBookingDialog?.findViewById<TextView>(R.id.date)

        var phoneNumber = newBookingDialog?.findViewById<TextView>(R.id.phoneNumber)
        var email = newBookingDialog?.findViewById<TextView>(R.id.email)

        var reject = newBookingDialog?.findViewById<CardView>(R.id.reject)
        var accept = newBookingDialog?.findViewById<CardView>(R.id.accept)


        data.isFeatureBooking?.let {

            if (it == "true") {
                patientName?.let {

                    it.text = data.patientName
                }

                yearsOld?.let {
                    it.text = data.patientAge
                }
            } else {
                patientName?.let {

                    it.text = data.name
                }

                yearsOld?.let {
                    data.DOB?.let {
                        yearsOld.text = "${BaseUtils.getAgeCalculation(it)} years old"
                    }
                }

            }
        }



        location?.let {
            it.text = data.location
        }

        phoneNumber?.let {
            it.text = "${data.countryCode}  ${data.mobileNumber}"
        }

        email?.let {
            it.text = data.email
        }


        kmAway?.let {
            data.bookingLatitude?.let { bkLat ->
                data.bookingLongitude?.let { bkLng ->
                    lat?.let { lat ->
                        lng?.let { lng ->
                            it.text =
                                "${BaseUtils.calculateDistance(
                                    bkLat.toDouble(),
                                    bkLng.toDouble(),
                                    lat.toDouble(),
                                    lng.toDouble()
                                )} km away"
                        }
                    }
                }
            }
        }


        reject?.setOnClickListener {
            callBack.onNegativeClick()
            newBookingDialog?.dismiss()
        }

        accept?.setOnClickListener {

            callBack.onPositiveClick()
            newBookingDialog?.dismiss()

        }



        newBookingDialog?.show()

    }
}