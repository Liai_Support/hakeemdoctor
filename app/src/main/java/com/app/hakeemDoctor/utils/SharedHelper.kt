package com.app.hakeemDoctor.utils

import android.content.Context

class SharedHelper(context: Context) {

    private var sharedPreference: SharedPref =
        SharedPref(context)

    var token: String
        get() : String {
            return sharedPreference.getKey("token")
        }
        set(value) {
            sharedPreference.putKey("token", value)
        }

    var providerType: String
        get() : String {
            return sharedPreference.getKey("providerType")
        }
        set(value) {
            sharedPreference.putKey("providerType", value)
        }

    var fcmToken: String
        get() : String {
            return sharedPreference.getKey("fcmToken")
        }
        set(value) {
            sharedPreference.putKey("fcmToken", value)
        }

    var language: String
        get() : String {
            return if (sharedPreference.getKey("language") == "") {
                "en"
            } else {
                sharedPreference.getKey("language")
            }

        }
        set(value) {
            sharedPreference.putKey("language", value)
        }

    var id: Int
        get() : Int {
            return sharedPreference.getInt("id")
        }
        set(value) {
            sharedPreference.putInt("id", value)
        }

    var name: String
        get() : String {
            return sharedPreference.getKey("name")
        }
        set(value) {
            sharedPreference.putKey("name", value)
        }

    var email: String
        get() : String {
            return sharedPreference.getKey("email")
        }
        set(value) {
            sharedPreference.putKey("email", value)
        }

    var mobileNumber: String
        get() : String {
            return sharedPreference.getKey("mobileNumber")
        }
        set(value) {
            sharedPreference.putKey("mobileNumber", value)
        }

    var imageUploadPath: String
        get() : String {
            return sharedPreference.getKey("imageUploadPath")
        }
        set(value) {
            sharedPreference.putKey("imageUploadPath", value)
        }
    var countryCode: String
        get() : String {
            return sharedPreference.getKey("countryCode")
        }
        set(value) {
            sharedPreference.putKey("countryCode", value)
        }

    var userImage: String
        get() : String {
            return sharedPreference.getKey("userImage")
        }
        set(value) {
            sharedPreference.putKey("userImage", value)
        }


    var latitude: String
        get() : String {
            return sharedPreference.getKey("latitude")
        }
        set(value) {
            sharedPreference.putKey("latitude", value)
        }

    var longitude: String
        get() : String {
            return sharedPreference.getKey("longitude")
        }
        set(value) {
            sharedPreference.putKey("longitude", value)
        }

    var bookingId: String
        get() : String {
            return sharedPreference.getKey("bookingId")
        }
        set(value) {
            sharedPreference.putKey("bookingId", value)
        }

    var loggedIn: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("loggedIn")
        }
        set(value) {
            sharedPreference.putBoolean("loggedIn", value)
        }

    var status: String
        get() : String {
            return sharedPreference.getKey("status")
        }
        set(value) {
            sharedPreference.putKey("status", value)
        }

    var chatTiming: Int
        get() : Int {
            return sharedPreference.getInt("chatTiming")
        }
        set(value) {
            sharedPreference.putInt("chatTiming", value)
        }

    var sharePercentage: Int
        get() : Int {
            return sharedPreference.getInt("sharePercentage")
        }
        set(value) {
            sharedPreference.putInt("sharePercentage", value)
        }
}