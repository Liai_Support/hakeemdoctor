package com.app.hakeemDoctor.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.util.Patterns
import androidx.core.content.FileProvider
import com.app.hakeemDoctor.R
import java.io.File
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


object BaseUtils {


    fun numberFormat(value: Int): String {
        return "%02d".format(value)
    }

    fun numberFormat(value: Double): String {
        return String.format("%.2f", value)
    }


    fun getTimeFromTimeStamp(value: String): String {

        var date = Date(value.toLong())
        val simpleDateFormat = SimpleDateFormat("hh:mm aa", Locale.ENGLISH)

        return simpleDateFormat.format(date)

    }

    fun getFormatedDate(date: String, inputFormat: String, outputFormat: String): String {

        var spf = SimpleDateFormat(inputFormat, Locale.ENGLISH)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        spf = SimpleDateFormat(outputFormat, Locale.ENGLISH)
        return if (newDate != null)
            spf.format(newDate)
        else
            ""

    }

    fun getFormatedDateUtc(
        date: String?,
        inputFormat: String?,
        outputFormat: String?
    ): String? {
        Log.d("receive",""+date)
        Log.d("q2wertyuio",""+date)
        Log.d("gbnmjk",""+inputFormat)
        var spf =
            SimpleDateFormat(inputFormat, Locale.ENGLISH)
        spf.timeZone = TimeZone.getTimeZone("UTC")
        Log.d("xcdfvgbhnjm",""+spf.timeZone)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
         //   Log.d("fghjkl;",""+date+":00")
            Log.d("fghjkl;",""+newDate)

            Log.d("zxcvfgbhnjmk,l",""+newDate)
        } catch (e: java.text.ParseException) {
            e.printStackTrace()

            Log.d("sdfghjk",""+e.printStackTrace())
            Log.d("sdfghjk",""+e)

        }
          Log.d("wertyuio",""+outputFormat)
        spf = SimpleDateFormat(outputFormat, Locale.ENGLISH)
        Log.d("return",""+spf.format(newDate))
        Log.d("3456uio",""+spf.format(newDate))
        return spf.format(newDate)
    }

    fun addEndTime(
        date: String,
        intputFormat: String,
        outputFormat: String,
        minutes: Int
    ): String? {

        var spf = SimpleDateFormat(intputFormat, Locale.ENGLISH)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        newDate?.let {
            var calander = getCalanderInstance(newDate)
            calander.add(Calendar.MINUTE, minutes)
            spf = SimpleDateFormat(outputFormat, Locale.ENGLISH)
            return spf.format(calander.time)

        }

        return ""

    }

    fun getCalanderInstance(date: Date): Calendar {
        var calander = Calendar.getInstance()
        calander.time = date
        return calander
    }


    fun getRoundedOffSixDigits(value: Double, format: String): String? {
        return String.format(Locale.ENGLISH, format, value)
    }

    fun calculateDistance(
        lat_a: Double,
        lng_a: Double,
        lat_b: Double,
        lng_b: Double
    ): String? {
        val earthRadius = 3958.75
        val latDiff = Math.toRadians(lat_b - lat_a)
        val lngDiff = Math.toRadians(lng_b - lng_a)
        val a = sin(latDiff / 2) * sin(latDiff / 2) +
                cos(Math.toRadians(lat_a)) * cos(
            Math.toRadians(lat_b)
        ) *
                sin(lngDiff / 2) * sin(lngDiff / 2)
        val c =
            2 * atan2(sqrt(a), sqrt(1 - a))
        val distance = earthRadius * c
        val meterConversion = 1609
        return ((distance * meterConversion).toInt() / 1000).toString()
    }

    private fun stringToDate(date: String): Date {
        return SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(date)!!
    }

    private fun dateToCalender(date: Date): Calendar {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.time = date
        return cal
    }

    fun calenderToDate(calender: Calendar, format: String): String {
        return SimpleDateFormat(format, Locale.ENGLISH).format(calender.time)
    }

    fun getCurrentDate(format: String): String {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        var date = cal.time
        return SimpleDateFormat(format, Locale.ENGLISH).format(date)
    }

    fun getFormatCurrentDate(format: String, date: Date): String {
        return SimpleDateFormat(format, Locale.ENGLISH).format(date)
    }

    fun isPastTime(givenTime: String): Boolean {
        val simpleDateFormat = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
        var date1 = simpleDateFormat.parse(givenTime)
        var date2 =
            simpleDateFormat.parse(SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(Date()))

        if (date1 != null && date2 != null) {
            val difference = date1.time - date2.time
            val days = difference / (1000 * 60 * 60 * 24)

            val hours = difference / (1000 * 60 * 60)
            val mins = difference / (1000 * 60) % 60
//            var hours = ((difference - 1000 * 60 * 60 * 24 * days) / (1000 * 60 * 60))
//            val min = (difference - 1000 * 60 * 60 * 24 * days - 1000 * 60 * 60 * hours) / (1000 * 60)

            if (hours < 0) {
                return true
            } else {
                if (hours > 0) {
                    return false
                }
                return mins <= 0
            }
        } else {
            return true
        }
    }

    fun stringToCalendering(dateString: String): Calendar {
        val date: Date =
            stringToDate(dateString)
        return dateToCalender(date)
    }

    fun dpToPx(dp: Float): Int {
        val density = Resources.getSystem().displayMetrics.density
        return Math.ceil((dp * density).toDouble()).toInt()
    }


    fun differenceBetweenDates(appDate: String): Float {

        val currentDate: Date = Calendar.getInstance(Locale.ENGLISH).time
        val appoinmentdate: Date =
            SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(appDate)!!

        val diff = appoinmentdate.time - currentDate.time

        val dayCount = diff.toFloat() / (24 * 60 * 60 * 1000)

        return dayCount

    }

    fun differenceBetweenDatesInInt(appDate: String): Int {

        val currentDate: Date = Calendar.getInstance(Locale.ENGLISH).time
        val appoinmentdate: Date =
            SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(appDate)!!

        val diff = appoinmentdate.time - currentDate.time

        val dayCount = diff.toFloat() / (24 * 60 * 60 * 1000)

        return dayCount.toInt()

    }

    fun getDay(day: Int): String {
        return when (day) {
            1 -> "Sun"
            2 -> "Mon"
            3 -> "Tue"
            4 -> "Wed"
            5 -> "Thu"
            6 -> "Fri"
            7 -> "Sat"
            else -> ""
        }
    }

    fun checkPermission(context: Context, permissionName: String): Boolean {
        val res = context.checkCallingPermission(permissionName)
        return res == PackageManager.PERMISSION_GRANTED
    }

    fun getDayFromDate(calenderInstance: Calendar): String {
        return getDay(
            calenderInstance.get(
                Calendar.DAY_OF_WEEK
            )
        )
    }

    fun isValidPassword(context: Context, password: String): String {
        val specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
        val upperCasePatten = Pattern.compile("[A-Z ]")
        val lowerCasePatten = Pattern.compile("[a-z ]")
        val digitCasePatten = Pattern.compile("[0-9 ]")

        return if (password.length < 8) {
            context.resources.getString(R.string.password_length_should_be_eight_characters)
        } else if (!specailCharPatten.matcher(password).find()) {
            context.resources.getString(R.string.password_should_contain_atleast_one_special_character)
        } else if (!upperCasePatten.matcher(password).find()) {
            context.resources.getString(R.string.password_should_contain_atleast_one_uppercase_character)
        } else if (!lowerCasePatten.matcher(password).find()) {
            context.resources.getString(R.string.password_should_contain_atleast_one_lowercase_character)
        } else if (!digitCasePatten.matcher(password).find()) {
            context.resources.getString(R.string.password_should_contain_atleast_one_numeric)
        } else {
            "true"
        }
    }

    fun isValidMobile(phone: String): Boolean {
        var check = false
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length in 6..13) {
                check = true
            }
        }
        return check
    }

    fun isValidEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun openCamera(activity: Activity) {
        val sharedHelper = SharedHelper(activity)
        val file = getFileTostoreImage(activity)
        val uri: Uri

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                // Ensure that there's a camera activity to handle the intent
                takePictureIntent.resolveActivity(activity.packageManager)?.also {
                    // Create the File where the photo should go
                    val photoFile: File? = try {
                        getFileTostoreImage(activity)
                    } catch (ex: IOException) {
                        // Error occurred while creating the File
                        null
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            activity,
                            "com.app.HakeemDoctor.fileprovider",
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        activity.startActivityForResult(takePictureIntent, Constants.RequestCode.CAMERA_INTENT)
                    }
                }
            }

        } else {
            uri = Uri.fromFile(file)
            sharedHelper.imageUploadPath = file.absolutePath
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            activity.startActivityForResult(takePictureIntent, Constants.RequestCode.CAMERA_INTENT)
        }
    }

    fun openCameraold(activity: Activity) {

        val sharedHelper = SharedHelper(activity)
        val file = getFileTostoreImage(activity)

        val uri: Uri
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(activity, activity.packageName + ".fileprovider", file)
            sharedHelper.imageUploadPath = file.absolutePath
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            activity.startActivityForResult(takePictureIntent, Constants.RequestCode.CAMERA_INTENT)

        } else {
            uri = Uri.fromFile(file)
            sharedHelper.imageUploadPath = file.absolutePath
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            activity.startActivityForResult(takePictureIntent, Constants.RequestCode.CAMERA_INTENT)
        }
    }


    fun openGallery(activity: Activity) {
        val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        activity.startActivityForResult(
            i,
            Constants.RequestCode.GALLERY_INTENT
        )
    }





    private fun getFileTostoreImage(activity: Activity): File {
        val sharedHelper = SharedHelper(activity)

        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            sharedHelper.imageUploadPath = absolutePath
        }
    }

    private fun getFileTostoreImageold(context: Context): File {
        val filepath = Environment.getExternalStorageDirectory()
        val zoeFolder = File(
            filepath.absolutePath,
            context.getString(R.string.app_name)
        ).absoluteFile
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir()
        }
        val newFolder = File(
            zoeFolder,
            context.getString(R.string.app_name) + " Image"
        ).absoluteFile
        if (!newFolder.exists()) {
            newFolder.mkdir()
        }

        val filename = System.currentTimeMillis()
        val camera_captureFile =
            "IMG_" + filename + "_" + System.currentTimeMillis().toString() + "_"

        return File(newFolder, "$camera_captureFile.jpg")
    }

    @SuppressLint("NewApi")
    fun getRealPathFromUriNew(context: Context, uri: Uri): String? {

        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(
                    uri
                )
            ) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }

                //  handle non-primary volumes
            } else if (isDownloadsDocument(
                    uri
                )
            ) {

                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )

                return getDataColumn(
                    context,
                    contentUri,
                    null,
                    null
                )
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                when (type) {
                    "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(
                    context,
                    contentUri,
                    selection,
                    selectionArgs
                )
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                context,
                uri,
                null,
                null
            )

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private fun getDataColumn(
        context: Context, uri: Uri?, selection: String?,
        selectionArgs: Array<String>?
    ): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor =
                context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    fun getRealPathFromURI(context: Context, contentURI: Uri): String? {
        val result: String?
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = context.contentResolver.query(contentURI, filePathColumn, null, null, null)

        if (cursor == null) {
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor
                .getColumnIndex(filePathColumn[0])
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    fun getAgeCalculation(it: String): Int? {

        var b_year = 0

        try {
            b_year = getFormatedDate(it, "yyyy-MM-dd", "yyyy").toInt()
        } catch (e: NumberFormatException) {

        }

        var cur_year = Calendar.getInstance().get(Calendar.YEAR)

        return cur_year - b_year
    }

    fun isValidCancelTime(date: String, format: String, time: Int): Boolean {

        var spf = SimpleDateFormat(format, Locale.ENGLISH)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        var secondsInMilli = 1000
        var minutesInMilli = secondsInMilli * 60

        var currentDate: Date = Calendar.getInstance().time

        var currentString = spf.format(currentDate)

        var curretDate: Date? = null
        try {
            curretDate = spf.parse(currentString)
            Log.d("'asdfghjk",""+currentDate)
        } catch (e: ParseException) {
            e.printStackTrace()

        }


        newDate?.let { nDate ->
            curretDate?.let { cDate ->

                var diff = nDate.time - cDate.time
                var elapsedMinutes: Int = (diff / minutesInMilli).toInt()

                Log.d("diff", elapsedMinutes.toString())


                return elapsedMinutes > time


            }
        }

        return false
    }


    fun isConsultationTime(date: String, format: String, time: Int): Boolean {

        var spf = SimpleDateFormat(format, Locale.ENGLISH)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        var secondsInMilli = 1000
        var minutesInMilli = secondsInMilli * 60

        var currentDate: Date = Calendar.getInstance().time


        var currentString = spf.format(currentDate)

        var curretDate: Date? = null
        try {
            curretDate = spf.parse(currentString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        newDate?.let { nDate ->
            curretDate?.let { cDate ->

                var diff = nDate.time - cDate.time
                var elapsedMinutes: Int = (diff / minutesInMilli).toInt()

                Log.d("diff", elapsedMinutes.toString())


                return elapsedMinutes in -time+1..0


            }
        }

        return false
    }


    fun isConsultationTimeOver(date: String, format: String, time: Int): Boolean {

        var spf = SimpleDateFormat(format, Locale.ENGLISH)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        var secondsInMilli = 1000
        var minutesInMilli = secondsInMilli * 60

        var currentDate: Date = Calendar.getInstance().time


        var currentString = spf.format(currentDate)

        var curretDate: Date? = null
        try {
            curretDate = spf.parse(currentString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        newDate?.let { nDate ->
            curretDate?.let { cDate ->

                var diff = nDate.time - cDate.time
                var elapsedMinutes: Int = (diff / minutesInMilli).toInt()

                Log.d("diff", elapsedMinutes.toString())


                return elapsedMinutes < -time+1


            }
        }


        return false
    }
}
