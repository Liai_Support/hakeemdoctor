package com.app.hakeemDoctor.app

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.IntentFilter
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import androidx.multidex.MultiDexApplication
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class AppController : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        instance = this
    }

    companion object {

        private val TAG: String = AppController::class.java.simpleName
        private var instance: AppController? = null
        private var requestQueue: RequestQueue? = null
        private val callTone =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString()

        @Synchronized
        fun getInstance(): AppController {
            return instance as AppController
        }

    }


    private fun getRequestQueue(): RequestQueue? {

        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(
                instance
            )

        return requestQueue
    }

    fun <T> addrequestToQueue(request: Request<T>) {
        request.tag = TAG
        getRequestQueue()?.add(request)
    }

    override fun onCreate() {
        super.onCreate()


        registerReceiver(
            TransferNetworkLossHandler.getInstance(applicationContext), IntentFilter(
                ConnectivityManager.CONNECTIVITY_ACTION
            )
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }

    }

    private fun createNotificationChannel() {


        val attributesCall = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
            .build()


        val notificationManager =
            instance?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val incomingCall =
                NotificationChannel("Alert", "Alert", NotificationManager.IMPORTANCE_HIGH)
            incomingCall.setSound(Uri.parse(callTone), attributesCall)
            notificationManager.createNotificationChannel(incomingCall)
        }



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val attributesCallMessage = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()
            val message =
                NotificationChannel("message", "message", NotificationManager.IMPORTANCE_HIGH)
            message.setSound(Uri.parse(callTone), attributesCallMessage)
            notificationManager.createNotificationChannel(message)
        } else {
        }


    }
}
