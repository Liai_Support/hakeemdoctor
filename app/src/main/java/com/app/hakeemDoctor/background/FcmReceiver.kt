package com.app.hakeemDoctor.background

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.util.Log
import androidx.core.app.NotificationCompat
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.notification.NotificationUtils
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.view.activity.ChatActivity
import com.app.hakeemDoctor.view.activity.DashBoardActivity
import com.app.hakeemDoctor.view.activity.VideoCallActivity
import com.app.hakeemUser.rxbus.RxBusNotification
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject

class FcmReceiver : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)


        val jsonObject = JSONObject(p0.data as Map<*, *>)
        if (jsonObject.has("type")) {
            when (jsonObject.optString("type")) {
                "call_notification" -> {
                    val jsonObjectBody = JSONObject(jsonObject["your_custom_data_key"].toString())
                    if (jsonObjectBody["type"] == Constants.ChatTypes.ENDCALL) {
                        endCall(jsonObjectBody)
                    } else if ((jsonObjectBody["callType"] == Constants.ChatTypes.VOICE_CALL
                                || jsonObjectBody["callType"] == Constants.ChatTypes.VIDEO_CALL)
                        && jsonObjectBody["type"] == "createcall"
                    ) {
                        incomingCall(jsonObjectBody)
                    }
                }

                "chat_notification" -> {
                    val jsonObjectBody = JSONObject(jsonObject["your_custom_data_key"].toString())
                    setChatNotification(jsonObjectBody)
                }

                else -> {
                    setNotification(jsonObject.optString("title"), jsonObject.optString("body"))
                }
            }
        } else
            setNotification(jsonObject.optString("title"), jsonObject.optString("body"))

        RxBusNotification.send("FCM")

    }


    private fun setChatNotification(jsonObjectBody: JSONObject) {


        val intent = Intent(this, ChatActivity::class.java)
            .putExtra(Constants.IntentKeys.DOCTORSID, jsonObjectBody.optString("senderID").toInt())
            .putExtra(Constants.IntentKeys.DOCTORNAME, jsonObjectBody.optString("name"))
            .putExtra(Constants.IntentKeys.DOCTORIMAGE, jsonObjectBody.optString("image"))

        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        var notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val notification: NotificationCompat.Builder = NotificationCompat.Builder(this, "message")
            .setContentText(jsonObjectBody.optString("name"))
            .setContentTitle(jsonObjectBody.optString("content"))
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.splash_image)
            .setSound(defaultSoundUri)
            .setFullScreenIntent(pendingIntent, true)




        notificationManager.notify(System.currentTimeMillis().toInt(), notification.build())

    }

    private fun incomingCall(jsonObject: JSONObject) {

        var intent = Intent(this, VideoCallActivity::class.java)
            .putExtra(
                Constants.NotificationIntentValues.CALL_TYPE,
                jsonObject["callType"].toString()
            )
            .putExtra(
                Constants.NotificationIntentValues.CALL_FROM,
                Constants.ChatTypes.INCOMING_CALL
            )
            .putExtra(Constants.NotificationIntentValues.CHANNEL_ID, jsonObject["to"].toString())
            .putExtra(Constants.NotificationIntentValues.TO_ID, jsonObject["to"].toString())
            .putExtra(Constants.NotificationIntentValues.FROM_ID, jsonObject["from"].toString())
            .putExtra(Constants.NotificationIntentValues.NAME, jsonObject["name"].toString())
            .putExtra(Constants.NotificationIntentValues.IMAGE, jsonObject["imageurl"].toString())
            .putExtra(Constants.NotificationIntentValues.ID, jsonObject["to"].toString())

        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        applicationContext.startActivity(intent)
        setIncomingNotification(jsonObject, intent)

    }

    private fun setIncomingNotification(jsonObject: JSONObject, intent: Intent) {

        var notificationUtils = NotificationUtils(this)
        if (jsonObject["callType"].toString() == Constants.ChatTypes.VOICE_CALL)
            notificationUtils.notificationIncomingCallVoice(
                "Incoming Voice call",
                jsonObject["name"].toString(),
                intent
            )
        else if (jsonObject["callType"].toString() == Constants.ChatTypes.VIDEO_CALL)
            notificationUtils.notificationIncomingCallVideo(
                "Incoming Video call",
                jsonObject["name"].toString(),
                intent
            )
    }


    private fun endCall(jsonObject: JSONObject) {
        UiUtils.showLog(" End call ", jsonObject.toString())
        NotificationUtils(this).removeCallNotifications()
        RxBusNotification.send(Constants.EventBusKeys.END_CALL)
    }


    private fun sendBookingNotification(
        userId: String,
        bookingId: String,
        body: String,
        title: String
    ) {

        val intent = Intent(this, DashBoardActivity::class.java)
            .putExtra(Constants.IntentKeys.PATIENTID, userId)
            .putExtra(Constants.IntentKeys.BOOKINGID, bookingId)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        var notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        val notification: NotificationCompat.Builder = NotificationCompat.Builder(this, "Alert")
            .setContentText(body)
            .setContentTitle(title)
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.splash_image)
            .setSound(defaultSoundUri)
            .setFullScreenIntent(pendingIntent, true)

        notificationManager.notify(System.currentTimeMillis().toInt(), notification.build())
    }


    fun setNotification(title: String, body: String) {

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val intent = Intent(this, DashBoardActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        var notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        val notification: NotificationCompat.Builder = NotificationCompat.Builder(this, "Alert")
            .setContentText(body)
            .setContentTitle(title)
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.splash_image)
            .setSound(defaultSoundUri)
            .setFullScreenIntent(pendingIntent, true)

        notificationManager.notify(System.currentTimeMillis().toInt(), notification.build())
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)

        SharedHelper(this).fcmToken = p0
    }


}