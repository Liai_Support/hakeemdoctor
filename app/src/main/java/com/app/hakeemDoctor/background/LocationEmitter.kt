package com.app.hakeemDoctor.background

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import androidx.core.app.ActivityCompat
import com.app.hakeemDoctor.network.UrlHelper
import com.app.hakeemDoctor.utils.BaseUtils
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import io.socket.client.Ack
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import java.net.URISyntaxException


class LocationEmitter : Service() {


    companion object {
        private var socket: Socket? = null
        private var sharedHelper: SharedHelper? = null
        var listener: LocationListener? = null
        var locationManager: LocationManager? = null
        var handler = Handler()
        lateinit var runnable: Runnable
    }


    class MyLocationListener : LocationListener {

        override fun onLocationChanged(location: Location) {
            location?.let {
                sharedHelper?.latitude = location.latitude.toString()
                sharedHelper?.longitude = location.longitude.toString()
            }
        }

        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

        }

        override fun onProviderEnabled(provider: String) {

        }

        override fun onProviderDisabled(provider: String) {

        }

    }


    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()



        if (Build.VERSION.SDK_INT >= 26) {
            val CHANNEL_ID = "LocationTracking"
            val channel = NotificationChannel(
                CHANNEL_ID,
                "LocationTracking",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            (getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                channel
            )

            val notification: Notification = Notification.Builder(this, CHANNEL_ID)
                .setContentTitle("Location Enabled")
                .setContentText("Location tracking on progress").build()
            startForeground(1, notification)
        }

        sharedHelper = SharedHelper(this)
        initSockets()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        listener = MyLocationListener()

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return START_STICKY
        }

        listener?.let {
            locationManager?.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                5000,
                0f,
                it
            )
        }



        return START_STICKY
    }


    private fun initSockets() {
        val opts = IO.Options()
        opts.forceNew = true
        opts.reconnection = false
//        opts.query = Constants.ApiKeys.AUTHORIZATION + "=" + sharedHelper.token
        try {
            socket = IO.socket(UrlHelper.SOCKETURL, opts)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        socket?.on(Socket.EVENT_DISCONNECT) {
            Log.d(" Socket ", " DisConnected")
            initSockets()
        }

        socket?.on(Socket.EVENT_CONNECT) {
            Log.d(" Socket ", " Connected")
//            initSockets()
        }

        socket?.let {
            if (!it.connected())
                socket?.connect()
        }

        runnable = Runnable {

            if(socket == null){
                initSockets()
            }
            var jsonObject = JSONObject()
            jsonObject.put(Constants.SocketKey.ID, sharedHelper?.bookingId)
            jsonObject.put(Constants.SocketKey.LATUTUDE, BaseUtils.getRoundedOffSixDigits(sharedHelper?.latitude!!.toDouble(),"%6f"))
            jsonObject.put(Constants.SocketKey.LONGITUDE, BaseUtils.getRoundedOffSixDigits(sharedHelper?.longitude!!.toDouble(),"%6f"))

            socket?.emit(UrlHelper.UPDATEPROVIDERLOCATION, jsonObject, object : Ack {
                override fun call(vararg args: Any?) {

                }
            })

            Log.d(
                "Emitting ",
                "${sharedHelper?.bookingId}  ${sharedHelper?.latitude}  ${sharedHelper?.longitude}"
            )

            handler.postDelayed(runnable, 4000)

        }


        handler.removeCallbacks(runnable)
        handler.postDelayed(runnable, 4000)


    }

    override fun onDestroy() {
        super.onDestroy()
        listener?.let {
            locationManager?.removeUpdates(it)
        }

        handler.removeCallbacks(runnable)

    }
}