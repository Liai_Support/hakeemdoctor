package com.app.hakeemDoctor.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemDoctor.RouteGenerate.Route
import com.app.hakeemDoctor.models.*
import com.app.hakeemDoctor.network.ApiInput
import com.app.hakeemDoctor.network.UrlHelper
import com.app.hakeemDoctor.repository.BookingRepository
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import com.google.android.gms.maps.model.LatLng
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList

class BookingViewModel(application: Application) : AndroidViewModel(application) {

    var repository: BookingRepository = BookingRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? =
        SharedHelper(applicationIns.applicationContext)


    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
       // sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = "ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnBaQ0k2TVN3aWJtRnRaU0k2SWtoaGNta2lMQ0pwWVhRaU9qRTJNakV5TWpnd016TXNJbVY0Y0NJNk1UWXlNemd5TURBek0zMC5KOFZYNzhFdjZyZDd5X3ZTMUdNYTZidkZxcTJKczFrUFJnWnctVUtzS25Z" }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PROVIDER

        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun updateDeviceToken(context: Context) {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.OS, Constants.ApiKeys.ANDROID)
        jsonObject.put(Constants.ApiKeys.FCMTOKEN, sharedHelper?.fcmToken)

//        repository.updateDeviceToken(getApiParams(jsonObject, UrlHelper.UPDATE_DEVICE_TOKEN))


    }

    fun getBookingList(context: Context, currentDate: String): LiveData<ListOfBookingResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.DATE, currentDate)

        return repository.getBookingList(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.LIST_OF_BOOKINGS
            )
        )

    }

    fun acceptBooking(context: Context, bookingId: Int?): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)

        return repository.acceptBooking(getApiParams(context, jsonObject, UrlHelper.ACCEPT_BOOKING))

    }

    fun gettestlistDetails(context : Context): LiveData<GetTestListResponse>? {

        return repository.gettestlistDetails(getApiParams(context,null, UrlHelper.GETTESTLIST))
    }

    fun getlablist(context: Context, idarray: JSONArray, bookingID: Int
    ): LiveData<GetLabListResponseModel>? {

        val jsonObject = JSONObject()
       // jsonObject.put("lat", sharedHelper?.currentLat)
        //jsonObject.put("lan", sharedHelper?.currentLng)
        jsonObject.put("bookingId", bookingID)
        jsonObject.put("test_ids", idarray)
        Log.d("khjkjkjk",""+jsonObject);


        return repository.getlablistrepo(getApiParams(context,jsonObject, UrlHelper.GETLABLIST))
    }

    fun cancelBooking(
        context: Context,
        bookingId: Int?,
        reason: String
    ): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)
        jsonObject.put(Constants.ApiKeys.REASON, reason)

        return repository.cancelBooking(getApiParams(context, jsonObject, UrlHelper.CANCEL_BOOKING))

    }

    fun getBookingDetail(
        context: Context,
        bookingID: Int,
        patientId: Int
    ): LiveData<ListOfBookingResponse>? {


        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.PATIENTID, patientId)
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingID)

        return repository.getBookingDetails(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.TRACK_PROVIDER
            )
        )

    }

    fun getRoute(context: Context, source: LatLng, destination: LatLng): LiveData<Route>? {

        return repository.getRouteDetails(applicationIns, source, destination)

    }

    fun updateBookingStartVisitStatus(context: Context, bookingId: Int): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)

        return repository.updateBookingStartVisitStatus(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.STATRVISIT
            )
        )
    }

    fun updateBookingCompleteVisitStatus(
        context: Context,
        bookingId: Int,
        fee: Int
    ): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)
        jsonObject.put(Constants.ApiKeys.EXTRAFEE, fee)

        return repository.updateBookingCompleteVisitStatus(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.COMPLETEVISIT
            )
        )
    }

    fun updateBookingDestination(
        context: Context,
        bookingId: Int
    ): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)

        return repository.updateBookingDestination(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.UPDATEREACHEDDESTINATION
            )
        )
    }

    fun getRating(context: Context): LiveData<ReviewResponse>? {

        return repository.getRating(
            getApiParams(
                context,
                null,
                UrlHelper.SHOWREVIEW
            )
        )

    }

    fun getConsultationHistory(
        context: Context,
        date: String
    ): LiveData<ConsultationHistoryResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.DATE, date)


        return repository.getConsultationHistory(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.CONSULTATION_HISTORY
            )
        )

    }

    fun confirmPaymentVerification(context: Context, bookingId: Int?): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)

        return repository.confirmPaymentVerification(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.UPDATEPAYMENTCONFIRMATION
            )
        )

    }


    fun getTransactionList(context: Context): LiveData<TransactionHistoryResponse>? {


        return repository.getTransactionList(
            getApiParams(
                context,
                null,
                UrlHelper.WALLETHISTORY
            )
        )


    }

    fun getWalletHistory(context: Context, date: String): LiveData<StatisResponse> {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.DATE, date)

        return repository.getWalletHistory(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.WALLETHISTORYDATE
            )
        )

    }

    fun payAdmin(context : Context,paymentAmount: Int, tnxId: String) : LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.AMOUNT, paymentAmount)
        jsonObject.put(Constants.ApiKeys.TRANSACTIONID, tnxId)

        return repository.payAdmin(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.PAYADMIN
            )
        )
    }

    fun addNotes(context: Context,bookingIds: String, bookingID: Int, patientId: Int, notes: String, medicinearray: JSONArray, medicinedescarray: JSONArray, testidarray: JSONArray, labidarray: JSONArray) : LiveData<CommonResponse>?{


        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingID)
        jsonObject.put("bookingIds", bookingIds)
        jsonObject.put(Constants.ApiKeys.PATIENTID, patientId)
        jsonObject.put(Constants.ApiKeys.NOTESCONTENT, notes)
        jsonObject.put("medicine", medicinearray)
        jsonObject.put("medicine_desc", medicinedescarray)
        jsonObject.put("test_to_take", testidarray)
        jsonObject.put("suggested_labs", labidarray)

        return repository.addNotes(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.ADDNOTES
            )
        )
    }

    fun getNotificationData(context: Context): LiveData<NotificationResponse> {
        return repository.getNotificationData(
            getApiParams(
                context,
                null,
                UrlHelper.GETMYNOTIFICATION
            )
        )
    }

    fun clearNotification(context: Context): LiveData<CommonResponse>? {
        return repository.clearNotification(
            getApiParams(
                context,
                null,
                UrlHelper.DELETEMYNOTIFICATION
            )
        )
    }

    fun viewlabreport(context: Context,id:Int): LiveData<ViewLabReportResponse> {
        val jsonObject = JSONObject()
        jsonObject.put("bookingId", id)

        return repository.viewlabreport(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.VIEWLABREPORT
            )
        )
    }

}