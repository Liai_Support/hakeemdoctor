package com.app.hakeemDoctor.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemDoctor.models.ChatResponse
import com.app.hakeemDoctor.models.CommonResponse
import com.app.hakeemDoctor.models.CreateCallResponseModel
import com.app.hakeemDoctor.network.ApiInput
import com.app.hakeemDoctor.network.UrlHelper
import com.app.hakeemDoctor.repository.CallRepository
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import org.json.JSONObject

class CallViewModel(application: Application) : AndroidViewModel(application) {

    var sharedHelper: SharedHelper? = null
    var callRepository: CallRepository? = null
    var applicationInstance: Application? = null

    init {
        sharedHelper = SharedHelper(application)
        callRepository = CallRepository.getInstance()
        applicationInstance = application
    }


    private fun getApiParams(jsonObject: JSONObject?, url: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language}
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PROVIDER

        val apiInputs = ApiInput()
        apiInputs.context = applicationInstance?.applicationContext
        apiInputs.jsonObject = jsonObject
        apiInputs.url = url
        apiInputs.headers = header

        return apiInputs
    }

    fun createCall(
        channelName: String,
        to: String,
        callType: String,
        from: String
    ): LiveData<CreateCallResponseModel>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.CHANNELNAME, channelName)
        jsonObject.put(Constants.ApiKeys.TO, to)
        jsonObject.put(Constants.ApiKeys.CALLTYPE, callType)
        jsonObject.put(Constants.ApiKeys.FROM, from)
        return callRepository?.createCall(getApiParams(jsonObject, UrlHelper.CREATECALL))
    }

    fun acceptCall(
        channelName: String,
        from: String,
        id: String,
        calltype: String
    ): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.CHANNELNAME, channelName)
        jsonObject.put(Constants.ApiKeys.FROM, from)
        jsonObject.put(Constants.ApiKeys.TO, id)
        jsonObject.put(Constants.ApiKeys.CALLTYPE, calltype)
        return callRepository?.acceptCall(getApiParams(jsonObject, UrlHelper.ACCEPTCALL))
    }

    fun endCall(
        channelName: String,
        from: String,
        to: String,
        callType: String
    ): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.CHANNELNAME, channelName)
        jsonObject.put(Constants.ApiKeys.FROM, from)
        jsonObject.put(Constants.ApiKeys.TO, to)
        jsonObject.put(Constants.ApiKeys.CALLTYPE, callType)
        return callRepository?.endCall(getApiParams(jsonObject, UrlHelper.ENDCALL))
    }

    fun getChatDetails(reciverID: String,Bookingid:String): LiveData<ChatResponse>? {

        var jsonObject = JSONObject()

        jsonObject.put(Constants.ApiKeys.RECIVERID, reciverID)
        jsonObject.put("bookingID", Bookingid)
        jsonObject.put("page", "1")

        return callRepository?.getChatDetails(getApiParams(jsonObject, UrlHelper.CHATDETAILS))
    }

}