package com.app.hakeemDoctor.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemDoctor.models.CommonResponse
import com.app.hakeemDoctor.repository.AmazonRepository
import com.app.hakeemDoctor.utils.SharedHelper
import java.io.File

class AmazonViewModel(application: Application) : AndroidViewModel(application) {

    var repository: AmazonRepository = AmazonRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? =
        SharedHelper(applicationIns.applicationContext)


    fun uploadImage(
        context: Context,
        file: File
    ): LiveData<CommonResponse>? {
        return repository.uploadImageToAWS(context, file)
    }


}