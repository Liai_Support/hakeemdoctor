package com.app.hakeemDoctor.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemDoctor.models.*
import com.app.hakeemDoctor.network.ApiInput
import com.app.hakeemDoctor.network.UrlHelper
import com.app.hakeemDoctor.repository.RegisterRepository
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import org.json.JSONObject

class RegisterViewModel(application: Application) : AndroidViewModel(application) {

    var repository: RegisterRepository = RegisterRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? =
        SharedHelper(applicationIns.applicationContext)


    fun createAccount(
        name: String,
        emailId: String,
        countryCode: String,
        mobileNumber: String,
        password: String
    ): LiveData<RegisterResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put(Constants.ApiKeys.EMAIL, emailId)
        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, countryCode)
        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, mobileNumber)
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)

        return repository.createAccount(
            getApiParams(
                jsonObject,
                UrlHelper.CREATE_ACCOUNT
            )
        )
    }

    private fun getApiParams(jsonObject: JSONObject, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PROVIDER


        val apiInputs = ApiInput()
        apiInputs.context = applicationIns.applicationContext
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun signin(email: String, password: String): LiveData<LoginResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)

        return repository.signIn(
            getApiParams(
                jsonObject,
                UrlHelper.LOGIN
                //"http://192.168.100.10:3000/provider/login"
                      //  "http://52.91.243.240:3000/provider/login"
            )
        )
    }

    fun getOtp(email: String): LiveData<GetOtpResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.EMAIL, email)

        return repository.getOtp(
            getApiParams(
                jsonObject,
                UrlHelper.GET_OTP
            )
        )
    }

    fun verifyOtp(email: String, otp: String): LiveData<CheckOtpResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
        jsonObject.put(Constants.ApiKeys.OTP, otp)

        return repository.checkOtp(
            getApiParams(
                jsonObject,
                UrlHelper.CHECK_OTP
            )
        )

    }

    fun resetPassword(password: String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)

        return repository.resetPassword(
            getApiParams(
                jsonObject,
                UrlHelper.ENTER_NEW_PASSWORD
            )
        )

    }

    fun socialLogin(id: String?, type: String): LiveData<SocialLoginResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SOCIALTOKEN, id)
        jsonObject.put(Constants.ApiKeys.TYPE, type)

        return repository.socialLogin(
            getApiParams(
                jsonObject,
                UrlHelper.CHECK_SOCIAL_LOGIN
            )
        )
    }

    fun socialRegister(
        email: String,
        imageurl: String,
        name: String,
        socialToken: String,
        selectedCountryCode: String?,
        phoneNumber: String
    ): LiveData<SocialRegisterResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
        jsonObject.put(Constants.ApiKeys.PROFILEPIC, imageurl)
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put(Constants.ApiKeys.SOCIALTOKEN, socialToken)
        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, selectedCountryCode)
        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, phoneNumber)

        return repository.socilaRegisterDetails(
            getApiParams(
                jsonObject,
                UrlHelper.GOOGLE_LOGIN
            )
        )

    }


}