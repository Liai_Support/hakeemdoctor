package com.app.hakeemDoctor.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemDoctor.models.CommonResponse
import com.app.hakeemDoctor.models.ProfileDetailsResponse
import com.app.hakeemDoctor.network.ApiInput
import com.app.hakeemDoctor.network.UrlHelper
import com.app.hakeemDoctor.repository.ProfileRepository
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import org.json.JSONObject

class ProfileViewModel(application: Application) : AndroidViewModel(application) {

    var repository: ProfileRepository = ProfileRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? =
        SharedHelper(applicationIns.applicationContext)


    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PROVIDER


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun getProfileDetails(context: Context): LiveData<ProfileDetailsResponse>? {
        return repository.getProfileDetails(getApiParams(context, null, UrlHelper.GET_PROFILE))
    }

    fun updateProfile(
        context: Context,
        name: String,
        email: String,
        cCode: String,
        mobile: String,
        gender: String,
        city: String,
        qualification: String,
        council: String,
        experience: String,
        profilePic: String?
    ): LiveData<CommonResponse>? {


        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, cCode)
        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, mobile)
        jsonObject.put(Constants.ApiKeys.GENDER, gender)
        jsonObject.put(Constants.ApiKeys.CITY, city)
        jsonObject.put(Constants.ApiKeys.EDUCATION, qualification)
        jsonObject.put(Constants.ApiKeys.REGISTERATION_COUNCIL, council)
        jsonObject.put(Constants.ApiKeys.EXPERIENCE, experience)
        profilePic?.let { jsonObject.put(Constants.ApiKeys.PROFILEPIC, it) }


        return repository.updateProfile(getApiParams(context, jsonObject, UrlHelper.UPDATE_PROFILE))
    }


}