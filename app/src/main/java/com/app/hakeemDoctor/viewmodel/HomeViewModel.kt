package com.app.hakeemDoctor.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemDoctor.models.AppSettingsResponse
import com.app.hakeemDoctor.models.CommonResponse
import com.app.hakeemDoctor.models.HomeDashBoardResponse
import com.app.hakeemDoctor.models.VersionCheckResponse
import com.app.hakeemDoctor.network.ApiInput
import com.app.hakeemDoctor.network.UrlHelper
import com.app.hakeemDoctor.repository.HomeRepository
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import org.json.JSONObject

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    var repository: HomeRepository = HomeRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? =
        SharedHelper(applicationIns.applicationContext)


//    fun createAccount(
//        name: String,
//        emailId: String,
//        countryCode: String,
//        mobileNumber: String,
//        password: String
//    ): LiveData<RegisterResponse>? {
//
//        val jsonObject = JSONObject()
//        jsonObject.put(Constants.ApiKeys.NAME, name)
//        jsonObject.put(Constants.ApiKeys.EMAIL, emailId)
//        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, countryCode)
//        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, mobileNumber)
//        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
//
//        return repository.createAccount(
//            getApiParams(
//                jsonObject,
//                UrlHelper.CREATE_ACCOUNT
//            )
//        )
//    }

    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PROVIDER


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun updateDeviceToken(context: Context) {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.OS, Constants.ApiKeys.ANDROID)
        jsonObject.put(Constants.ApiKeys.FCMTOKEN, sharedHelper?.fcmToken)
        jsonObject.put("status",sharedHelper?.status)

        repository.updateDeviceToken(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.UPDATE_DEVICE_TOKEN
            )
        )

    }

    fun getHomeBoardDetails(context: Context): LiveData<HomeDashBoardResponse>? {
        return repository.getHomeDetails(getApiParams(context, null, UrlHelper.HOMEDASHBOARD))

    }

    fun getAppSettings(context: Context): MutableLiveData<AppSettingsResponse>? {

        return repository.getAppSettings(getApiParams(context, null, UrlHelper.APPSETTINGS))

    }

    fun updateLocation(context: Context, myLat: Double?, myLng: Double?) {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.LATUTUDE, myLat)
        jsonObject.put(Constants.ApiKeys.LONGITUDE, myLng)

        return repository.updateLocation(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.PROVIDERUPDATELATLNG
            )
        )

    }

    fun callBookigResponse(
        context: Context,
        bookingId: Int?,
        fee: String,
        methodName: String
    ): LiveData<CommonResponse> {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)


        return repository.callBookigResponse(
            getApiParams(
                context,
                jsonObject,
                methodName
            )
        )

    }
    fun getcheckversion(context: Context): LiveData<VersionCheckResponse> {
        return repository.getcheckversion(
            getApiParams(
                context,
                null,
                UrlHelper.VERSION_CHECK
            )
        )
    }


}