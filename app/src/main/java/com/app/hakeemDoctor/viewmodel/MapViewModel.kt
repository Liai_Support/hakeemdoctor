package com.app.hakeemDoctor.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemDoctor.models.AutoCompleteResponse
import com.app.hakeemDoctor.network.ApiInput
import com.app.hakeemDoctor.repository.MapRepository
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import org.json.JSONObject

class MapViewModel(application: Application) : AndroidViewModel(application) {

    var repository: MapRepository = MapRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? =
        SharedHelper(applicationIns.applicationContext)


//    fun createAccount(
//        name: String,
//        emailId: String,
//        countryCode: String,
//        mobileNumber: String,
//        password: String
//    ): LiveData<RegisterResponse>? {
//
//        val jsonObject = JSONObject()
//        jsonObject.put(Constants.ApiKeys.NAME, name)
//        jsonObject.put(Constants.ApiKeys.EMAIL, emailId)
//        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, countryCode)
//        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, mobileNumber)
//        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
//
//        return repository.createAccount(
//            getApiParams(
//                jsonObject,
//                UrlHelper.CREATE_ACCOUNT
//            )
//        )
//    }

    private fun getApiParams(context: Context,jsonObject: JSONObject, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PROVIDER


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun getAddress(input: ApiInput): LiveData<AutoCompleteResponse>? {
        return repository.getAddress(input)
    }



}