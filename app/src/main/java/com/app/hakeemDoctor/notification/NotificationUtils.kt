package com.app.hakeemDoctor.notification

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.utils.Constants


class NotificationUtils(var context: Context) {


    private var notificationManager: NotificationManager? = null
    private val DEFAULT_NOTIFICATION_ICON: Int = R.mipmap.ic_launcher


    fun notificationIncomingCallVoice(docName: String, body: String, intent: Intent) {

        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)

        val intentActivity = PendingIntent.getActivity(context, Constants.NotificationIds.CALL_NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = NotificationCompat.Builder(context, Constants.NotificationIds.INCOMING_CALL_CHANNEL_ID)
            .setContentTitle(docName)
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setContentText(body)
            .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
            .setAutoCancel(true)
            .setContentIntent(intentActivity)
            .setSound(defaultSoundUri)
            .setFullScreenIntent(intentActivity, true)
            .setUsesChronometer(false)
            .setSmallIcon(DEFAULT_NOTIFICATION_ICON)


        val actionCall = Intent(context, CallNotificationReceiver::class.java)
        actionCall.action = Constants.NotificationActions.ACCEPT_CALL

        val actionReject = Intent(context, CallNotificationReceiver::class.java)
        actionReject.action = Constants.NotificationActions.REJECT_CALL

        val pendingActionCall = PendingIntent.getBroadcast(context, Constants.NotificationIds.CALL_NOTIFICATION_ID, actionCall, PendingIntent.FLAG_UPDATE_CURRENT)
        val pendingActionReject = PendingIntent.getBroadcast(context, Constants.NotificationIds.CALL_NOTIFICATION_ID, actionReject, PendingIntent.FLAG_UPDATE_CURRENT)


        notification.addAction(R.drawable.call_icon, Constants.NotificationActions.ACCEPT_CALL, pendingActionCall)
        notification.addAction(R.drawable.call_end, Constants.NotificationActions.REJECT_CALL, pendingActionReject)


        notificationManager?.notify(Constants.NotificationIds.CALL_NOTIFICATION_ID, notification.build())
    }


    fun notificationIncomingCallVideo(docName: String, body: String, intent: Intent) {

        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)

        val intentActivity = PendingIntent.getActivity(context, Constants.NotificationIds.CALL_NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = NotificationCompat.Builder(context, Constants.NotificationIds.INCOMING_CALL_CHANNEL_ID)
            .setContentTitle(docName)
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setContentText(body)
            .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
            .setAutoCancel(true)
            .setAutoCancel(false)
            .setContentIntent(intentActivity)
            .setSound(defaultSoundUri)
            .setFullScreenIntent(intentActivity, true)
            .setUsesChronometer(false)
            .setSmallIcon(DEFAULT_NOTIFICATION_ICON)

        val actionCall = Intent(context, CallNotificationReceiver::class.java)
        actionCall.action = Constants.NotificationActions.ACCEPT_CALL

        val actionReject = Intent(context, CallNotificationReceiver::class.java)
        actionReject.action = Constants.NotificationActions.REJECT_CALL

        val pendingActionCall = PendingIntent.getBroadcast(context, Constants.NotificationIds.CALL_NOTIFICATION_ID, actionCall, PendingIntent.FLAG_UPDATE_CURRENT)
        val pendingActionReject = PendingIntent.getBroadcast(context, Constants.NotificationIds.CALL_NOTIFICATION_ID, actionReject, PendingIntent.FLAG_UPDATE_CURRENT)

        notification.addAction(R.drawable.video_on, Constants.NotificationActions.ACCEPT_CALL, pendingActionCall)
        notification.addAction(R.drawable.call_end, Constants.NotificationActions.REJECT_CALL, pendingActionReject)



        notificationManager?.notify(Constants.NotificationIds.CALL_NOTIFICATION_ID, notification.build())
    }

    fun notificationEndCall(docName: String) {

        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notification = NotificationCompat.Builder(context, Constants.NotificationIds.HANGUP_CALL_CHANNEL_ID)
            .setContentTitle(docName)
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setUsesChronometer(true)
            .setSmallIcon(DEFAULT_NOTIFICATION_ICON)


        val actionEnd = Intent(context, CallNotificationReceiver::class.java)
        actionEnd.action = Constants.NotificationActions.END_CALL

        val pendingActionHangUp = PendingIntent.getBroadcast(context, Constants.NotificationIds.CALL_NOTIFICATION_ID, actionEnd, PendingIntent.FLAG_UPDATE_CURRENT)
        notification.addAction(R.drawable.call_end, Constants.NotificationActions.END_CALL, pendingActionHangUp)

        notificationManager?.notify(Constants.NotificationIds.CALL_NOTIFICATION_ID, notification.build())
    }


    fun notificationHangupCall(docName: String, body: String) {

        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notification = NotificationCompat.Builder(context, Constants.NotificationIds.HANGUP_CALL_CHANNEL_ID)
            .setContentTitle(docName)
            .setContentText(body)
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
            .setAutoCancel(true)
            .setSmallIcon(DEFAULT_NOTIFICATION_ICON)


        val actionEnd = Intent(context, CallNotificationReceiver::class.java)
        actionEnd.action = Constants.NotificationActions.HANGUP_CALL

        val pendingActionHangUp = PendingIntent.getBroadcast(context, Constants.NotificationIds.CALL_NOTIFICATION_ID, actionEnd, PendingIntent.FLAG_UPDATE_CURRENT)
        notification.addAction(R.drawable.call_end, Constants.NotificationActions.HANGUP_CALL, pendingActionHangUp)

        notificationManager?.notify(Constants.NotificationIds.CALL_NOTIFICATION_ID, notification.build())
    }

    fun removeCallNotifications() {
        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager?.cancel(Constants.NotificationIds.CALL_NOTIFICATION_ID)
    }
}