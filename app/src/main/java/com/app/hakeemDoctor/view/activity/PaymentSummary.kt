package com.app.hakeemDoctor.view.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivitySocialLoginDetailsBinding
import com.app.hakeemDoctor.databinding.ActivitySummaryBinding
import com.app.hakeemDoctor.interfaces.DialogCallBack
import com.app.hakeemDoctor.models.PayedData
import com.app.hakeemDoctor.utils.BaseUtils
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.viewmodel.BookingViewModel

class PaymentSummary : BaseActivity() {

    var binding: ActivitySummaryBinding? = null
    var bookingViewModel: BookingViewModel? = null

    var data: PayedData? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySummaryBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_summary)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        getIntentValues()
        binding!!.cardView10.visibility = View.GONE

    }

    private fun getIntentValues() {

        intent.extras?.let { bundle ->
            bundle.getParcelable<PayedData>(Constants.IntentKeys.PAYMENTDETAIL)?.let { value ->
                data = value
                setData()
            }
        }

    }

    private fun setData() {

        data?.let { value ->
            value.isFeatureBooking?.let { value1 ->
                if (value1 == "true") {
                    value.patientName?.let { binding!!.patientName.text = it }
                    value.patientAge?.let { binding!!.yearsOld.text = "$it years old" }
                    value.previousIssue?.let { binding!!.healthIssue.text = it }
                    binding!!.healthIssueText.visibility = View.VISIBLE
                } else {
                    binding!!.healthIssueText.visibility = View.INVISIBLE
                    binding!!.healthIssue.text = ""
                    value.name?.let { binding!!.patientName.text = it }
                    value.DOB?.let {
                        binding!!.yearsOld.text = "${BaseUtils.getAgeCalculation(it)} years old"
                    }
                }
            }

//            value.previousIssue?.let { healthIssue.text = it }

            value.providerName?.let { binding!!.docName.text = it }
            value.location?.let { binding!!.location.text = it }

            value.fee?.let {
                if (value.extraFee != null) {
                    binding!!.singleConsultation.text = "$it SAR"
                    binding!!.consultationAmount.text = "${value.extraFee} SAR"
                    binding!!.totalAmount.text = "${it.toInt() + value.extraFee!!.toInt()} SAR"
                } else {
                    binding!!.singleConsultation.text = "$it SAR"
                    binding!!.consultationAmount.text = "0 SAR"
                    binding!!.totalAmount.text = "$it SAR"
                }

            }


            value.countryCode?.let { code ->
                value.mobileNumber?.let {
                    binding!!.phoneNumber.text = "+$code $it"
                }
            }

            value.email?.let { binding!!.mailId.text = it }

            value.bookingDate?.let {
                binding!!.date.text =
                    BaseUtils.getFormatedDate(it, "yyyy-MM-dd'T'HH:mm:ss", "dd MMMM yyyy")
            }
            value.bookingTime?.let {
                binding!!.time.text =
                    BaseUtils.getFormatedDate(it, "HH:mm:ss", "hh:mm a")
            }
        }

    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    fun onPaymentRecived(view: View) {

        data?.let {


            DialogUtils.showAlertDialog(this,
                resources.getString(R.string.payment_confirm_sure),
                resources.getString(R.string.invoice),
                resources.getString(R.string.ok),
                resources.getString(R.string.cancel), object : DialogCallBack {
                    override fun onPositiveClick() {

                        DialogUtils.showLoader(this@PaymentSummary)
                        bookingViewModel?.confirmPaymentVerification(
                            this@PaymentSummary,
                            it.bookingId
                        )?.observe(this@PaymentSummary,
                            Observer {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                UiUtils.showSnack(binding!!.root, msg)
                                            }
                                        } else {
                                            finish()
                                        }
                                    }

                                }
                            })

                    }

                    override fun onNegativeClick() {

                    }
                })

        }
    }
}