package com.app.hakeemDoctor.view.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityDashboardBinding
import com.app.hakeemDoctor.interfaces.OnClickListener
import com.app.hakeemDoctor.utils.BaseUtils
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.view.fragment.*
import com.app.hakeemDoctor.viewmodel.HomeViewModel
import java.util.*

class DashBoardActivity : BaseActivity() {

    var binding: ActivityDashboardBinding? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var imageList = arrayListOf<ImageView>()
    private var homeFragment: HomeFragment? = null
    private var profileFragment: ProfileFragment? = null
    private var consultationFragment: AppoinmentApprovalFragment? = null
    private var homeViewModel: HomeViewModel? = null
    private var sharedHelper: SharedHelper? = null
    public var fvalue: String?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)

        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        sharedHelper = SharedHelper(this)

        imageList.add(binding!!.home)
        imageList.add(binding!!.appoinment)
        imageList.add(binding!!.wallet)
        imageList.add(binding!!.account)
       // updateDeviceToken()
         //setAdapter(0)
        if (intent.extras == null) {
            setAdapter(0)
        }else{
            var value =  intent.extras!!.getInt(Constants.IntentKeys.HOMETYPE, 0)
            setAdapter(value)
        }

        binding!!.homeDrawer.patientName1.text = sharedHelper?.name
        UiUtils.loadImage(binding!!.homeDrawer.circleImageView1, sharedHelper?.userImage)


        if(sharedHelper?.status == "online"){
            binding!!.customSwitch12.isChecked = true
            statuson()
        }
        binding!!.customSwitch12.setOnCheckedChangeListener { buttonView, isChecked ->
            /* val msg: String = if (isChecked)
                 "Switch Button is Checked"
             else
                 "Switch Button is UnChecked"
             showToast(msg)*/
            if(isChecked) {
                sharedHelper?.status = "online"
                statuson()
            }
            else{
                sharedHelper?.status = "offline"
                statuson()
            }
        }

        binding!!.notification1.setOnClickListener {
            startActivity(Intent(this, NotificationActivity::class.java))
        }


        /*     val crashButton = Button(this)
              crashButton.text = "Test Crash"
              crashButton.setOnClickListener {
                  throw RuntimeException("Test Crash") // Force a crash
              }

              addContentView(crashButton, ViewGroup.LayoutParams(
                  ViewGroup.LayoutParams.MATCH_PARENT,
                  ViewGroup.LayoutParams.WRAP_CONTENT))*/

    }

    private fun statuson(){
      updateDeviceToken()
    }



    fun updateDeviceToken() {

        homeViewModel?.updateDeviceToken(this)
    }

    public fun setAdapter(position: Int) {
        setUiTab(position)
        when (position) {
            0 -> {
                homeFragment = HomeFragment(object : OnClickListener {
                    override fun onClickItem(position: Int) {
                        setAdapter(position)
                    }

                })

                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.commit()


            }
            1 -> {
                consultationFragment =
                    AppoinmentApprovalFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, consultationFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()


            }
            2 -> {
                /*val myWalletFragment =
                    EarningsFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myWalletFragment)
                fragmentTransaction.commit()*/

                /*val myWalletFragment =
                    EarningsFragment1()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myWalletFragment)
                fragmentTransaction.commit()*/
/*
                val myWalletFragment =
                    WalletFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myWalletFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()*/

                val myWalletFragment = EarningsFragment1()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myWalletFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
            3 -> {
                profileFragment =
                    ProfileFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, profileFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()


            }
            4->{
                val myWalletFragment =
                    WalletFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myWalletFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }

        }
    }

    private fun setUiTab(position: Int) {
        for (i in 0 until imageList.size) {
            if (position == i) {
                //imageList[i].background = ContextCompat.getDrawable(this, R.drawable.tab_selected_background)
               // imageList[i].setColorFilter(UiUtils.fetchAccentColor(this))
                imageList[i].setColorFilter(ContextCompat.getColor(this, R.color.bnav))

            } else {
               // imageList[i].setBackgroundColor(ContextCompat.getColor(this, R.color.transparent))
                imageList[i].setColorFilter(ContextCompat.getColor(this, R.color.grey))
            }
        }
    }


    fun onHomeClicked(view: View) {
        setAdapter(0)
    }

    fun onAppoinmentClicked(view: View) {
        setAdapter(1)
        closeDrawer()
    }

    fun onCloseClicked(view: View){
        closeDrawer()
    }

    fun onWalletClicked(view: View) {
        setAdapter(4)
        closeDrawer()
    }

    fun onAccountClicked(view: View) {
        setAdapter(3)
        closeDrawer()
    }

    fun onEarningsClicked(view: View) {
        startActivity(Intent(this, EarningsActivity::class.java))
        closeDrawer()


    }

    fun onLogoutClicked(view: View) {
        closeDrawer()
        sharedHelper?.loggedIn = false
        sharedHelper?.status = "offline"
        val intent =
            Intent(this, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    fun OnChooseLanguageClicked(view: View) {
        startActivity(Intent(this, ChooseLanguageActivity::class.java))
        closeDrawer()

    }

    fun onPatientConsultationHistoryActivity(view: View){
        var calender = Calendar.getInstance()

        DatePickerDialog(
            this, R.style.CalenderPickerTheme,
            DatePickerDialog.OnDateSetListener { p0, p1, p2, p3 ->


                val bundle = Bundle()
                /*val myMessage = "Stack Overflow is cool!"
                bundle.putString("message", myMessage)*/
               /* bundle.putString(
                    Constants.IntentKeys.DATE,
                    "$p1-${BaseUtils.numberFormat(p2 + 1)}-$p3"
                )*/

                bundle.putString(
                    Constants.IntentKeys.DATE,
                    "$p1-${BaseUtils.numberFormat(p2 + 1)}-${BaseUtils.numberFormat(p3)}"
                )
                bundle.putString(
                    Constants.IntentKeys.DISPLAYMONTH,
                    BaseUtils.getFormatedDate(
                        "$p1-${BaseUtils.numberFormat(p2 + 1)}-$p3",
                        "yyyy-MM-dd",
                        "MMMM"
                    )
                )
                val myWalletFragment = PatientConsultationHistoryFragment()
                myWalletFragment.setArguments(bundle)
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myWalletFragment)
                fragmentTransaction.commit()

            },
            calender.get(Calendar.YEAR),
            calender.get(Calendar.MONTH),
            calender.get(Calendar.DAY_OF_MONTH)
        ).show()
        closeDrawer()
    }

    fun onDrawerMenuClicked(view: View) {
        binding!!.drawer.openDrawer(GravityCompat.START)
        binding!!.navigationDrawer.bringToFront()
    }


    /*override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            Log.d("cgxcv","zxcbvcx ")
            setAdapter(0)
           // finish()
        }



    }*/



    fun closeDrawer() {
        if (binding!!.drawer.isDrawerOpen(GravityCompat.START)) {
            binding!!.drawer.closeDrawer(GravityCompat.START)
        }
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestCode.GPS_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                homeFragment?.onActivityResults(requestCode, resultCode, data)
                consultationFragment?.onActivityResults(requestCode, resultCode, data)
            }
        }

        profileFragment?.onActivityResults(requestCode, resultCode, data)

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        profileFragment?.onRequestPermissionResult(requestCode, permissions, grantResults)

    }


}