package com.app.hakeemDoctor.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityForgotPasswordBinding
import com.app.hakeemDoctor.models.CheckOtpData
import com.app.hakeemDoctor.models.GetOtpData
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.SharedHelper
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.viewmodel.RegisterViewModel

class ForgotPasswordActivity : BaseActivity() {

    var binding: ActivityForgotPasswordBinding? = null
    private var viewmodel: RegisterViewModel? = null
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedHelper = SharedHelper(this)

        getIntentValues()
    }

    private fun getIntentValues() {
        intent.extras?.let {
            it.getString(Constants.IntentKeys.EMAIL)?.let { email ->
                binding!!.emailId.setText(email)
                getOtp(email)
            }
        }
    }

    private fun getOtp(email: String) {
        DialogUtils.showLoader(this)
        viewmodel?.getOtp(email)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data)
                        }
                    }
                }
            }
        })
    }

    private fun handleResponse(data: GetOtpData) {
        data.otp?.let {
            binding!!.otp.setText(it)
        }
    }

    fun onVerifiedClicked(view: View) {


        if (binding!!.otp.text.toString().trim() == "") {
            UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourotp))
        } else {
            DialogUtils.showLoader(this)
            viewmodel?.verifyOtp(binding!!.emailId.text.toString(), binding!!.otp.text.toString())?.observe(this,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.root, msg)
                                }
                            } else {
                                it.data?.let { data ->
                                    handleCheckotpResponse(data)
                                }
                            }
                        }

                    }

                })
        }

    }

    private fun handleCheckotpResponse(data: CheckOtpData) {
        data.token?.let { sharedHelper?.token = it }
        startActivity(Intent(this, ResetPasswordActivity::class.java))

    }

}