package com.app.hakeemDoctor.view.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.barchart.components.XAxis
import com.app.hakeemDoctor.barchart.data.BarData
import com.app.hakeemDoctor.barchart.data.BarDataSet
import com.app.hakeemDoctor.barchart.data.BarEntry
import com.app.hakeemDoctor.barchart.data.Entry
import com.app.hakeemDoctor.barchart.highlight.Highlight
import com.app.hakeemDoctor.barchart.interfaces.datasets.IBarDataSet
import com.app.hakeemDoctor.barchart.listener.OnChartValueSelectedListener
import com.app.hakeemDoctor.barchart.model.GradientColor
import com.app.hakeemDoctor.barchart.utils.YearXAxisFormatter
import com.app.hakeemDoctor.databinding.FragmentEarnings1Binding
import com.app.hakeemDoctor.databinding.FragmentEarningsBinding
import com.app.hakeemDoctor.databinding.FragmentWalletBinding
import com.app.hakeemDoctor.interfaces.ExtraFeeCallBack
import com.app.hakeemDoctor.models.StatData
import com.app.hakeemDoctor.models.TransactionData
import com.app.hakeemDoctor.utils.*
import com.app.hakeemDoctor.view.activity.DashBoardActivity
import com.app.hakeemDoctor.view.adapter.TransactionAdapter
import com.app.hakeemDoctor.viewmodel.BookingViewModel
import com.paypal.android.sdk.payments.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class WalletFragment : Fragment(R.layout.fragment_wallet) {

    var binding: FragmentWalletBinding? = null
    var sharedHelper: SharedHelper? = null
    private var xAxisFormatter: YearXAxisFormatter? = null

    var bookingViewModel: BookingViewModel? = null
    var chartData: ArrayList<StatData>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentWalletBinding.inflate(inflater, container, false)
        val view = binding!!.root
        sharedHelper = SharedHelper(requireContext())
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)

        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // isEnabled = false
                // activity?.onBackPressed()
                (activity as DashBoardActivity?)?.setAdapter(0)
            }
        })
        getStatValues()

        /* if(sharedHelper?.status == true){
             customSwitch1.isChecked = true
             statuson()
         }
         customSwitch1.setOnCheckedChangeListener { buttonView, isChecked ->
             *//* val msg: String = if (isChecked)
                 "Switch Button is Checked"
             else
                 "Switch Button is UnChecked"
             showToast(msg)*//*
            if(isChecked) {
                sharedHelper?.status = true
                statuson()
            }
            else{
                sharedHelper?.status = false
            }
        }*/
        return view
    }

    private fun statuson(){
        (activity as DashBoardActivity?)?.updateDeviceToken()
    }

    private fun getStatValues() {

        DialogUtils.showLoader(requireContext())
        bookingViewModel?.getWalletHistory(requireContext(), BaseUtils.getCurrentDate("yyyy-MM-dd"))
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.root, msg)
                                }
                            } else {
                                it.data?.let { data ->
                                    data.history?.let { list ->

                                        handleResponse(list)
                                    }

                                }
                            }
                        }

                    }
                })
    }

    private fun handleResponse(data: ArrayList<StatData>) {

        var calculatedList = ArrayList<StatData>()


        for (i in 0 until data.size) {
            data[i].createdAt?.let {

                BaseUtils.getFormatedDate(it, "yyyy-MM-dd'T'HH:mm:ss", "EEE").let { value ->
                    when {
                        value.equals("Mon", true) -> {
                            data[i].day = "Mo"
                        }
                        value.equals("Tue", true) -> {
                            data[i].day = "Tu"
                        }
                        value.equals("Wed", true) -> {
                            data[i].day = "We"
                        }
                        value.equals("Thu", true) -> {
                            data[i].day = "Th"
                        }
                        value.equals("Fri", true) -> {
                            data[i].day = "Fr"
                        }
                        value.equals("Sat", true) -> {
                            data[i].day = "Sa"
                        }
                        value.equals("Sun", true) -> {
                            data[i].day = "Su"
                        }
                    }

                }

                BaseUtils.getFormatedDate(it, "yyyy-MM-dd'T'HH:mm:ss", "EEEE,MMM dd,yyyy")
                    .let { convertedVal ->
                        data[i].createdAt = convertedVal
                    }

            }

        }

        var calendar = Calendar.getInstance()
        var dayList = arrayListOf<String>()

        for (i in 0..6) {
            dayList.add(BaseUtils.calenderToDate(calendar, "EEEE,MMM dd,yyyy"))
            calendar.add(Calendar.DATE, -1)
        }

        dayList.reversed()

        for (i in 6 downTo 0) {
            calculatedList.add(getSumOnDay(data, dayList[i]))
        }

        initView(calculatedList)
        setStatAdapter(calculatedList)
        setViewData(calculatedList)
    }

    private fun setViewData(calculatedList: java.util.ArrayList<StatData>) {

        try {
            binding!!.amount.text = calculatedList[6].amount.toString() + " SAR"
            binding!!.dateandTime.text = calculatedList[6].createdAt.toString()
            /*amount.text = "100 SAR"
            dateandTime.text = "10-08-1999"*/
        } catch (e: ArrayIndexOutOfBoundsException) {

        }


    }

    private fun getSumOnDay(list: ArrayList<StatData>, date: String): StatData {

        var sum = 0f
        var dayV = BaseUtils.getFormatedDate(date, "EEEE,MMM dd,yyyy", "EEE")

        for (i in 0 until list.size) {
            if (list[i].createdAt.equals(date, true)) {
                list[i].amount?.let {
                    sum += it
                    dayV = list[i].day
                }

            }
        }

        var returnData = StatData()

        return returnData.apply {
            createdAt = date
            amount = sum
            day = dayV
        }


    }

    private fun initView(dayList: ArrayList<StatData>) {


        var mMonths: Array<String?> = arrayOf("mon", "tue", "wed", "thu", "fri", "sat", "sun")

        for (i in 0 until dayList.size) {
            mMonths[i] = dayList[i].day
        }

        binding!!.chart.setDrawBarShadow(false)
        binding!!.chart.setDrawBorders(false)
        binding!!.chart.setDrawValueAboveBar(true)
        binding!!.chart.setScaleEnabled(false)
        binding!!.chart.setPinchZoom(false)
        binding!!.chart.setDrawGridBackground(false)
        binding!!.chart.description.isEnabled = false
        binding!!.chart.setMaxVisibleValueCount(7)
        binding!!.chart.axisLeft.setDrawGridLines(false)


        binding!!.chart.axisLeft.setDrawLabels(false)
        binding!!.chart.axisRight.setDrawLabels(false)

        xAxisFormatter = YearXAxisFormatter(sharedHelper?.language)
        xAxisFormatter?.mMonths = mMonths
        binding!!.chart.xAxis.isGranularityEnabled = true


        binding!!.chart.xAxis.typeface = ResourcesCompat.getFont(requireContext(), R.font.opensans_regular)
        binding!!.chart.axisLeft.setDrawAxisLine(false)
        binding!!.chart.axisRight.setDrawGridLines(false)
        binding!!.chart.axisRight.setDrawAxisLine(false)
        binding!!.chart.xAxis.setDrawGridLines(false)
        binding!!.chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        binding!!.chart.xAxis.setDrawLabels(true)
        binding!!.chart.xAxis.setDrawAxisLine(false)


        binding!!.chart.legend.isEnabled = false
        binding!!.chart.animateXY(2000, 2000)
    }

    private fun setStatAdapter(calculatedList: ArrayList<StatData>) {
        chartData = calculatedList

        val barEntries = java.util.ArrayList<BarEntry>()
        for (i in 0..6) {
            barEntries.add(i, BarEntry(i.toFloat(), 2f))
        }

        for (i in 0..6) {
            barEntries[i] =
                BarEntry(i.toFloat(), calculatedList[i].amount!!.toFloat())
        }

        val values: ArrayList<BarEntry> = ArrayList()
        val set1: BarDataSet
        set1 = BarDataSet(values, resources.getString(R.string.earnings))
        set1.highLightColor = resources.getColor(R.color.colorPrimary)
        set1.highLightAlpha = 255
        set1.setDrawIcons(false)
        val colors: MutableList<Int> = ArrayList()


        colors.add(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
        val startColor1 = ContextCompat.getColor(requireContext(), R.color.input_field_bg_color)
        val endColor1 = ContextCompat.getColor(requireContext(), R.color.input_field_bg_color)

        val gradientColors: MutableList<GradientColor> = ArrayList()
        gradientColors.add(GradientColor(startColor1, endColor1))


        set1.gradientColors = gradientColors
        set1.values = barEntries
        val dataSets: ArrayList<IBarDataSet> = ArrayList<IBarDataSet>()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.setValueTextSize(7f)
        data.barWidth = 0.6f
        data.isHighlightEnabled = true

        binding!!.chart.zoom(1.2f, 0f, 0f, 0f)
        binding!!.chart.data = data
        binding!!.chart.xAxis.valueFormatter = xAxisFormatter
        binding!!.chart.invalidate()

        binding!!.chart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onNothingSelected() {


            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {
                e?.x?.let {
                    try {
                        binding!!.amount.text = chartData?.get(it.toInt())?.amount.toString() + " SAR"
                        binding!!.dateandTime.text = chartData?.get(it.toInt())?.createdAt.toString()
                    } catch (e: ArrayIndexOutOfBoundsException) {

                    }

                }


            }

        })

    }


}
