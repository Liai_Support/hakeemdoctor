package com.app.hakeemDoctor.view.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemDoctor.databinding.ActivityViewLabReportBinding
import com.app.hakeemDoctor.models.LabReportJson
import com.app.hakeemDoctor.models.LabReportJson2
import com.app.hakeemDoctor.models.ReportData
import com.app.hakeemDoctor.view.adapter.LabReportAdapter

class ViewLabReportActivity : BaseActivity() {
    var binding: ActivityViewLabReportBinding? = null
    var testnamee: MutableList<String> = mutableListOf<String>()
    var testnamee2: MutableList<String> = mutableListOf<String>()
    var myMaps: Map<String, ArrayList<String>> = HashMap()
    var listt: List<Map<String, List<String>>> = ArrayList()
    var labReportJson: java.util.ArrayList<LabReportJson>? =
    java.util.ArrayList<LabReportJson>()
    var labReportJson2: java.util.ArrayList<LabReportJson2>? =
        java.util.ArrayList<LabReportJson2>()
    var map1: Map<String, List<String>> =
        HashMap()

    var arraylist1: List<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewLabReportBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
       // setContentView(R.layout.activity_view_lab_report)
        var data: ArrayList<ReportData>? = null

        val lab: ArrayList<ReportData> = intent.getSerializableExtra("labreports") as ArrayList<ReportData>




        for (i in 0 until  lab.size){

            testnamee.add(lab[i].test_name.toString())
            Log.d("etdyteywe",""+testnamee)
        }

        testnamee2=testnamee.distinct().toMutableList()
        Log.d("sdxfggvbhn",""+testnamee2)



        for (i in 0 until testnamee2.size){
            var reportname: MutableList<String> = mutableListOf<String>()


            for (k in 0 until lab.size){


                if (testnamee2[i]==lab[k].test_name){

                    if (reportname.size==0){
                        reportname.add(lab[k].report!!)

                        Log.d("qweewewew",""+reportname)


                    }else{
                        for (j in 0 until reportname.size){
                            val m:Int=0
                            Log.d("ettete",""+lab[k])
                            Log.d("ettete",""+k)
                            Log.d("ettete",""+j)
                             Log.d("tetettete", (""+reportname[j]==lab[k].report).toString())
                            if (reportname[j]==lab[k].report){

                            }else{
                                reportname.add(lab[k].report!!)
                                break
                            }
                        }


                    }


                }


            }


             reportname=reportname.distinct().toMutableList()
            labReportJson2!!.add(LabReportJson2(testnamee2[i], reportname))
            Log.d("ccccc",""+labReportJson2)




        }





    /*    val sortedList = labReportJson!!.sortedWith(compareBy({ it.tesname }, { it.tesname }))

        Log.d("asdfgthyju",""+sortedList)
        for (i in 0 until  labReportJson!!.size){


            for (k in 0 until  lab!!.size){

                if (lab[i].test_name== labReportJson!![i].tesname){
                    Log.d("sbwhhbshjbs",""+lab[k].report)
                //   Log.d("bvvbvbbvvbvbvb",""+ labReportJson!![i].Report)


                }


            }

             break

        }
*/


        /*     val sortedList = lab.sortedWith(compareBy({ it.test_name }, { it.test_name }))
             lab.clear()
             lab += sortedList
             Log.d("wuidhwiusihjow",""+lab[0].report)
             Log.d("wuidhwiusihjow",""+lab[1].test_name)
             Log.d("wuidhwiusihjow",""+lab[2])*/

/*
        for (i in 0 until  lab.size){
            Log.d("etdyteywe",""+lab[i].test_name)

            lab[i].test_name?.let { lab[i].report?.let { it1 -> LabReportJson(it, it1) } }?.let { labReportJson!!.add(it) }
            Log.d("nsnshdxjnmaaa", "" + labReportJson)


        }

        for (k in 0 until lab.size){


            for (i in 0 until labReportJson!!.size){

                if (labReportJson!![i].tesname == lab[k].test_name){
                    reportname.add(lab[k].report!!)



                }

                labReportJson2!!.add(LabReportJson2(labReportJson!![i].tesname, reportname))
                Log.d("nsnshdxjnmaaa", "" + labReportJson2)



            }

        }
*/





       /*  for (i in 0 until testnamee2.size){
             for (k in 0 until lab.size){

                 if (testnamee2[i] == lab[k].test_name){
                     reportname.add(lab[k].report!!)



                 }
                 labReportJson!!.add(LabReportJson(testnamee2[i], reportname))
                 Log.d("nsnshdxjnmaaa", "" + labReportJson)

             }



         }*/

        /*    for (j in 0 until  lab.size){
                for (i in 0 until  testnamee2.size) {
                    if (testnamee2[i] == lab[j].test_name) {

                        Log.d("aAaAaAaAA", "" + lab[j].report)
                        Log.d("bnxbnbnsbsn", ("" + testnamee2[i] == lab[j].test_name).toString())

                        reportname.add(lab[j].report!!)

                        labReportJson!!.add(LabReportJson(testnamee2[i], reportname))
                        Log.d("nsnshdxjnmaaa", "" + labReportJson)

                    }

                }




          //  reportname.clear()

        }*/






        binding!!.labreports.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding!!.labreports.adapter = LabReportAdapter(this@ViewLabReportActivity,this, lab,labReportJson2)
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }
}