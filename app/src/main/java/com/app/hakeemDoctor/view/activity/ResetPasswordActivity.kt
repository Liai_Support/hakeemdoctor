package com.app.hakeemDoctor.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityResetPasswordBinding
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.SharedHelper
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.viewmodel.RegisterViewModel

class ResetPasswordActivity : BaseActivity() {

    var binding: ActivityResetPasswordBinding? = null
    private var viewmodel: RegisterViewModel? = null
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResetPasswordBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedHelper = SharedHelper(this)

    }


    private fun isValidInputs(): Boolean {

        when {

            binding!!.newPassword.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourpassword))
                return false
            }
            binding!!.confirmPassword.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourconfirmpassword))
                return false
            }
            binding!!.confirmPassword.text.toString().trim() != binding!!.newPassword.text.toString().trim() -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.confirmpasswordshouldbesameaspassword))
                return false
            }
            else -> return true
        }

    }

    fun onResetPasswordClicked(view: View) {


        if (isValidInputs()) {
            DialogUtils.showLoader(this)
            viewmodel?.resetPassword(binding!!.newPassword.text.toString().trim())?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            val intent =
                                Intent(this, LoginActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                        }
                    }

                }
            })
        }
    }

}