package com.app.hakeemDoctor.view.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemDoctor.R
import androidx.lifecycle.Observer
import com.app.hakeemDoctor.barchart.utils.YearXAxisFormatter
import com.app.hakeemDoctor.databinding.*

import com.app.hakeemDoctor.interfaces.ExtraFeeCallBack
import com.app.hakeemDoctor.models.ConsultationHistoryData
import com.app.hakeemDoctor.models.StatData
import com.app.hakeemDoctor.models.TransactionData
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.SharedHelper
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.view.activity.DashBoardActivity
import com.app.hakeemDoctor.view.adapter.PatientConsultationHistoryAdapter
import com.app.hakeemDoctor.view.adapter.TransactionAdapter
import com.app.hakeemDoctor.viewmodel.BookingViewModel
import com.paypal.android.sdk.payments.*
import org.json.JSONException
import org.json.JSONObject
import java.math.BigDecimal
import java.util.*
import kotlin.Comparator

class PatientConsultationHistoryFragment : Fragment(R.layout.fragment_consultation_history) {

    var binding: FragmentConsultationHistoryBinding? = null

    var viewModel: BookingViewModel? = null
    var sharedHelper: SharedHelper? = null
    var datee:String=""
    var year:String=""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentConsultationHistoryBinding.inflate(inflater, container, false)
        val view = binding!!.root
        sharedHelper = SharedHelper(requireContext())
        viewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        getIntentValues()
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as DashBoardActivity?)?.setAdapter(0)
            }
        })
        return view
    }

    private fun getIntentValues() {

        requireArguments()?.let {
            it.getString(Constants.IntentKeys.DATE)?.let { date ->
                initobserver(date)
             /*    datee=date.substring(8,10)
                 year=date.substring(0,4)
                Log.d("asZSDazdszads",""+date.substring(8,10))
                Log.d("asZSDazdszads",""+date.substring(0,4))*/
                datee=date.substring(8,10)
                year=date.substring(0,4)
                Log.d("kanika",""+date)
            }
            it.getString(Constants.IntentKeys.DISPLAYMONTH)?.let { mont ->
                binding!!.month.text = datee+ " "+mont+" "+year
            }
        }



    }

    private fun initobserver(date: String) {
        DialogUtils.showLoader(requireContext())
        viewModel?.getConsultationHistory(requireContext(), date)?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            if (data.size != 0)
                                handleResponse(data)
                        }
                    }
                }


            }
        })


    }

    private fun handleResponse(data: ArrayList<ConsultationHistoryData>) {
        Collections.sort(data,
            Comparator<ConsultationHistoryData?> { o1, o2 ->
                Integer.compare(
                    o2!!.bookingId!!,
                    o1!!.bookingId!!
                )
            })

        var consultationAdapter = sharedHelper?.latitude?.let {
            PatientConsultationHistoryAdapter(
                requireContext(),
                data,
                it,
                sharedHelper!!.longitude
            )
        }
        binding?.historyListView?.layoutManager = LinearLayoutManager(requireContext())
        binding?.historyListView?.adapter = consultationAdapter

    }


    fun onBackPressed(view: View) {
        // onBackPressed()
    }

}
