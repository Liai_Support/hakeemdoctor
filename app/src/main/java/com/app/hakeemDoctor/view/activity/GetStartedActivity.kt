package com.app.hakeemDoctor.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.app.hakeemDoctor.R

class GetStartedActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_started)

    }

    fun onStartedClicked(view: View) {

        startActivity(Intent(this,
            LoginActivity::class.java))
        finish()
    }

}