package com.app.hakeemDoctor.view.activity

import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.SharedHelper
import com.app.hakeemDoctor.viewmodel.HomeViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.Task
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class SplashActivity : BaseActivity() {
    private var appUpdateManager: AppUpdateManager? = null
    private var installStateUpdatedListener: InstallStateUpdatedListener? = null
    private var FLEXIBLE_APP_UPDATE_REQ_CODE = 123
    var viewmodel: HomeViewModel? = null
    var version = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       setContentView(R.layout.activity_splash)
        appUpdateManager = AppUpdateManagerFactory.create(applicationContext)
        installStateUpdatedListener = InstallStateUpdatedListener { state: InstallState ->
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackBarForCompleteUpdate()
            } else if (state.installStatus() == InstallStatus.INSTALLED) {
                removeInstallStateUpdateListener()
            } else {
                Toast.makeText(
                    applicationContext,
                    "InstallStateUpdatedListener: state: " + state.installStatus(),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        appUpdateManager!!.registerListener(installStateUpdatedListener)
        checkUpdate()

        printHashKey()

        viewmodel = ViewModelProvider(this).get(HomeViewModel::class.java)
       // getcheckversion()
        GetAppVersion(this)
        getcheckversion()
    }

    override fun onResume() {
        super.onResume()


    }

    fun printHashKey() {
        try {
            val info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.e("keyhash", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("keyhash", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("keyhash", "printHashKey()", e)
        }

    }


    private fun checkUpdate() {
        val appUpdateInfoTask: Task<AppUpdateInfo> = appUpdateManager!!.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() === UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
            ) {
                startUpdateFlow(appUpdateInfo)
            } else if (appUpdateInfo.installStatus() === InstallStatus.DOWNLOADED) {
                popupSnackBarForCompleteUpdate()
            }
        }
    }

    private fun startUpdateFlow(appUpdateInfo: AppUpdateInfo) {
        try {
            appUpdateManager!!.startUpdateFlowForResult(
                appUpdateInfo,
                AppUpdateType.FLEXIBLE,
                this,
                FLEXIBLE_APP_UPDATE_REQ_CODE
            )
        } catch (e: IntentSender.SendIntentException) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FLEXIBLE_APP_UPDATE_REQ_CODE) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(
                    applicationContext,
                    "Update canceled by user! Result Code: $resultCode", Toast.LENGTH_LONG
                ).show()
            } else if (resultCode == RESULT_OK) {
                Toast.makeText(
                    applicationContext,
                    "Update success! Result Code: $resultCode", Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    applicationContext,
                    "Update Failed! Result Code: $resultCode",
                    Toast.LENGTH_LONG
                ).show()
                checkUpdate()
            }
        }
    }

    private fun popupSnackBarForCompleteUpdate() {
        Snackbar.make(
            findViewById<View>(android.R.id.content).rootView,
            "New app is ready!",
            Snackbar.LENGTH_INDEFINITE
        )
            .setAction("Install") { view: View? ->
                if (appUpdateManager != null) {
                    appUpdateManager!!.completeUpdate()
                }
            }
            .setActionTextColor(resources.getColor(android.R.color.black))
            .show()
    }

    private fun removeInstallStateUpdateListener() {
        if (appUpdateManager != null) {
            appUpdateManager!!.unregisterListener(installStateUpdatedListener)
        }
    }

    override fun onStop() {
        super.onStop()
        removeInstallStateUpdateListener()
    }




    fun GetAppVersion(context: Context): String {

        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            version = pInfo.versionName

            Log.d("kanikashanmugam",""+pInfo.versionName)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return version
    }

    private fun getcheckversion() {
        DialogUtils.showLoader(this)
        viewmodel?.getcheckversion(this)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {

                    } else {
                        it.data?.let { data ->
                            it.data!!.version?.let { data ->
                                data.android_patient?.let { it1 -> Log.d("wertyuio", it1) }
                                if (version<data.android_patient!!){
                                    val builder = AlertDialog.Builder(this)
                                    builder.setTitle(getString(R.string.alert))
                                    builder.setMessage(getString(R.string.youhaveupdateinyourapplicationpleaseclickoktoupdateit))
                                    builder.setCancelable(false)
                                    builder.setPositiveButton(android.R.string.ok) { dialog, which ->
                                        val uri: Uri = Uri.parse("market://details?id=$packageName")
                                        val myAppLinkToMarket = Intent(Intent.ACTION_VIEW, uri)
                                        try {
                                            startActivity(myAppLinkToMarket)
                                        } catch (e: ActivityNotFoundException) {
                                            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show()
                                        }
                                    }
                                    builder.setNegativeButton(android.R.string.cancel) { dialog, which ->
                                        builder.setCancelable(true)
                                        Handler().postDelayed({
                                            if (SharedHelper(this).loggedIn) {
                                                startActivity(Intent(this, DashBoardActivity::class.java))
                                            } else {
                                                startActivity(Intent(this, LoginActivity::class.java))
                                            }
                                            finish()
                                        }, 2000)
                                    }

                                    builder.show()
                                }else{
                                    Handler().postDelayed({
                                        if (SharedHelper(this).loggedIn) {
                                            startActivity(Intent(this, DashBoardActivity::class.java))
                                        } else {
                                            startActivity(Intent(this, LoginActivity::class.java))
                                        }
                                        finish()
                                    }, 2000)
                                }
                            }
                        }
                    }
                }

            }
        })
    }
}