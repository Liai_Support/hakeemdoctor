package com.app.hakeemDoctor.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ChildFeedbackBinding
import com.app.hakeemDoctor.models.ReviewData
import com.app.hakeemDoctor.utils.BaseUtils
import com.app.hakeemDoctor.utils.UiUtils
import java.util.*

class FeedbackAdapter(
    var context: Context,
    var data: ArrayList<ReviewData>
) :
    RecyclerView.Adapter<FeedbackAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildFeedbackBinding = ChildFeedbackBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_feedback,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        data[position].name?.let { holder.binding.name.text = it }
        data[position].rating?.let {
            holder.binding.ratingValue.text = it
            UiUtils.setRating(context, it.toFloat(), holder.binding.rating, 45)
        }


        data[position].createdAt?.let {
            holder.binding.dateAndTime.text =
                BaseUtils.getFormatedDate(it, "yyyy-MM-dd'T'HH:mm:ss", "EEE,dd MMMM yyyy")
        }

        data[position].review?.let { holder.binding.comment.text = it }
    }

}