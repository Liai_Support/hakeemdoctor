package com.app.hakeemDoctor.view.fragment

import android.R.attr.fragment
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.FragmentEarningsBinding
import com.app.hakeemDoctor.databinding.FragmentProfile1Binding
import com.app.hakeemDoctor.interfaces.OnClickListener
import com.app.hakeemDoctor.models.ProfileDetails
import com.app.hakeemDoctor.utils.*
import com.app.hakeemDoctor.view.activity.DashBoardActivity
import com.app.hakeemDoctor.view.activity.NotificationActivity
import com.app.hakeemDoctor.viewmodel.AmazonViewModel
import com.app.hakeemDoctor.viewmodel.ProfileViewModel
import java.io.File
import java.util.*
import kotlin.system.exitProcess


class ProfileFragment : Fragment(R.layout.fragment_profile1) {

    var binding: FragmentProfile1Binding? = null
    private var profileViewModel: ProfileViewModel? = null
    private var amazonViewModel: AmazonViewModel? = null
    var sharedHelper: SharedHelper? = null
    var uploadFile: File? = null
    private var profileImageUrl = "nil"
    var cCode = "966"
    var mobile = ""
    var email = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentProfile1Binding.inflate(inflater, container, false)
        val view = binding!!.root
        sharedHelper = SharedHelper(requireContext())
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        amazonViewModel = ViewModelProvider(this).get(AmazonViewModel::class.java)
        initListeners()
        getProfileData()
        enableEdit(false)

        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // isEnabled = false
                // activity?.onBackPressed()
                (activity as DashBoardActivity?)?.setAdapter(0)
            }
        })

        sharedHelper?.language?.let {
            if (it == "en") {
            }
            else{
                binding!!.contactNumber.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.emailId.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.city.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.qualification1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.experience1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.speciality.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.council.gravity = View.TEXT_ALIGNMENT_VIEW_START

                binding!!.edtContactNumber.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtEmailId.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtCity.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtQualification1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtExperience1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtCouncil.gravity = View.TEXT_ALIGNMENT_VIEW_START
            }
        }

        /* if(sharedHelper?.status == true){
             customSwitch1.isChecked = true
             statuson()
         }
         customSwitch1.setOnCheckedChangeListener { buttonView, isChecked ->
             *//* val msg: String = if (isChecked)
                 "Switch Button is Checked"
             else
                 "Switch Button is UnChecked"
             showToast(msg)*//*
            if(isChecked) {
                sharedHelper?.status = true
                statuson()
            }
            else{
                sharedHelper?.status = false
            }
        }*/
        return view
    }

    private fun statuson(){
        (activity as DashBoardActivity?)?.updateDeviceToken()
    }

    /*fun onBackPressed(view: View) {
      //  activity?.onBackPressed()
       // startActivity(Intent(requireActivity(), NotificationActivity::class.java))
        Log.d("bcgf","vhvh");
    }*/






    private fun initListeners() {

        binding!!.editImage.setOnClickListener {
            DialogUtils.showPictureDialog(requireActivity())
        }

        binding!!.back.setOnClickListener{
            (getActivity() as DashBoardActivity?)?.setAdapter(0)
           // startActivity(Intent(requireActivity(), DashBoardActivity::class.java))
        }

        binding!!.notification.setOnClickListener {
            startActivity(Intent(requireActivity(), NotificationActivity::class.java))
        }



        binding!!.editProfile.setOnClickListener {
            if (binding!!.editProfile.text.toString() == resources.getString(R.string.edit_profile)) {
                enableEdit(true)
            } else {
                if (isValidInputs()) {
                    DialogUtils.showLoader(requireContext())
                    if (uploadFile != null) {
                        amazonViewModel?.uploadImage(requireContext(), uploadFile!!)
                            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {

                                it.error?.let { value ->
                                    if (!value) {
                                        updateProfile(it.message)
                                    } else {
                                        DialogUtils.dismissLoader()
                                        //updateProfile("nil")
                                    }
                                }
                            })
                    } else {
                        updateProfile(profileImageUrl)
                    }


                }
            }

        }

        binding!!.male.setOnClickListener{
            binding!!.male.isChecked = true
            binding!!.female.isChecked = false
        }

        binding!!.female.setOnClickListener{
            binding!!.male.isChecked = false
            binding!!.female.isChecked = true
        }

    }

    private fun selectDob() {


        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(
            requireActivity(),
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                //                dob.text = StringBuilder().append(BaseUtils.numberFormat(dayOfMonth)).append("-")
//                    .append(BaseUtils.numberFormat(monthOfYear + 1)).append("-")
//                    .append(year)

            },
            year,
            month,
            day
        )

        dpd.show()
    }

    private fun showBloodGrpDialog() {

        var list = arrayListOf("A+ve", "A-ve", "B+ve", "B-ve", "AB+ve", "AB-ve", "O+ve", "O-ve")

        DialogUtils.showListDialog(requireContext(), list, "Choose Blood Group", object :
            OnClickListener {
            override fun onClickItem(position: Int) {
//                bloodGroup.text = list[position]
            }

        })

    }

    private fun updateProfile(profilePic: String?) {


        DialogUtils.showLoader(requireContext())
        profileViewModel?.updateProfile(
            requireContext(),
            binding!!.edtName.text.toString(),
            email,
            cCode,
            mobile,
            getGender(),
            binding!!.edtCity.text.toString(),
            binding!!.edtQualification1.text.toString(),
            binding!!.edtCouncil.text.toString(),
            binding!!.edtExperience1.text.toString(),
            profilePic
        )?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            DialogUtils.dismissLoader()
            it.error?.let { error ->
                if (error) {
                    it.message?.let { message -> UiUtils.showSnack(binding!!.root, message) }
                } else {
                    profilePic?.let { value -> sharedHelper?.userImage = value }
                    successResponse()
                }
            }
        })

    }

    private fun successResponse() {

        binding!!.editImage.visibility = View.GONE
        enableEdit(false)
        getProfileData()
    }

    private fun getGender(): String {
        return when {
            binding!!.male.isChecked -> {
                "male"
            }
            binding!!.female.isChecked -> {
                "female"
            }
            else -> {
                ""
            }
        }
    }

    private fun isValidInputs(): Boolean {

        if (binding!!.name.text.toString() == "") {
            UiUtils.showSnack(binding!!.root, getString(R.string.enteryourname))
            return false
        } else if (!binding!!.male.isChecked && !binding!!.female.isChecked) {
            UiUtils.showSnack(binding!!.root, getString(R.string.selectyourgender))
            return false
        } else if (binding!!.edtCity.text.toString() == "") {
            UiUtils.showSnack(binding!!.root, getString(R.string.enteryourlocationname))
            return false
        } else if (binding!!.edtQualification1.text.toString() == "") {
            UiUtils.showSnack(binding!!.root, getString(R.string.enteryourqualification))
            return false
        } else if (binding!!.edtCouncil.text.toString() == "") {
            UiUtils.showSnack(binding!!.root, getString(R.string.enteryourregistrationcouncil))
            return false
        } else if (binding!!.edtExperience1.text.toString() == "") {
            UiUtils.showSnack(binding!!.root, getString(R.string.enteryourexperience))
            return false
        } else {
            return true
        }

    }

    private fun getProfileData() {
        DialogUtils.showLoader(requireContext())
        profileViewModel?.getProfileDetails(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->

                                if (data.size != 0) {
                                    handleResponse(data[0])

                                }

                            }
                        }
                    }

                }
            })
    }

    private fun handleResponse(data: ProfileDetails) {

        data.name?.let {
            binding!!.name.text = it
            binding!!.edtName.setText(it)
            sharedHelper?.name = it
            (activity as DashBoardActivity?)!!.binding!!.homeDrawer.patientName1.text = sharedHelper?.name
        }
        data.countryCode?.let { ccpVal ->
             Log.d("sedrtyuiop",""+data.countryCode)
            if(ccpVal == "0"){
                cCode="966"
            }else{
                cCode = ccpVal
                data.mobileNumber?.let {
                    binding!!.contactNumber.text = "+$ccpVal  $it"
                    mobile = it
                }
            }

        }

        data.mobileNumber?.let {
            binding!!.contactNumber.text = "+"+cCode+" "+it
            mobile = it
        }

        data.profilePic?.let {
            profileImageUrl = it
            sharedHelper?.userImage = it
            UiUtils.loadImage(binding!!.profilePicture, it)
            UiUtils.loadImage((activity as DashBoardActivity?)!!.binding!!.homeDrawer.circleImageView1, it)
        }
        data.email?.let {
            binding!!.emailId.text = it
            email = it
        }

        data.specialityName?.let {
            binding!!.speciality.text = it
        }

        data.hospital?.let {
            binding!!.hospital.text = it
        }

        data.city?.let {
            binding!!.city.setText(it)
            binding!!.edtCity.setText(it)
        }

        data.education?.let {
            binding!!.qualification1.setText(it)
            binding!!.edtQualification1.setText(it)
        }
        data.registrationCouncil?.let {
           // if (!it.contains("not", true))
            binding!!.council.setText(it)
            binding!!.edtCouncil.setText(it)

        }
        data.expierence?.let {
           // if (!it.contains("not", true))
            binding!!.experience1.setText(it)
            binding!!.edtExperience1.setText(it)
        }
        data.gender?.let {
            if (it.equals("male", true)) {
                binding!!.male.isChecked = true
            } else if (it.equals("female", true)) {
                binding!!.female.isChecked = true
            }
        }

        enableEdit(false)
    }

    private fun enableEdit(isEnable: Boolean) {

        binding!!.name.isEnabled = isEnable
        binding!!.male.isEnabled = isEnable
        binding!!.female.isEnabled = isEnable
        binding!!.city.isEnabled = isEnable
        binding!!.qualification1.isEnabled = isEnable
        binding!!.council.isEnabled = isEnable
        binding!!.experience1.isEnabled = isEnable

        if (isEnable)
            binding!!.editProfile.text = resources.getString(R.string.update)
        else
            binding!!.editProfile.text = resources.getString(R.string.edit_profile)

        binding!!.editImage.visibility =
            if (isEnable)
                View.VISIBLE
            else
                View.GONE

        if(isEnable){
            binding!!.name.visibility = View.GONE
            binding!!.city.visibility = View.GONE
            binding!!.qualification1.visibility = View.GONE
            binding!!.council.visibility = View.GONE
            binding!!.experience1.visibility = View.GONE

            binding!!.edtName.visibility = View.VISIBLE
            binding!!.edtCity.visibility = View.VISIBLE
            binding!!.edtQualification1.visibility = View.VISIBLE
            binding!!.edtCouncil.visibility = View.VISIBLE
            binding!!.edtExperience1.visibility = View.VISIBLE
        }
        else{
            binding!!.name.isEnabled = true
            binding!!.city.isEnabled = true
            binding!!.qualification1.isEnabled = true
            binding!!.council.isEnabled = true
            binding!!.experience1.isEnabled = true
            binding!!.name.visibility = View.VISIBLE
            binding!!.city.visibility = View.VISIBLE
            binding!!.qualification1.visibility = View.VISIBLE
            binding!!.council.visibility = View.VISIBLE
            binding!!.experience1.visibility = View.VISIBLE

            binding!!.edtName.visibility = View.GONE
            binding!!.edtCity.visibility = View.GONE
            binding!!.edtQualification1.visibility = View.GONE
            binding!!.edtCouncil.visibility = View.GONE
            binding!!.edtExperience1.visibility = View.GONE
        }


    }

    fun onActivityResults(requestCode: Int, resultCode: Int, data: Intent?) {
        if(!isDetached)
        when (requestCode) {
            Constants.RequestCode.GALLERY_INTENT//gallery
            -> if (resultCode == AppCompatActivity.RESULT_OK) {
                handleGallery(data)
            }
            Constants.RequestCode.CAMERA_INTENT//camera
            -> if (resultCode == AppCompatActivity.RESULT_OK) {
                handleCamera()
            }

        }
    }

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                UiUtils.loadImage(
                    binding!!.profilePicture,
                    BaseUtils.getRealPathFromUriNew(requireContext(), uri),
                    ContextCompat.getDrawable(requireContext(), R.drawable.doctor)!!
                )
                uploadFile = File(BaseUtils.getRealPathFromURI(requireContext(), uri)!!)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_select))
            }
        }
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper!!.imageUploadPath)
        if (uploadFile!!.exists()) {
            UiUtils.loadImage(
                binding!!.profilePicture,
                sharedHelper!!.imageUploadPath,
                ContextCompat.getDrawable(requireContext(), R.drawable.doctor)!!
            )
        } else {
            UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_retrieve))
        }
    }

    fun onRequestPermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (!isDetached)
            when (requestCode) {

                Constants.Permission.READ_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    BaseUtils.openGallery(requireActivity())
                } else {
                    UiUtils.showSnack(binding!!.root, getString(R.string.storage_permission_error))
                }

                Constants.Permission.CAMERA_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                ) {
                    BaseUtils.openCamera(requireActivity())
                } else {
                    UiUtils.showSnack(binding!!.root, getString(R.string.camera_permission_error))
                }
            }
    }

}