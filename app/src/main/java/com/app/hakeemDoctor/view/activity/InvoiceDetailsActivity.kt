package com.app.hakeemDoctor.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivitySummaryBinding
import com.app.hakeemDoctor.models.*
import com.app.hakeemDoctor.utils.BaseUtils
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.viewmodel.BookingViewModel

class InvoiceDetailsActivity : BaseActivity() {

    var binding: ActivitySummaryBinding? = null
    var viewModel: BookingViewModel? = null
    var bookingid = 0
    var datee = ""
    var time = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySummaryBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_summary)

        viewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        getIntentValues()
        binding!!.visitCompleted.visibility = View.GONE

        binding!!.docName.text = getString(R.string.consultationfee)
        binding!!.textView37.text = getString(R.string.amount_paid)
        // binding!!.textView.text = getString(R.string.invoice)
    }

    private fun getIntentValues() {

        intent.extras?.let {
            it.getParcelable<ConsultationHistoryData>(Constants.IntentKeys.PAYMENTDETAIL)?.let { data ->

                bookingid = data.bookingId!!.toInt()
                Log.d("xcdfgvhjk",""+data.noteContent)

                data.patientName?.let { binding!!.patientName.text = it }
                data.noteContent?.let { binding!!.textViewnote.text = it }
                data.patientAge?.let { binding!!.yearsOld.text = "$it years old" }
                data.previousIssue?.let { binding!!.healthIssue.text = it }
                binding!!.healthIssue.text = ""
                binding!!.healthIssueText.visibility = View.INVISIBLE
                data.DOB?.let { binding!!.yearsOld.text = "${BaseUtils.getAgeCalculation(it)} years old" }

              //  data.providerName?.let { binding!!.docName.text = it }
                data.location?.let { binding!!.location.text = it }


             /*   data.countryCode?.let { code ->
                    value.mobileNumber?.let {
                        binding!!.phoneNumber.text = "+$code $it"
                    }
                }

                data.email?.let { binding!!.mailId.text = it }*/
                binding!!.mailId.text = ""
                binding!!.phoneNumber.text = ""
                binding!!.textView46.visibility = View.INVISIBLE
                binding!!.textView47.visibility = View.INVISIBLE



                data.bookingTime?.let {

                  time=  BaseUtils.getFormatedDate(it, "HH:mm:ss", "hh:mm a")

                   /* binding!!.time.text =
                        BaseUtils.getFormatedDate(it, "HH:mm:ss", "hh:mm a")*/
                }
                data.bookingDate?.let {

                    datee=  BaseUtils.getFormatedDate(it, "yyyy-MM-dd", "dd MMMM yyyy")

                    binding!!.date.text = datee+" "+time

                }

                data.fee?.let {
                    if (data.extraFee != null) {

                        binding!!.singleConsultation.text = "${it.toInt()} SAR"
                        binding!!.consultationAmount.text = "${data.extraFee} SAR"
                        binding!!.totalAmount.text = "${it.toInt() + data.extraFee!!.toInt()} SAR"

                    } else {
                        binding!!.singleConsultation.text = "$it SAR"
                        binding!!.consultationAmount.text = "0 SAR"
                        binding!!.totalAmount.text = "$it SAR"
                    }

                }

            }
        }




        val lab: ArrayList<SlabData> = intent.getSerializableExtra("lab") as ArrayList<SlabData>
        val medicine: ArrayList<MedData> = intent.getSerializableExtra("med") as ArrayList<MedData>
        val test: ArrayList<TestData> = intent.getSerializableExtra("test") as ArrayList<TestData>




        if(medicine.isEmpty()==false){

            Log.d("vdvbdcbcdb", medicine[0].medicine.toString())
           // Log.d("sdxbnsxa", medicine.size.toString())
//            Log.d("a,sa,aq,,a", medicine[0].medicine!!.size.toString())
            Log.d("xsxs", medicine[0].medicine_desc.toString())
            var finaltext = ""


            if (medicine[0].medicine!=null){
                for (i in 0 until  medicine[0].medicine!!.size ){
                    Log.d("medicinee", medicine[0].medicine!![i])
                    for (j in 0 until  medicine[0].medicine_desc!!.size ){

                        var sizee = medicine[0].medicine_desc!!.size
                        /*         Log.d("kanikashanmugam", medicine[0].medicine_desc!![0])
                                 Log.d("kanikashanmugam", medicine[0].medicine_desc!![1])*/
                        Log.d("kanikashanmugam", medicine[0].medicine_desc!!.size.toString())
                        Log.d("vcvcvcvvcvccv", medicine[0].medicine!![i]+" - "+medicine[0].medicine_desc!![j])

                        if (i ==0){
                            finaltext = finaltext+(i+1)+" . "+medicine[0].medicine!![i]+" - "+medicine[0].medicine_desc!![0]

                        }else{
                            finaltext = finaltext+"\n"+(i+1)+" . "+medicine[0].medicine!![i]+" - "+medicine[0].medicine_desc!![sizee-1]

                        }

                        break
                    }
                }
                val str1 = medicine[0].medicine
                val str2 = medicine[0].medicine_desc
                /*  val separate1 = str1!!.split(",").map { it.trim() }
                   val separate2 = str2!!.split(",").map { it.trim() }*/
/*
            for ((index,value) in separate1.withIndex()){
                if(index == 0){
                    finaltext = finaltext+(index+1)+"."+separate1[index]+" - "+separate2[index]
                }
                else{
                    finaltext = finaltext+"\n"+(index+1)+"."+separate1[index]+" - "+separate2[index]
                }
            }
*/
                binding!!.medicinelist.text = finaltext
            }


        }

        if(lab.isEmpty()==false){
            var labnames: String? = ""
            for (i in 0 until lab.size){
                if(i == 0){
                    labnames = ""+(i+1)+"."+lab[i].lab_name
                }
                else{
                    labnames = labnames+"\n"+(i+1)+"."+lab[i].lab_name
                }
            }
            binding!!.lablist.text = labnames
        }

        if(test.isEmpty()==false){
            var testnames: String? = ""
            for (i in 0 until test.size){
                if(i == 0){
                    testnames = ""+(i+1)+"."+test[i].Test_name
                }
                else{
                    testnames = testnames+"\n"+(i+1)+"."+test[i].Test_name
                }
            }
            binding!!.testlist.text = testnames
        }

        binding!!.labreport.setOnClickListener {
            DialogUtils.showLoader(this@InvoiceDetailsActivity)
            viewModel?.viewlabreport(this@InvoiceDetailsActivity, bookingid)?.observe(this@InvoiceDetailsActivity, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        }
                        else {
                            it.data?.let { data ->
                                if(data.size!=0){
                                    startActivity(
                                        Intent(
                                            this@InvoiceDetailsActivity,
                                            ViewLabReportActivity::class.java
                                        )
                                            .putExtra("labreports",data as ArrayList<ReportData>)
                                    )
                                }
                                else{
                                    UiUtils.showSnack(binding!!.root, getString(R.string.labreportnotuploaded))
                                }
                            }
                        }
                    }
                }
            })
        }

    }


    fun onBackPressed(view: View) {
        onBackPressed()
    }

}