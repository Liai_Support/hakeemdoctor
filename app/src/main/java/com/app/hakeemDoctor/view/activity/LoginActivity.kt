package com.app.hakeemDoctor.view.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityLogin1Binding
import com.app.hakeemDoctor.models.CheckOtpData
import com.app.hakeemDoctor.models.GetOtpData
import com.app.hakeemDoctor.models.LoginData
import com.app.hakeemDoctor.utils.*
import com.app.hakeemDoctor.viewmodel.RegisterViewModel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient

class LoginActivity : BaseActivity(), GoogleApiClient.OnConnectionFailedListener {

    var binding: ActivityLogin1Binding? = null
    private var viewmodel: RegisterViewModel? = null
    var sharedHelper: SharedHelper? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLogin1Binding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_login1)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedHelper = SharedHelper(this)
    }

    fun ShowHidePass(view: View) {
        if(binding!!.password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
            binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_off_24);

            //Show Password
            binding!!.password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_24);

            //Hide Password
            binding!!.password.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }

    private fun showDialog1(mail: String) {
        /*val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.popup_forget)

        *//*val body = dialog.findViewById(R.id.body) as TextView
        body.text = title
        val yesBtn = dialog.findViewById(R.id.yesBtn) as Button
        val noBtn = dialog.findViewById(R.id.noBtn) as TextView
        yesBtn.setOnClickListener {
            dialog.dismiss()
        }
        noBtn.setOnClickListener { dialog.dismiss() }*//*
        dialog.show()*/

        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_forget, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        //show dialog
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        mDialogView.findViewById<EditText>(R.id.mail).setText(mail)
        //login button click of custom layout
        mDialogView.findViewById<Button>(R.id.send).setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
            //get text from EditTexts of custom layout
            val mail = mDialogView.findViewById<EditText>(R.id.mail).text.toString()
            getOtp(mail)
        }
        /*//cancel button click of custom layout
        mDialogView.dialogCancelBtn.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
        }
*/
    }

    private fun getOtp(email: String) {
        DialogUtils.showLoader(this)
        viewmodel?.getOtp(email)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data)
                        }
                    }
                }
            }
        })
    }

    private fun handleResponse(data: GetOtpData) {
        data.otp?.let {
           // otp.setText(it)
            showDialog2(it)
        }
    }

    private fun showDialog2(s: String) {
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_forget_otp, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        //show dialog
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        //login button click of custom layout
        //mDialogView.otp.setText(s)
        mDialogView.findViewById<TextView>(R.id.getStarted).setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
            if (mDialogView.findViewById<EditText>(R.id.otp).text.toString().trim() == "") {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourotp))
            } else {
                DialogUtils.showLoader(this)
                viewmodel?.verifyOtp(binding!!.emailId.text.toString(), mDialogView.findViewById<EditText>(R.id.otp).text.toString())?.observe(this,
                    Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root, msg)
                                    }
                                } else {
                                    it.data?.let { data ->
                                        handleCheckotpResponse(data)
                                    }
                                }
                            }

                        }

                    })
            }

            //get text from EditTexts of custom layout
        }
    }

    private fun handleCheckotpResponse(data: CheckOtpData) {
        data.token?.let { sharedHelper?.token = it }
        showDialog3()
        //startActivity(Intent(this, ResetPasswordActivity::class.java))
    }

    private fun showDialog3(){
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_forget_new, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        //show dialog
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        //login button click of custom layout
       mDialogView.findViewById<Button>(R.id.onResetPasswordClicked).setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
           if (!mAlertDialog.findViewById<EditText>(R.id.newPassword).text.toString().equals("") && !mAlertDialog.findViewById<EditText>(R.id.confirmPassword).text.toString().equals("")) {
               if(mAlertDialog.findViewById<EditText>(R.id.newPassword).text.toString().trim().equals(mAlertDialog.findViewById<EditText>(R.id.confirmPassword).text.toString().trim())){
                   DialogUtils.showLoader(this)
                   viewmodel?.resetPassword(mAlertDialog.findViewById<EditText>(R.id.newPassword).text.toString().trim())?.observe(this, Observer {
                       DialogUtils.dismissLoader()
                       it?.let {
                           it.error?.let { error ->
                               if (error) {
                                   it.message?.let { msg ->
                                       UiUtils.showSnack(binding!!.root, msg)
                                   }
                               } else {
                                   val intent =
                                           Intent(this, LoginActivity::class.java)
                                   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                   intent.flags =
                                           Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                   startActivity(intent)
                               }
                           }

                       }
                   })
               }
               else{
                   UiUtils.showSnack(mDialogView,getString(R.string.passwordmismatch))
               }
           }
           else{
               UiUtils.showSnack(mDialogView,getString(R.string.pleaseenterpassword))
           }

           //get text from EditTexts of custom layout
        }

        mDialogView.findViewById<ImageView>(R.id.show_pass_btn1).setOnClickListener {
            if(mDialogView.findViewById<EditText>(R.id.newPassword).getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_off_24);

                //Show Password
                mDialogView.findViewById<EditText>(R.id.newPassword).setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_24);

                //Hide Password
                mDialogView.findViewById<EditText>(R.id.newPassword).setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }
        mDialogView.findViewById<ImageView>(R.id.show_pass_btn2).setOnClickListener {
            if(mDialogView.findViewById<EditText>(R.id.confirmPassword).getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_off_24);

                //Show Password
                mDialogView.findViewById<EditText>(R.id.confirmPassword).setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_24);

                //Hide Password
                mDialogView.findViewById<EditText>(R.id.confirmPassword).setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }
    }



    fun onLoginClicked(view: View) {


        if (isValidInputs()) {
            DialogUtils.showLoader(this)
            viewmodel?.signin(binding!!.emailId.text.toString().trim(), binding!!.password.text.toString().trim())
                ?.observe(this,
                    Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root, msg)
                                    }
                                } else {
                                    it.data?.let { data ->
                                        handleResponse(data)
                                    }
                                }
                            }

                        }
                    })
        }


    }

    fun onForgotPasswordClicked(view: View) {
        /*// Initialize a new layout inflater instance
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // Inflate a custom view using layout inflater
        val view = inflater.inflate(R.layout.popup_forget,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
            view, // Custom view to show in popup window
            LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT // Window height
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }


        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }
*/
        /*// Get the widgets reference from custom view
        val tv = view.findViewById<TextView>(R.id.text_view)
        val buttonPopup = view.findViewById<Button>(R.id.button_popup)

        // Set click listener for popup window's text view
        tv.setOnClickListener{
            // Change the text color of popup window's text view
            tv.setTextColor(Color.RED)
        }

        // Set a click listener for popup's button widget
        buttonPopup.setOnClickListener{
            // Dismiss the popup window
            popupWindow.dismiss()
        }
*/
        /*// Set a dismiss listener for popup window
        popupWindow.setOnDismissListener {
            Toast.makeText(applicationContext,"Popup closed",Toast.LENGTH_SHORT).show()
        }


        // Finally, show the popup window on app
        TransitionManager.beginDelayedTransition(root)
        popupWindow.showAtLocation(
            root, // Location to display popup window
            Gravity.CENTER, // Exact position of layout to display popup
            0, // X offset
            0 // Y offset
        )
*/

        if (binding!!.emailId.text.toString().trim() == "") {
            UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryouremailid))
        } else if (!BaseUtils.isValidEmail(binding!!.emailId.text.toString().trim())) {
            UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourvalidemailid))
        } else {
            showDialog1(binding!!.emailId.text.toString().trim())

           /* startActivity(
                Intent(this, ForgotPasswordActivity::class.java)
                    .putExtra(Constants.IntentKeys.EMAIL, emailId.text.toString().trim())
            )*/
        }

    }

    private fun handleResponse(data: LoginData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.mobileNumber?.let { sharedHelper?.mobileNumber = it }
        data.token?.let { sharedHelper?.token = it }
        data.providerType?.let { sharedHelper?.providerType = it }
        data.countryCode?.let { value -> sharedHelper?.countryCode = value }
        data.profilePic?.let { value -> sharedHelper?.userImage = value }

        sharedHelper?.loggedIn = true
        sharedHelper?.status = "online"

        val intent =
            Intent(this, DashBoardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun isValidInputs(): Boolean {
        when {

            binding!!.emailId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryouremailid))
                return false
            }

            !BaseUtils.isValidEmail(binding!!.emailId.text.toString()) -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourvalidemailid))
                return false
            }

            binding!!.password.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourpassword))
                return false
            }

            else -> return true
        }


    }


    override fun onConnectionFailed(p0: ConnectionResult) {

    }


}