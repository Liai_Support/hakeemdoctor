package com.app.hakeemDoctor.view.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivitySocialLoginDetailsBinding
import com.app.hakeemDoctor.models.SocialRegisterData
import com.app.hakeemDoctor.utils.*
import com.app.hakeemDoctor.viewmodel.AmazonViewModel
import com.app.hakeemDoctor.viewmodel.RegisterViewModel
import java.io.File

class SocialLoginDetailsActivity : BaseActivity() {

    var binding: ActivitySocialLoginDetailsBinding? = null

    var socialToken = ""
    var loginType = ""
    var profileImage = ""
    var uploadFile: File? = null
    var sharedHelper: SharedHelper? = null
    private var amazonViewModel: AmazonViewModel? = null
    private var viewmodel: RegisterViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySocialLoginDetailsBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_social_login_details)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        amazonViewModel = ViewModelProvider(this).get(AmazonViewModel::class.java)
        sharedHelper = SharedHelper(this)
        getIntentValues()

    }

    private fun getIntentValues() {

        intent.extras?.let { extra ->

            extra.getString(Constants.IntentKeys.EMAIL)?.let { value ->
                if (value.isNotEmpty()) {
                    binding!!.emailId.setText(value)
                    binding!!.emailId.isEnabled = false
                } else {
                    binding!!.emailId.isEnabled = true
                }

            }

            extra.getString(Constants.IntentKeys.NAME)?.let { value ->
                binding!!.name.setText(value)
            }

            extra.getString(Constants.IntentKeys.SOCIALTOKEN)?.let { value ->
                socialToken = value
            }

            extra.getString(Constants.IntentKeys.LOGINTYPE)?.let { value ->
                loginType = value
            }

            extra.getString(Constants.IntentKeys.PROFILE_PICTURE)?.let { value ->
                if (value.isNotEmpty()) {
                    profileImage = value
                    UiUtils.loadImage(binding!!.profilePicture, value)
                }
            }


        }

    }

    fun onSubmitClicked(view: View) {

        if (isValidInputs()) {
            DialogUtils.showLoader(this)
            if (uploadFile != null) {

                amazonViewModel?.uploadImage(this, uploadFile!!)?.observe(this, Observer {


                    it.error?.let { value ->
                        if (!value) {
                            proceedSignIn(it.message)
                        } else {
                            DialogUtils.dismissLoader()
                        }
                    }
                })
            } else {
                proceedSignIn("")
            }
        }

    }

    private fun proceedSignIn(imageUrlString: String?) {

        imageUrlString?.let { image ->

            var imageurl = ""

            imageurl = if (image.isEmpty()) {
                profileImage
            } else {
                imageUrlString
            }

            viewmodel?.socialRegister(
                binding!!.emailId.text.toString().trim(),
                imageurl,
                binding!!.name.text.toString().trim(),
                socialToken,
                binding!!.countryCodePicker.selectedCountryCode,
                binding!!.phoneNumber.text.toString().trim()
            )?.observe(this, Observer {
                it?.let {
                    DialogUtils.dismissLoader()
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)
                            }
                        }
                    }
                }
            })

        }

    }

    private fun handleResponse(data: SocialRegisterData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.token?.let { sharedHelper?.token = it }

        sharedHelper?.loggedIn = true
        val intent =
            Intent(this, DashBoardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)

    }

    fun OnProfileEditClicked(view: View) {
        DialogUtils.showPictureDialog(this)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constants.RequestCode.GALLERY_INTENT//gallery
            -> if (resultCode == RESULT_OK) {
                handleGallery(data)
            }
            Constants.RequestCode.CAMERA_INTENT//camera
            -> if (resultCode == RESULT_OK) {
                handleCamera()
            }

        }
    }

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                UiUtils.loadImage(
                    binding!!.profilePicture,
                    BaseUtils.getRealPathFromUriNew(this, uri),
                    ContextCompat.getDrawable(this, R.drawable.doctor)!!
                )
                uploadFile = File(BaseUtils.getRealPathFromURI(this, uri)!!)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_select))
            }
        }
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper!!.imageUploadPath)
        if (uploadFile!!.exists()) {
            UiUtils.loadImage(
                binding!!.profilePicture,
                sharedHelper!!.imageUploadPath,
                ContextCompat.getDrawable(this, R.drawable.doctor)!!
            )
        } else {
            UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_retrieve))
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {

            Constants.Permission.READ_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                BaseUtils.openGallery(this)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.storage_permission_error))
            }

            Constants.Permission.CAMERA_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
                && grantResults[2] == PackageManager.PERMISSION_GRANTED
            ) {
                BaseUtils.openCamera(this)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.camera_permission_error))
            }
        }
    }


    private fun isValidInputs(): Boolean {
        when {

            uploadFile == null && profileImage.isEmpty() -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseuploadprofilepicture))
            }

            binding!!.name.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourname))
                return false
            }
            binding!!.emailId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryouremailid))
                return false
            }

            !BaseUtils.isValidEmail(binding!!.emailId.text.toString()) -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourvalidemailid))
                return false
            }
            binding!!.phoneNumber.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourmobilenumber))
                return false
            }

            else -> return true
        }

        return true
    }
}