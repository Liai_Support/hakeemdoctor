package com.app.hakeemDoctor.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ChildAppoinmentListBinding
import com.app.hakeemDoctor.databinding.ChildDoctorVirtualConsultationBinding
import com.app.hakeemDoctor.interfaces.OnClickListener
import com.app.hakeemDoctor.interfaces.SingleTapListener
import com.app.hakeemDoctor.models.BookingData
import com.app.hakeemDoctor.utils.BaseUtils
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.SharedHelper
import java.util.*

class AppoinmentListAdapter(
    var context: Context,
    var lat: Double?,
    var lng: Double?,
    var onClickListener: OnClickListener,
    var data: ArrayList<BookingData>,
    var cancel: OnClickListener,
    var chatListner: OnClickListener
) :
    RecyclerView.Adapter<AppoinmentListAdapter.ViewHolder>() {

    var sharedHelper: SharedHelper? = null

    init {
        sharedHelper = SharedHelper(context)
    }

    inner class ViewHolder(view: View, type: Int) : RecyclerView.ViewHolder(view) {
        var binding: ChildAppoinmentListBinding? = null
        var virtualBinding: ChildDoctorVirtualConsultationBinding? = null

        init {
            if (type == 0) {
                binding = ChildAppoinmentListBinding.bind(view)
            } else {
                virtualBinding =
                    ChildDoctorVirtualConsultationBinding.bind(view)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        if (viewType == 0) {
            return ViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.child_appoinment_list,
                    parent,
                    false
                )
                , 0
            )
        } else {
            return ViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.child_doctor_virtual_consultation,
                    parent,
                    false
                )
                , 1
            )
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(data[position].is_ins == 1){
            if(data[position].ins_status!= null && data[position].ins_status!!.equals("paid")|| (data[position].ins_status!!.equals("accepted"))) {
                if (data[position].isVirtualBooking == null || data[position].isVirtualBooking == 0) {

          /*          holder.binding?.bookingid!!.text =
                        "" + context.getString(R.string.bookingid) + "" + data[position].bookingIds.toString()*/


                    holder.binding?.bookingid1!!.text =
                      data[position].bookingIds.toString()

                    holder.binding?.main?.setOnClickListener {
                        onClickListener.onClickItem(position)
                        onClickListener.onClickItem(position)
                    }
                    Log.d("jansjAKZN",""+data[position].fee)
                    if ( data[position].booking_type=="home_visit"){
                        holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                        holder.binding!!.textViewststuss.text =" Home visit "

                    }else if ( data[position].booking_type=="virtual"){

                        holder.virtualBinding!!.textViewststuss.text =" Virtual Consult"
                        holder.virtualBinding?.fee?.text = " : "+data[position].fee + " SAR"

                    }else if ( data[position].booking_type=="emergency"){
                        holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                        holder.binding!!.textViewststuss.text =" Emergency"

                    }else if ( data[position].booking_type.equals("global")){
                        Log.d("kanikankanaknis","sjxbsjxhn1")
                        holder.virtualBinding?.fee?.text = " : "+data[position].fee + " SAR"

                        holder.virtualBinding!!.textViewststuss.text =" Global Doctor "

                    }
                    if(data[position].previousIssue.equals("not mentioned",true)){
                        holder.binding?.healthIssue?.text = "Not mentioned"
                    }else  if (data[position].previousIssue!!.isBlank()||data[position].previousIssue!!.isEmpty()){
                        holder.binding!!.healthIssue.text = "Not mentioned"

                    }else if(data[position].previousIssue==""||data[position].previousIssue!!.length==0) {
                        holder.binding!!.healthIssue.text = "Not mentioned"

                    }else{
                        holder.binding?.healthIssue?.text = data[position].previousIssue

                    }
                    data[position].isFeatureBooking?.let {
                        if (it) {
                            data[position].patientName?.let { holder.binding?.patientName?.text = it }
                            data[position].patientAge?.let {
                                holder.binding?.yearsOld?.text = "$it years old"
                            }
                            if(data[position].previousIssue==""||data[position].previousIssue!!.length==0) {
                                holder.binding!!.healthIssue.text = "Not mentioned"

                            }else{
                                holder.binding?.healthIssue?.text = data[position].previousIssue

                            }
                       // data[position].previousIssue?.let { holder.binding?.healthIssue?.text = it }
                            holder.binding?.healthIssueText?.visibility = View.VISIBLE
                        } else {
                            holder.binding?.healthIssueText?.visibility = View.VISIBLE
                            holder.binding?.healthIssue?.text = context.getString(R.string.notmentioned)
                            data[position].name?.let { holder.binding?.patientName?.text = it }
                            data[position].DOB?.let {
                                holder.binding?.yearsOld?.text =
                                    "${BaseUtils.getAgeCalculation(it)} years old"
                            }
                        }
                    }

//        data[position].previousIssue?.let { holder.binding.healthIssue.text = it }
                    data[position].mobileNumber?.let {
                        holder.binding?.phoneNumber?.text = "${data[position].countryCode} $it"
                    }
                    data[position].email?.let { holder.binding?.mailId?.text = it }
                    data[position].location?.let { holder.binding?.location?.text = it }


//        data[position].distance?.let {
//            holder.binding.kmAway.text = BaseUtils.numberFormat(it.toDouble())
//        }

                    data[position].bookingLatitude?.let { bkLat ->
                        data[position].bookingLongitude?.let { bkLng ->
                            lat?.let { lat ->
                                lng?.let { lng ->
                                    holder.binding?.kmAway?.text =
                                        "${
                                            BaseUtils.calculateDistance(
                                                bkLat.toDouble(),
                                                bkLng.toDouble(),
                                                lat,
                                                lng
                                            )
                                        } km away"
                                }
                            }
                        }
                    }
                    holder.binding?.date?.text = data[position].bookingDate+" " +data[position].bookingPeriod
                 /*   data[position].bookingDate?.let {
                        holder.binding?.date?.text =
                            BaseUtils.getFormatedDateUtc(
                                BaseUtils.getFormatedDate(
                                    it,
                                    "yyyy-MM-dd hh:mm",
                                    "dd,MMMM,yyyy"
                                ) + " " + data[position].bookingTime + " " + data[position].bookingPeriod,
                                "dd,MMMM,yyyy hh:mm:ss a", "dd,MMMM,yyyy"
                            )

                    }*/


                    data[position].status?.let {
                        when {
                            it.equals("pending", true) -> {
                                holder.virtualBinding?.statusButton?.text =
                                    context.resources.getString(R.string.cancel_appoinment)
                            }
                            it.equals("startvisit", true) -> {
                                holder.binding?.statusButton?.text =
                                    context.getString(R.string.start_visit)

                            }
                            it.equals("completed", true) -> {
                                holder.binding?.statusButton?.text =
                                    context.getString(R.string.visit_completed)

                            }
                            it.equals("cancelledbyprovider", true) -> {
                                holder.binding?.statusButton?.text =
                                    context.getString(R.string.cancelled_by_provider)

                            }
                            it.equals("cancelledbypatient", true) -> {
                                holder.binding?.statusButton?.text =
                                    context.getString(R.string.cancelled_by_paitent)

                            }
                            it.equals("paymentverified", true) -> {
                                holder.binding?.statusButton?.text =
                                    context.getString(R.string.payment_verified)

                            }
                        }
                    }


                    data[position].status?.let {
                        if (it.equals("pending", true)) {
                            holder.binding?.cancelAppoinment?.isEnabled = true
                            context.resources.getString(R.string.cancel)
                            holder.binding?.cancelAppoinment?.visibility = View.VISIBLE
                            holder.binding?.StartAppoinment?.visibility = View.GONE

                        } else {
                            holder.binding?.cancelAppoinment?.isEnabled = false
                            holder.binding?.cancelAppoinment?.visibility = View.GONE
                        }
                    }

                    holder.binding?.cancelAppoinment?.setOnClickListener {
                        if (data[position].status.equals("pending", true))
                            cancel.onClickItem(position)
                    }
                }
                else {


                       holder.virtualBinding?.bookingid1!!.text =
          data[position].bookingIds.toString()
                 /*   holder.virtualBinding?.bookingid!!.text =
                        "" + context.getString(R.string.bookingid) + "" + data[position].bookingIds.toString()
*/
                    data[position].status?.let {
                        when {
                            it.equals("pending", true) -> {

                                if (BaseUtils.isConsultationTimeOver(
                                        BaseUtils.getFormatedDateUtc(
                                            data[position].bookingDate,
                                            "yyyy-MM-dd HH:mm",
                                            "dd/MM/yyyy hh:mm a"
                                        )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                                    )
                                ) {
                                    holder.virtualBinding?.statusButton?.text =
                                        context.resources.getString(R.string.time_expired)
                                } else {
                                    holder.virtualBinding?.statusButton?.text =
                                        context.resources.getString(R.string.cancel_appoinment)
                                }

                            }
                            it.equals("startvisit", true) -> {
                                holder.virtualBinding?.statusButton?.text =
                                    context.getString(R.string.start_visit)

                            }
                            it.equals("virtualbookingcompleted", true) -> {
                                holder.virtualBinding?.statusButton?.text =
                                    context.getString(R.string.booking_completed)

                            }
                            it.equals("cancelledbyprovider", true) -> {
                                holder.virtualBinding?.statusButton?.text =
                                    context.getString(R.string.cancelled_by_provider)

                            }
                            it.equals("cancelledbypatient", true) -> {
                                holder.virtualBinding?.statusButton?.text =
                                    context.getString(R.string.cancelled_by_paitent)

                            }
                            it.equals("paymentverified", true) -> {
                                holder.virtualBinding?.statusButton?.text =
                                    context.getString(R.string.payment_verified)

                            }
                        }
                    }

                    if ( data[position].booking_type=="home_visit"){
                        holder.binding!!.textViewststuss.text =" Home visit"
                        holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                    }else   if ( data[position].booking_type=="virtual"){
                        holder.virtualBinding?.fee?.text = " : "+data[position].fee + " SAR"

                        holder.virtualBinding!!.textViewststuss.text =" Virtual Consult"

                    }
                    holder.virtualBinding?.cancel?.setOnClickListener {
                        if (BaseUtils.isValidCancelTime(
                                BaseUtils.getFormatedDateUtc(
                                    data[position].bookingDate!!,
                                    "yyyy-MM-dd HH:mm",
                                    "dd/MM/yyyy hh:mm a"
                                )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                            )
                            && data[position].status!! == "pending"
                        )
                            cancel.onClickItem(position)
                        else if (data[position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(
                                BaseUtils.getFormatedDateUtc(
                                    data[position].bookingDate,
                                    "yyyy-MM-dd HH:mm",
                                    "dd/MM/yyyy hh:mm a"
                                )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                            )
                        )

                            DialogUtils.showAlert(
                                context,
                                object : SingleTapListener {
                                    override fun singleTap() {

                                    }

                                },
                                context.getString(R.string.cancellation_alert1)
                                        + " " + sharedHelper?.chatTiming
                                        + " " + context.getString(
                                    R.string.cancellation_alert2
                                )
                            )
                    }

                    holder.virtualBinding?.chat?.setOnClickListener {
                        if (BaseUtils.isConsultationTime(
                                BaseUtils.getFormatedDateUtc(
                                    data[position].bookingDate!!,
                                    "yyyy-MM-dd HH:mm",
                                    "dd/MM/yyyy hh:mm a"
                                )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                            )
                            && data[position].status!! == "pending"
                        )
                            chatListner.onClickItem(position)
                        else if (data[position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(
                                BaseUtils.getFormatedDateUtc(
                                    data[position].bookingDate,
                                    "yyyy-MM-dd HH:mm",
                                    "dd/MM/yyyy hh:mm a"
                                )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                            )
                        )
                            DialogUtils.showAlert(context, object : SingleTapListener {
                                override fun singleTap() {

                                }

                            }, context.getString(R.string.wait_alert))
                    }

                    data[position].status?.let {
                        holder.virtualBinding?.cancel?.isEnabled = it.equals("pending", true)
                    }


                    data[position].patientName?.let { holder.virtualBinding?.name?.text = it }
                    data[position].patientAge?.let {
                        holder.virtualBinding?.yearsOld?.text = "$it years old"
                    }

                    data[position].email?.let { holder.virtualBinding?.mailId?.text = it }


                    data[position].bookingDate?.let {
                        data[position].bookingTime?.let { time ->
                            holder.virtualBinding?.startTime?.text =
                                "${
                                    BaseUtils.getFormatedDateUtc(
                                        it,
                                        "yyyy-MM-dd HH:mm",
                                        "dd/MM/yyyy hh:mm a"
                                    )
                                }"

                        }

                    }

                    data[position].bookingDate?.let {
                        data[position].bookingTime?.let { time ->
                            holder.virtualBinding?.endTime?.text = BaseUtils.addEndTime(
                                "${
                                    BaseUtils.getFormatedDateUtc(
                                        it,
                                        "yyyy-MM-dd HH:mm",
                                        "dd/MM/yyyy hh:mm a"
                                    )
                                }",
                                "dd/MM/yyyy hh:mm a",
                                "dd/MM/yyyy hh:mm a",
                                sharedHelper?.chatTiming!!
                            )
                        }
                    }



                    data[position].patientAge?.let {
                        holder.virtualBinding?.yearsOld?.text = "$it years old"
                    }

                }
            }
            else{

                if (data[position].isVirtualBooking == null || data[position].isVirtualBooking == 0){

                    holder.binding!!.main.visibility = View.GONE
                }
                else {
                    holder.virtualBinding!!.main.visibility = View.GONE
                }
            }
        }
        else {
            if (data[position].isVirtualBooking == null || data[position].isVirtualBooking == 0) {

              /*  holder.binding?.bookingid!!.text =
                    "" + context.getString(R.string.bookingid) + "" + data[position].bookingIds.toString()*/
                holder.binding?.bookingid1!!.text =
                   data[position].bookingIds.toString()
                holder.binding?.main?.setOnClickListener {
                    onClickListener.onClickItem(position)
                    onClickListener.onClickItem(position)
                }

                if ( data[position].booking_type=="home_visit"){
                    holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.binding!!.textViewststuss.text =" Home visit "

                }else if ( data[position].booking_type=="virtual"){
                    holder.virtualBinding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.virtualBinding!!.textViewststuss.text =" Virtual Consult"

                }else if ( data[position].booking_type=="emergency"){
                    holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.binding!!.textViewststuss.text =" Emergency"

                }else if ( data[position].booking_type.equals("global")){
                    Log.d("kanikankanaknis","sjxbsjxhn1")
                    holder.virtualBinding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.virtualBinding!!.textViewststuss.text =" Global Doctor "

                }
                data[position].isFeatureBooking?.let {
                    if (it) {
                        data[position].patientName?.let { holder.binding?.patientName?.text = it }
                        data[position].patientAge?.let {
                            holder.binding?.yearsOld?.text = "$it years old"
                        }
                        if(data[position].previousIssue==""||data[position].previousIssue!!.length==0) {
                            holder.binding!!.healthIssue.text = "Not mentioned"

                        }else{
                            holder.binding?.healthIssue?.text = data[position].previousIssue

                        }
                  // data[position].previousIssue?.let { holder.binding?.healthIssue?.text = it }
                        holder.binding?.healthIssueText?.visibility = View.VISIBLE
                    } else {
                        holder.binding?.healthIssueText?.visibility = View.VISIBLE
                    holder.binding?.healthIssue?.text = context.getString(R.string.notmentioned)
                        data[position].name?.let { holder.binding?.patientName?.text = it }
                        data[position].DOB?.let {
                            holder.binding?.yearsOld?.text =
                                "${BaseUtils.getAgeCalculation(it)} years old"
                        }
                    }
                }
                if(data[position].previousIssue==""||data[position].previousIssue!!.length==0) {
                    holder.binding!!.healthIssue.text = "Not mentioned"

                }else{
                    holder.binding?.healthIssue?.text = data[position].previousIssue

                }
   //    data[position].previousIssue?.let { holder.binding!!.healthIssue.text = it }
                data[position].mobileNumber?.let {
                    holder.binding?.phoneNumber?.text = "${data[position].countryCode} $it"
                }
                data[position].email?.let { holder.binding?.mailId?.text = it }
                data[position].location?.let { holder.binding?.location?.text = it }


//        data[position].distance?.let {
//            holder.binding.kmAway.text = BaseUtils.numberFormat(it.toDouble())
//        }

                data[position].bookingLatitude?.let { bkLat ->
                    data[position].bookingLongitude?.let { bkLng ->
                        lat?.let { lat ->
                            lng?.let { lng ->
                                holder.binding?.kmAway?.text =
                                    "${
                                        BaseUtils.calculateDistance(
                                            bkLat.toDouble(),
                                            bkLng.toDouble(),
                                            lat,
                                            lng
                                        )
                                    } km away"
                            }
                        }
                    }
                }
                holder.binding?.date?.text = data[position].bookingDate+" " +data[position].bookingPeriod

               /* data[position].bookingDate?.let {
                    holder.binding?.date?.text =
                        BaseUtils.getFormatedDateUtc(
                            BaseUtils.getFormatedDate(
                                it,
                                "yyyy-MM-dd hh:mm",
                                "dd,MMMM,yyyy"
                            ) + " " + data[position].bookingTime + " " + data[position].bookingPeriod,
                            "dd,MMMM,yyyy hh:mm:ss a", "dd,MMMM,yyyy"
                        )

                }*/


                data[position].status?.let {
                    when {
                        it.equals("pending", true) -> {
                            holder.virtualBinding?.statusButton?.text =
                                context.resources.getString(R.string.cancel_appoinment)
                        }
                        it.equals("startvisit", true) -> {
                            holder.binding?.statusButton?.text =
                                context.getString(R.string.start_visit)

                        }
                        it.equals("completed", true) -> {
                            holder.binding?.statusButton?.text =
                                context.getString(R.string.visit_completed)

                        }
                        it.equals("cancelledbyprovider", true) -> {
                            holder.binding?.statusButton?.text =
                                context.getString(R.string.cancelled_by_provider)

                        }
                        it.equals("cancelledbypatient", true) -> {
                            holder.binding?.statusButton?.text =
                                context.getString(R.string.cancelled_by_paitent)

                        }
                        it.equals("paymentverified", true) -> {
                            holder.binding?.statusButton?.text =
                                context.getString(R.string.payment_verified)

                        }
                    }
                }


                data[position].status?.let {
                    if (it.equals("pending", true)) {
                        holder.binding?.cancelAppoinment?.isEnabled = true
                        context.resources.getString(R.string.cancel)
                        holder.binding?.cancelAppoinment?.visibility = View.VISIBLE
                        holder.binding?.StartAppoinment?.visibility = View.GONE
                    } else {
                        holder.binding?.cancelAppoinment?.isEnabled = false
                        holder.binding?.cancelAppoinment?.visibility = View.GONE
                    }
                }

                holder.binding?.cancelAppoinment?.setOnClickListener {
                    if (data[position].status.equals("pending", true))
                        cancel.onClickItem(position)
                }
            }
            else {
                if ( data[position].booking_type=="home_visit"){
                    holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.binding!!.textViewststuss.text =" Home visit "

                }else if ( data[position].booking_type=="virtual"){
                    holder.virtualBinding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.virtualBinding!!.textViewststuss.text =" Virtual Consult"

                }else if ( data[position].booking_type=="emergency"){
                    holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.binding!!.textViewststuss.text =" Emergency"

                }else if ( data[position].booking_type.equals("global")){
                    Log.d("kanikankanaknis","sjxbsjxhn1")
                    holder.virtualBinding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.virtualBinding!!.textViewststuss.text =" Global Doctor "

                }

            /*    holder.virtualBinding?.bookingid!!.text =
                    "" + context.getString(R.string.bookingid) + "" + data[position].bookingIds.toString()*/
                holder.virtualBinding?.bookingid1!!.text =
                    data[position].bookingIds.toString()
                data[position].status?.let {
                    when {
                        it.equals("pending", true) -> {

                            if (BaseUtils.isConsultationTimeOver(
                                    BaseUtils.getFormatedDateUtc(
                                        data[position].bookingDate,
                                        "yyyy-MM-dd HH:mm",
                                        "dd/MM/yyyy hh:mm a"
                                    )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                                )
                            ) {
                                holder.virtualBinding?.statusButton?.text =
                                    context.resources.getString(R.string.time_expired)
                            } else {
                                holder.virtualBinding?.statusButton?.text =
                                    context.resources.getString(R.string.cancel_appoinment)
                            }

                        }
                        it.equals("startvisit", true) -> {
                            holder.virtualBinding?.statusButton?.text =
                                context.getString(R.string.start_visit)

                        }
                        it.equals("virtualbookingcompleted", true) -> {
                            holder.virtualBinding?.statusButton?.text =
                                context.getString(R.string.booking_completed)

                        }
                        it.equals("cancelledbyprovider", true) -> {
                            holder.virtualBinding?.statusButton?.text =
                                context.getString(R.string.cancelled_by_provider)

                        }
                        it.equals("cancelledbypatient", true) -> {
                            holder.virtualBinding?.statusButton?.text =
                                context.getString(R.string.cancelled_by_paitent)

                        }
                        it.equals("paymentverified", true) -> {
                            holder.virtualBinding?.statusButton?.text =
                                context.getString(R.string.payment_verified)

                        }
                    }
                }



                holder.virtualBinding?.cancel?.setOnClickListener {
                    if (BaseUtils.isValidCancelTime(
                            BaseUtils.getFormatedDateUtc(
                                data[position].bookingDate!!,
                                "yyyy-MM-dd HH:mm",
                                "dd/MM/yyyy hh:mm a"
                            )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                        )
                        && data[position].status!! == "pending"
                    )
                        cancel.onClickItem(position)
                    else if (data[position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(
                            BaseUtils.getFormatedDateUtc(
                                data[position].bookingDate,
                                "yyyy-MM-dd HH:mm",
                                "dd/MM/yyyy hh:mm a"
                            )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                        )
                    )

                        DialogUtils.showAlert(
                            context,
                            object : SingleTapListener {
                                override fun singleTap() {

                                }

                            },
                            context.getString(R.string.cancellation_alert1)
                                    + " " + sharedHelper?.chatTiming
                                    + " " + context.getString(
                                R.string.cancellation_alert2
                            )
                        )
                }

                holder.virtualBinding?.chat?.setOnClickListener {
                    if (BaseUtils.isConsultationTime(
                            BaseUtils.getFormatedDateUtc(
                                data[position].bookingDate!!,
                                "yyyy-MM-dd HH:mm",
                                "dd/MM/yyyy hh:mm a"
                            )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                        )
                        && data[position].status!! == "pending"
                    )
                        chatListner.onClickItem(position)
                    else if (data[position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(
                            BaseUtils.getFormatedDateUtc(
                                data[position].bookingDate,
                                "yyyy-MM-dd HH:mm",
                                "dd/MM/yyyy hh:mm a"
                            )!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!
                        )
                    )
                        DialogUtils.showAlert(context, object : SingleTapListener {
                            override fun singleTap() {

                            }

                        }, context.getString(R.string.wait_alert))
                }

                data[position].status?.let {
                    holder.virtualBinding?.cancel?.isEnabled = it.equals("pending", true)
                }


                data[position].patientName?.let { holder.virtualBinding?.name?.text = it }
                data[position].patientAge?.let {
                    holder.virtualBinding?.yearsOld?.text = "$it years old"
                }

                data[position].email?.let { holder.virtualBinding?.mailId?.text = it }


                data[position].bookingDate?.let {
                    data[position].bookingTime?.let { time ->
                        holder.virtualBinding?.startTime?.text =
                            "${
                                BaseUtils.getFormatedDateUtc(
                                    it,
                                    "yyyy-MM-dd HH:mm",
                                    "dd/MM/yyyy hh:mm a"
                                )
                            }"

                    }

                }

                data[position].bookingDate?.let {
                    data[position].bookingTime?.let { time ->
                        holder.virtualBinding?.endTime?.text = BaseUtils.addEndTime(
                            "${
                                BaseUtils.getFormatedDateUtc(
                                    it,
                                    "yyyy-MM-dd HH:mm",
                                    "dd/MM/yyyy hh:mm a"
                                )
                            }",
                            "dd/MM/yyyy hh:mm a",
                            "dd/MM/yyyy hh:mm a",
                            sharedHelper?.chatTiming!!
                        )
                    }
                }



                data[position].patientAge?.let {
                    holder.virtualBinding?.yearsOld?.text = "$it years old"
                }

            }
        }
    }

    override fun getItemViewType(position: Int): Int {

        return if (data[position].isVirtualBooking == 1) {
            1
        } else {
            0
        }
    }

    fun setLatLng(myLat: Double, myLng: Double) {

        lat = myLat
        lng = myLng

        notifyDataSetChanged()

    }
}