package com.app.hakeemDoctor.view.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import android.view.View
import androidx.core.content.ContextCompat
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityChooseLanguageBinding
import com.app.hakeemDoctor.utils.SharedHelper
import java.util.*

class ChooseLanguageActivity : BaseActivity() {

    var binding: ActivityChooseLanguageBinding? = null
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChooseLanguageBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_choose_language)
        sharedHelper = SharedHelper(this)

        sharedHelper?.language?.let {
            setSelectedBack(it)

        }

        if (sharedHelper!!.language.equals("ar")){
            binding!!.imageView14.rotation=180F
        }
        binding!!.english.setOnClickListener {
            sharedHelper?.language?.let {
                if (it == "ar") {
                    sharedHelper?.language = "en"
                    setPhoneLanguage()
                }
            }
        }


        binding!!.arabic.setOnClickListener {
            sharedHelper?.language?.let {
                if (it == "en") {
                    sharedHelper?.language = "ar"
                    setPhoneLanguage()
                }
            }
        }

    }

    private fun setSelectedBack(language: String) {

        if (language == "en") {

            binding!!.english.setBackgroundResource(R.drawable.border_selected_language_background)
            binding!!.english.setTextColor(ContextCompat.getColor(this, R.color.white))

            binding!!.arabic.setBackgroundResource(R.drawable.border_unselected_language_background)
            binding!!.arabic.setTextColor(ContextCompat.getColor(this, R.color.textcolor_1))

        } else {
            binding!!.arabic.setBackgroundResource(R.drawable.border_selected_language_background)
            binding!!.arabic.setTextColor(ContextCompat.getColor(this, R.color.white))

            binding!!.english.setBackgroundResource(R.drawable.border_unselected_language_background)
            binding!!.english.setTextColor(ContextCompat.getColor(this, R.color.textcolor_1))
        }
    }


    private fun setPhoneLanguage() {
        val res = resources
        val conf = res.configuration
        val locale =
            Locale(sharedHelper?.language?.toLowerCase()!!)
        Locale.setDefault(locale)
        conf.setLocale(locale)
        applicationContext.createConfigurationContext(conf)
        val dm = res.displayMetrics
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(LocaleList(locale))
        } else {
            conf.locale = locale
        }
        res.updateConfiguration(conf, dm)
        reloadApp()
    }

    private fun reloadApp() {
        val intent =
            Intent(this, DashBoardActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    fun onBackPressed(view: View) {
        finish()
    }

}
