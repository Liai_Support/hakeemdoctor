package com.app.hakeemDoctor.view.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.FragmentHome1Binding
import com.app.hakeemDoctor.interfaces.DialogCallBack
import com.app.hakeemDoctor.interfaces.OnClickListener
import com.app.hakeemDoctor.interfaces.SingleTapListener
import com.app.hakeemDoctor.models.AppData
import com.app.hakeemDoctor.network.UrlHelper
import com.app.hakeemDoctor.utils.*
import com.app.hakeemDoctor.view.activity.*
import com.app.hakeemDoctor.viewmodel.HomeViewModel
import com.app.hakeemUser.rxbus.RxBusNotification
import com.google.android.gms.location.*
import io.reactivex.disposables.Disposable
import java.util.*
import kotlin.system.exitProcess

class HomeFragment(var onClickListner: OnClickListener) : Fragment(R.layout.fragment_home1) {

    var binding: FragmentHome1Binding? = null
    private var homeViewModel: HomeViewModel? = null
    private var sharedHelper: SharedHelper? = null
    private lateinit var notificationEventListner: Disposable
    private var isAttached = false
    private lateinit var fragmentTransaction: FragmentTransaction

    private var myLat: Double? = null
    private var myLng: Double? = null
    var doubleBackToExitPressedOnce = false

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null

    private var activeStatus = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHome1Binding.inflate(inflater, container, false)
        val view = binding!!.root
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        initListener()


        //listernNotification()
        //locationListner()
        // askLocationPermission()

        if (sharedHelper?.providerType.equals("ambulance", true)) {
            //textView.text = getString(R.string.hello_ambulance)
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())

        if(sharedHelper?.status == "online"){
            binding!!.customSwitch1.isChecked = true
            statuson()
        }

        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // isEnabled = false
                // activity?.onBackPressed()
                if ((activity as DashBoardActivity?)!!.binding!!.drawer.isDrawerOpen(GravityCompat.START)) {
                    (activity as DashBoardActivity?)!!.binding!!.drawer.closeDrawer(GravityCompat.START)
                }
                else {
                    //finish()
                    //moveTaskToBack(true);
                    //exitProcess(-1)
                    if (doubleBackToExitPressedOnce) {
                        requireActivity().moveTaskToBack(true);
                        exitProcess(-1)
                        /* val intent = Intent(Intent.ACTION_MAIN)
                        intent.addCategory(Intent.CATEGORY_HOME)
                        startActivity(intent)*/
                        return
                    }

                    doubleBackToExitPressedOnce = true
                    Toast.makeText(requireContext(), getString(R.string.pleaseclickbackagaintoexit), Toast.LENGTH_SHORT).show()

                    Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
                }
            }
        })

        /*
        customSwitch1.setOnCheckedChangeListener { buttonView, isChecked ->
           *//* val msg: String = if (isChecked)
                "Switch Button is Checked"
            else
                "Switch Button is UnChecked"
            showToast(msg)*//*
            if(isChecked) {
                sharedHelper?.status = true
                statuson()
            }
            else{
                sharedHelper?.status = false
            }
        }*/

        return view
    }

    private fun showToast(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }

    private fun statuson(){
        (activity as DashBoardActivity?)?.updateDeviceToken()
        locationListner()
        askLocationPermission()
        listernNotification()
        getAppSettings()


      /*  var scheduleTaskExecutor = Executors.newScheduledThreadPool(5)
        // This schedule a task to run every 10 seconds:
        scheduleTaskExecutor.scheduleAtFixedRate(Runnable {
            try {
                Log.d("hbhbc", "jbjb");
                //add your code you would like to be executed every 10 seconds.
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }, 0, 10, TimeUnit.SECONDS)*/
    }

    override fun onResume() {
        super.onResume()
        if(sharedHelper?.status == "online"){
            getAppSettings()
        }
        isAttached = true
    }

    override fun onPause() {
        super.onPause()
        isAttached = false
    }

    private fun listernNotification() {

        notificationEventListner =
            RxBusNotification.listen(String::class.java).doOnError {
            }.subscribe {
                ThreadUtils.runOnUiThread {
                    if (isAttached){
                        getAppSettings()
                    }
                }
            }
    }


    fun getAppSettings() {

        homeViewModel?.getAppSettings(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)
                            }
                        }
                    }

                }
            })

    }

    private fun handleResponse(data: AppData) {
        getLastKnownLocation()
        data.isDeleted?.let {
            if (it == "active") {
                moveToLoginActivity(requireContext())
            }
        }

        data.configurations?.let {
            if (it.size >= 3) {
                sharedHelper?.chatTiming = it[1].objvalue.toString().toInt()
                sharedHelper?.sharePercentage = it[2].objvalue.toString().toInt()
            }
        }

        if (data.pending != null && data.pending!!.size != 0) {

            data.pending?.let {
                if (it.size != 0) {
                    DialogUtils.showNewBookingAlert(
                        requireContext(),
                        object : DialogCallBack {
                            override fun onPositiveClick() {

                                Log.d("bxdhsabdx","snxksa"+it[0])
                                Log.d("bxdhsabdx","snxksa"+it[0].fee)
                                Log.d("bxdhsabdx","snxksa"+it[0].providerId)
                                Log.d("bxdhsabdx","snxksa"+sharedHelper!!.id)
                                callBookingResponseApi(it[0].bookingId,it[0].fee!!, UrlHelper.ACCEPTREQUEST)
                            }

                            override fun onNegativeClick() {
                                callBookingResponseApi(it[0].bookingId,it[0].fee, UrlHelper.REJECTREQUEST)
                            }

                        }, it[0], sharedHelper?.latitude.toString(), sharedHelper?.longitude.toString()
                    )
                } else {


                }
            }

        } else {
            data.payed?.let {
                if (it.size != 0) {
                    DialogUtils.showAlertDialogPayment(requireContext(),
                        requireContext().resources.getString(R.string.payment_confirm),
                        requireContext().resources.getString(R.string.invoice),
                        requireContext().resources.getString(R.string.ok),
                        requireContext().resources.getString(R.string.cancel),
                        object : DialogCallBack {
                            override fun onPositiveClick() {

                                startActivity(
                                    Intent(requireContext(), PaymentSummary::class.java)
                                        .putExtra(
                                            Constants.IntentKeys.PAYMENTDETAIL,
                                            it[0] as Parcelable
                                        )
                                )

                            }

                            override fun onNegativeClick() {

                            }
                        })
                }
            }
        }


    }

    private fun callBookingResponseApi(bookingId: Int?, fees: String?,methodName: String) {

        DialogUtils.showLoader(requireContext())
        homeViewModel?.callBookigResponse(requireContext(), bookingId, fees!!, methodName)
            ?.observe(viewLifecycleOwner,
                androidx.lifecycle.Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                   // UiUtils.showSnack(binding!!.root, msg)
                                }

                                val builder = AlertDialog.Builder(context)
                                builder.setTitle(R.string.alert)
                                builder.setMessage(R.string.bookingalreadyacceptedbyanotherdoctor)
                                builder.setCancelable(false)
                                builder.setPositiveButton(android.R.string.ok) { dialog, which ->
                                    builder.setCancelable(true)

                                }

                                builder.show()

                            } else {
                                  Log.d("wggvdsywgsyuwhgsui",""+methodName)
                                if (methodName == UrlHelper.ACCEPTREQUEST) {

                                    onClickListner.onClickItem(1)

                                } else {

                                }
                            }
                        }

                    }
                })

    }

    private fun showConfirmDialog() {

        DialogUtils.showSuccessDialog(requireContext(), object : SingleTapListener {
            override fun singleTap() {

            }
        }, getString(R.string.booking_successful))

    }

    private fun initListener() {

/*
        statusImage.setOnClickListener {
            if (activeStatus) {
                statusText.text = getString(R.string.oops_you_are_inactive)
                statusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.offline
                    )
                )
            } else {
                statusText.text = getString(R.string.you_are_active)
                statusImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.online
                    )
                )
            }

            activeStatus = !activeStatus
        }
*/
        binding!!.notification.setOnClickListener {
            startActivity(Intent(requireActivity(), NotificationActivity::class.java))
        }

        binding!!.scheduled.setOnClickListener {
            binding!!.scheduled.setCardBackgroundColor(getResources().getColor(R.color.com_facebook_messenger_blue))
            binding!!.txt2.setTextColor(resources.getColor(R.color.white))
            binding!!.img2.setColorFilter(resources.getColor(R.color.white))
            binding!!.todayAppoinment.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt1.setTextColor(resources.getColor(R.color.black))
            binding!!.img1.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            binding!!.consultationHistory.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt3.setTextColor(resources.getColor(R.color.black))
            binding!!.img3.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            binding!!.earnings.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt4.setTextColor(resources.getColor(R.color.black))
            binding!!.img4.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
           // onClickListner.onClickItem(1)

            //startActivity(Intent(requireActivity(), CalenderActivity::class.java))
            val myWalletFragment = CalenderFragment()
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, myWalletFragment)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }

        /*feedback.setOnClickListener {
            startActivity(Intent(requireActivity(), FeedbackActivity::class.java))
        }
*/
        binding!!.consultationHistory.setOnClickListener {
            binding!!.consultationHistory.setCardBackgroundColor(getResources().getColor(R.color.com_facebook_messenger_blue))
            binding!!.txt3.setTextColor(resources.getColor(R.color.white))
            binding!!.img3.setColorFilter(resources.getColor(R.color.white))
            binding!!.todayAppoinment.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt1.setTextColor(resources.getColor(R.color.black))
            binding!!.img1.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            binding!!.scheduled.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt2.setTextColor(resources.getColor(R.color.black))
            binding!!.img2.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            binding!!.earnings.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt4.setTextColor(resources.getColor(R.color.black))
            binding!!.img4.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            showCalender()

        }

        binding!!.earnings.setOnClickListener {
            binding!!.earnings.setCardBackgroundColor(getResources().getColor(R.color.com_facebook_messenger_blue))
            binding!!.txt4.setTextColor(resources.getColor(R.color.white))
            binding!!.img4.setColorFilter(resources.getColor(R.color.white))
            binding!!.todayAppoinment.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt1.setTextColor(resources.getColor(R.color.black))
            binding!!.img1.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            binding!!.scheduled.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt2.setTextColor(resources.getColor(R.color.black))
            binding!!.img2.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            binding!!.consultationHistory.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt3.setTextColor(resources.getColor(R.color.black))
            binding!!.img3.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            val myWalletFragment = EarningsFragment1()
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, myWalletFragment)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            //startActivity(Intent(requireActivity(), EarningsActivity::class.java))
        }

        binding!!.todayAppoinment.setOnClickListener {
            //todayAppoinment.setCardBackgroundColor(Color.parseColor("#FF0800"))
            //todayAppoinment.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.com_facebook_messenger_blue))
            binding!!.todayAppoinment.setCardBackgroundColor(getResources().getColor(R.color.com_facebook_messenger_blue))
            binding!!.txt1.setTextColor(resources.getColor(R.color.white))
            binding!!.img1.setColorFilter(resources.getColor(R.color.white))
            binding!!.scheduled.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt2.setTextColor(resources.getColor(R.color.black))
            binding!!.img2.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            binding!!.consultationHistory.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt3.setTextColor(resources.getColor(R.color.black))
            binding!!.img3.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            binding!!.earnings.setCardBackgroundColor(getResources().getColor(R.color.white))
            binding!!.txt4.setTextColor(resources.getColor(R.color.black))
            binding!!.img4.setColorFilter(resources.getColor(R.color.com_facebook_messenger_blue))
            onClickListner.onClickItem(1)
        }

    }

    private fun showCalender() {

        var calender = Calendar.getInstance()

        DatePickerDialog(
            requireContext(), R.style.CalenderPickerTheme,
            DatePickerDialog.OnDateSetListener { p0, p1, p2, p3 ->

/*
                timePicker("$p1/${BaseUtils.numberFormat(p2 + 1)}/${BaseUtils.numberFormat(p3)}")
*/

                val bundle = Bundle()
                /*val myMessage = "Stack Overflow is cool!"
                bundle.putString("message", myMessage)*/
                bundle.putString(
                    Constants.IntentKeys.DATE,
                    "$p1-${BaseUtils.numberFormat(p2 + 1)}-${BaseUtils.numberFormat(p3)}"
                )
                Log.d("xbvhcxv",""+"$p1-${BaseUtils.numberFormat(p2 + 1)}-$p3")
                bundle.putString(
                    Constants.IntentKeys.DISPLAYMONTH,
                    BaseUtils.getFormatedDate(
                        "$p1-${BaseUtils.numberFormat(p2 + 1)}-$p3",
                        "yyyy-MM-dd",
                        "MMMM"
                    )
                )

                Log.d("wertyuiop",""+bundle)
                val myWalletFragment = PatientConsultationHistoryFragment()
                myWalletFragment.setArguments(bundle)
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myWalletFragment)
                fragmentTransaction.commit()

            },
            calender.get(Calendar.YEAR),
            calender.get(Calendar.MONTH),
            calender.get(Calendar.DAY_OF_MONTH)
        ).show()


    }


    //locationconsultationHistory

    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()

                    }
                }
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireContext().checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }


    private fun getUserLocation() {
        GpsUtils(requireActivity()).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {

        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {

                myLat = location.latitude
                myLng = location.longitude

                sharedHelper?.latitude = location.latitude.toString()
                sharedHelper?.longitude = location.longitude.toString()

                homeViewModel?.updateLocation(requireContext(), myLat, myLng)

            } else {

                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )

            }
        }

    }

    fun onActivityResults(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!isDetached)
            if (requestCode == Constants.RequestCode.GPS_REQUEST) {
                if (resultCode == Activity.RESULT_OK)
                    getLastKnownLocation()
            }
    }

    private fun moveToLoginActivity(context: Context?) {

        context?.let {
            SharedHelper(it).loggedIn = false
            val intent = Intent(it, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            it.startActivity(intent)
            (it as Activity).finish()
        }

    }


}