package com.app.hakeemDoctor.view.fragment

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.FragmentAppoinmentApprovalListBinding
import com.app.hakeemDoctor.interfaces.OnClickListener
import com.app.hakeemDoctor.models.BookingData
import com.app.hakeemDoctor.models.CurrentBookingListJson
import com.app.hakeemDoctor.utils.*
import com.app.hakeemDoctor.view.activity.CancelationReasonActivity
import com.app.hakeemDoctor.view.activity.ChatActivity
import com.app.hakeemDoctor.view.activity.DashBoardActivity
import com.app.hakeemDoctor.view.activity.DoctorTrackingActivity
import com.app.hakeemDoctor.view.adapter.AppoinmentApprovalListAdapter
import com.app.hakeemDoctor.viewmodel.BookingViewModel
import com.app.hakeemUser.rxbus.RxBusNotification
import com.google.android.gms.location.*
import io.reactivex.disposables.Disposable
import java.util.*

class AppoinmentApprovalFragment : Fragment(R.layout.fragment_appoinment_approval_list) {

    var binding: FragmentAppoinmentApprovalListBinding? = null
    var viewModel: BookingViewModel? = null

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null


    private var myLat: Double? = null
    private var myLng: Double? = null
    var adapter: AppoinmentApprovalListAdapter? = null
    var sharedHelper: SharedHelper? = null

    private lateinit var notificationEventListner: Disposable
    private var isAttached = false

    var data:ArrayList<CurrentBookingListJson>? =ArrayList<CurrentBookingListJson>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentAppoinmentApprovalListBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        sharedHelper = SharedHelper(requireContext())

        locationListner()
        askLocationPermission()
        listernNotification()
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // isEnabled = false
                // activity?.onBackPressed()
                (activity as DashBoardActivity?)?.setAdapter(0)
            }
        })
        return view
    }

    override fun onResume() {
        super.onResume()
        getListOfBookings()
        isAttached = true
    }


    override fun onPause() {
        super.onPause()
        isAttached = false
    }

    private fun listernNotification() {

        notificationEventListner =
            RxBusNotification.listen(String::class.java).doOnError {
            }.subscribe {
                ThreadUtils.runOnUiThread {
                    if (isAttached)
                        getListOfBookings()
                }
            }
    }

    private fun getListOfBookings() {

        DialogUtils.showLoader(requireContext())
        viewModel?.getBookingList(requireContext(), BaseUtils.getCurrentDate("yyyy-MM-dd"))
            ?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data1 ->
                               // handleResponse(data)



                                data!!.clear()
                                for (i in 0 until data1.size){
                                    if(data1[i].status=="pending"||data1[i].status=="startvisit"){
                                        Log.d("hxdeuesuue","true")
                                        Log.d("hxdeuesuue","true"+data1[i].status)
                                        Log.d("hxdeuesuue","true"+ sharedHelper?.chatTiming!!)
                                  /*      var timee:String=""
                                        data1[i].bookingDate!!.let {
                                            data1[i].bookingTime?.let { time ->
                                                timee = BaseUtils.addEndTime(
                                                    "${BaseUtils.getFormatedDateUtc(
                                                        it,
                                                        "yyyy-MM-dd HH:mm",
                                                        "dd/MM/yyyy hh:mm a"
                                                    )}",
                                                    "dd/MM/yyyy hh:mm a",
                                                    "dd/MM/yyyy hh:mm a",
                                                    sharedHelper?.chatTiming!!
                                                ).toString()
                                            }
                                        }
                                        Log.d("azsaza","QASQQQQ"+timee)
*/


                                        if (data1[i].booking_type=="virtual"){
                                            if (BaseUtils.isConsultationTimeOver(
                                                    BaseUtils.getFormatedDateUtc(
                                                        data1[i].bookingDate!!,
                                                        "yyyy-MM-dd HH:mm",
                                                        "dd/MM/yyyy hh:mm a"
                                                    )!!, "dd/MM/yyyy hh:mm a"
                                                    , sharedHelper?.chatTiming!!
                                                )
                                            ){

                                            }else{

                                                if (data1[i].ins_status==null){
                                                    data1[i].ins_status=""

                                                    data!!.add(
                                                        CurrentBookingListJson(
                                                            data1[i].bookingId!!,
                                                            data1[i].bookingIds!!,
                                                            data1[i].isVirtualBooking!!,
                                                            data1[i].profilePic!!,
                                                            data1[i].providerName!!,
                                                            data1[i].expierence!!,
                                                            data1[i].previousIssue!!,
                                                            data1[i].fee!!,
                                                            data1[i].bookingDate!!,
                                                            data1[i].bookingTime!!,
                                                            data1[i].booking_type!!,
                                                            data1[i].status!!,
                                                            data1[i].specialityName!!,
                                                            data1[i].is_ins!!,
                                                            data1[i].isFeatureBooking!!,
                                                            data1[i].bookingPeriod!!,
                                                            data1[i].email!!,
                                                            data1[i].DOB!!,
                                                            data1[i].location!!,
                                                            data1[i].bookingLatitude!!,
                                                            data1[i].bookingLongitude!!,
                                                            data1[i].patientId!!,
                                                            data1[i].patientName!!,
                                                            data1[i].ins_status!!,
                                                            data1[i].patientAge!!,
                                                            data1[i].mobileNumber!!,
                                                            data1[i].countryCode!!,
                                                            data1[i].name!!
                                                        )
                                                    )
                                                }
                                                else{
                                                    data!!.add(
                                                        CurrentBookingListJson(
                                                            data1[i].bookingId!!,
                                                            data1[i].bookingIds!!,
                                                            data1[i].isVirtualBooking!!,
                                                            data1[i].profilePic!!,
                                                            data1[i].providerName!!,
                                                            data1[i].expierence!!,
                                                            data1[i].previousIssue!!,
                                                            data1[i].fee!!,
                                                            data1[i].bookingDate!!,
                                                            data1[i].bookingTime!!,
                                                            data1[i].booking_type!!,
                                                            data1[i].status!!,
                                                            data1[i].specialityName!!,
                                                            data1[i].is_ins!!,
                                                            data1[i].isFeatureBooking!!,
                                                            data1[i].bookingPeriod!!,
                                                            data1[i].email!!,
                                                            data1[i].DOB!!,
                                                            data1[i].location!!,
                                                            data1[i].bookingLatitude!!,
                                                            data1[i].bookingLongitude!!,
                                                            data1[i].patientId!!,
                                                            data1[i].patientName!!,
                                                            data1[i].ins_status!!,
                                                            data1[i].patientAge!!,
                                                            data1[i].mobileNumber!!,
                                                            data1[i].countryCode!!,
                                                            data1[i].name!!
                                                        )
                                                    )
                                                }

                                            }
                                        }
                                        else{

                                            Log.d("vgvhbhj","ghghgghghghgh")


                                           /* if (data1[i].ins_status==null){
                                                data1[i].ins_status=""

                                                data!!.add(
                                                    CurrentBookingListJson(
                                                        data1[i].bookingId!!,
                                                        data1[i].bookingIds!!,
                                                        data1[i].isVirtualBooking!!,
                                                        data1[i].profilePic!!,
                                                        data1[i].providerName!!,
                                                        data1[i].expierence!!,
                                                        data1[i].previousIssue!!,
                                                        data1[i].fee!!,
                                                        data1[i].bookingDate!!,
                                                        data1[i].bookingTime!!,
                                                        data1[i].booking_type!!,
                                                        data1[i].status!!,
                                                        data1[i].specialityName!!,
                                                        data1[i].is_ins!!,
                                                        data1[i].isFeatureBooking!!,
                                                        data1[i].bookingPeriod!!,
                                                        data1[i].email!!,
                                                        data1[i].DOB!!,
                                                        data1[i].location!!,
                                                        data1[i].bookingLatitude!!,
                                                        data1[i].bookingLongitude!!,
                                                        data1[i].patientId!!,
                                                        data1[i].patientName!!,
                                                        data1[i].ins_status!!,
                                                        data1[i].patientAge!!,
                                                        data1[i].mobileNumber!!,
                                                        data1[i].countryCode!!,
                                                        data1[i].name!!
                                                    )
                                                )
                                            }
                                            else{
                                                data!!.add(
                                                    CurrentBookingListJson(
                                                        data1[i].bookingId!!,
                                                        data1[i].bookingIds!!,
                                                        data1[i].isVirtualBooking!!,
                                                        data1[i].profilePic!!,
                                                        data1[i].providerName!!,
                                                        data1[i].expierence!!,
                                                        data1[i].previousIssue!!,
                                                        data1[i].fee!!,
                                                        data1[i].bookingDate!!,
                                                        data1[i].bookingTime!!,
                                                        data1[i].booking_type!!,
                                                        data1[i].status!!,
                                                        data1[i].specialityName!!,
                                                        data1[i].is_ins!!,
                                                        data1[i].isFeatureBooking!!,
                                                        data1[i].bookingPeriod!!,
                                                        data1[i].email!!,
                                                        data1[i].DOB!!,
                                                        data1[i].location!!,
                                                        data1[i].bookingLatitude!!,
                                                        data1[i].bookingLongitude!!,
                                                        data1[i].patientId!!,
                                                        data1[i].patientName!!,
                                                        data1[i].ins_status!!,
                                                        data1[i].patientAge!!,
                                                        data1[i].mobileNumber!!,
                                                        data1[i].countryCode!!,
                                                        data1[i].name!!
                                                    )
                                                )
                                            }*/

                                                 if (data1[i].booking_type == "emergency") {


                                                     if (data1[i].providerName == null && data1[i].profilePic == null && data1[i].previousIssue == null && data1[i].education == null && data1[i].expierence == null && data1[i].previousIssue == null) {
                                                         data1[i].providerName = ""
                                                         data1[i].profilePic = ""
                                                         data1[i].previousIssue = "Not Mentioned"
                                                         data1[i].education = ""
                                                         data1[i].expierence = ""
                                                         data!!.add(
                                                             CurrentBookingListJson(
                                                                 data1[i].bookingId!!,
                                                                 data1[i].bookingIds!!,
                                                                 data1[i].isVirtualBooking!!,
                                                                 data1[i].profilePic!!,
                                                                 data1[i].providerName!!,
                                                                 data1[i].expierence!!,
                                                                 data1[i].previousIssue!!,
                                                                 data1[i].fee!!,
                                                                 data1[i].bookingDate!!,
                                                                 data1[i].bookingTime!!,
                                                                 data1[i].booking_type!!,
                                                                 data1[i].status!!,
                                                                 data1[i].specialityName!!,
                                                                 data1[i].is_ins!!,
                                                                 data1[i].isFeatureBooking!!,
                                                                 data1[i].bookingPeriod!!,
                                                                 data1[i].email!!,
                                                                 data1[i].DOB!!,
                                                                 data1[i].location!!,
                                                                 data1[i].bookingLatitude!!,
                                                                 data1[i].bookingLongitude!!,
                                                                 data1[i].patientId!!,
                                                                 data1[i].patientName!!,
                                                                 data1[i].ins_status!!,
                                                                 data1[i].patientAge!!,
                                                                 data1[i].mobileNumber!!,
                                                                 data1[i].countryCode!!,
                                                                 data1[i].name!!
                                                             )
                                                         )
                                                     } else {
                                                         if (data1[i].previousIssue == null) {
                                                             data1[i].previousIssue = ""


                                                             if ( data1[i].ins_status==null){
                                                                 data1[i].ins_status = ""

                                                                 data!!.add(
                                                                     CurrentBookingListJson(
                                                                         data1[i].bookingId!!,
                                                                         data1[i].bookingIds!!,
                                                                         data1[i].isVirtualBooking!!,
                                                                         data1[i].profilePic!!,
                                                                         data1[i].providerName!!,
                                                                         data1[i].expierence!!,
                                                                         data1[i].previousIssue!!,
                                                                         data1[i].fee!!,
                                                                         data1[i].bookingDate!!,
                                                                         data1[i].bookingTime!!,
                                                                         data1[i].booking_type!!,
                                                                         data1[i].status!!,
                                                                         data1[i].specialityName!!,
                                                                         data1[i].is_ins!!,
                                                                         data1[i].isFeatureBooking!!,
                                                                         data1[i].bookingPeriod!!,
                                                                         data1[i].email!!,
                                                                         data1[i].DOB!!,
                                                                         data1[i].location!!,
                                                                         data1[i].bookingLatitude!!,
                                                                         data1[i].bookingLongitude!!,
                                                                         data1[i].patientId!!,
                                                                         data1[i].patientName!!,
                                                                         data1[i].ins_status!!,
                                                                         data1[i].patientAge!!,
                                                                         data1[i].mobileNumber!!,
                                                                         data1[i].countryCode!!,
                                                                         data1[i].name!!
                                                                     )
                                                                 )
                                                             }else{


                                                                 data!!.add(
                                                                     CurrentBookingListJson(
                                                                         data1[i].bookingId!!,
                                                                         data1[i].bookingIds!!,
                                                                         data1[i].isVirtualBooking!!,
                                                                         data1[i].profilePic!!,
                                                                         data1[i].providerName!!,
                                                                         data1[i].expierence!!,
                                                                         data1[i].previousIssue!!,
                                                                         data1[i].fee!!,
                                                                         data1[i].bookingDate!!,
                                                                         data1[i].bookingTime!!,
                                                                         data1[i].booking_type!!,
                                                                         data1[i].status!!,
                                                                         data1[i].specialityName!!,
                                                                         data1[i].is_ins!!,
                                                                         data1[i].isFeatureBooking!!,
                                                                         data1[i].bookingPeriod!!,
                                                                         data1[i].email!!,
                                                                         data1[i].DOB!!,
                                                                         data1[i].location!!,
                                                                         data1[i].bookingLatitude!!,
                                                                         data1[i].bookingLongitude!!,
                                                                         data1[i].patientId!!,
                                                                         data1[i].patientName!!,
                                                                         data1[i].ins_status!!,
                                                                         data1[i].patientAge!!,
                                                                         data1[i].mobileNumber!!,
                                                                         data1[i].countryCode!!,
                                                                         data1[i].name!!
                                                                     )
                                                                 )
                                                             }

                                                         }else{
                                                             data1[i].ins_status = ""

                                                             data!!.add(
                                                                 CurrentBookingListJson(
                                                                     data1[i].bookingId!!,
                                                                     data1[i].bookingIds!!,
                                                                     data1[i].isVirtualBooking!!,
                                                                     data1[i].profilePic!!,
                                                                     data1[i].providerName!!,
                                                                     data1[i].expierence!!,
                                                                     data1[i].previousIssue!!,
                                                                     data1[i].fee!!,
                                                                     data1[i].bookingDate!!,
                                                                     data1[i].bookingTime!!,
                                                                     data1[i].booking_type!!,
                                                                     data1[i].status!!,
                                                                     data1[i].specialityName!!,
                                                                     data1[i].is_ins!!,
                                                                     data1[i].isFeatureBooking!!,
                                                                     data1[i].bookingPeriod!!,
                                                                     data1[i].email!!,
                                                                     data1[i].DOB!!,
                                                                     data1[i].location!!,
                                                                     data1[i].bookingLatitude!!,
                                                                     data1[i].bookingLongitude!!,
                                                                     data1[i].patientId!!,
                                                                     data1[i].patientName!!,
                                                                     data1[i].ins_status!!,
                                                                     data1[i].patientAge!!,
                                                                     data1[i].mobileNumber!!,
                                                                     data1[i].countryCode!!,
                                                                     data1[i].name!!
                                                                 )
                                                             )
                                                         }

                                                     }
                                                     Log.d("swhgbxywhxg", "whsuitrueeeee")
                                                 }
                                                 else {

                                                     if (data1[i].ins_status==null){
                                                         data1[i].ins_status=""

                                                         if ( data1[i].previousIssue==null){
                                                             data1[i].previousIssue=""
                                                             Log.d("swhgbxywhxg", "thgftgghfvfgn")
                                                             data!!.add(
                                                                 CurrentBookingListJson(
                                                                     data1[i].bookingId!!,
                                                                     data1[i].bookingIds!!,
                                                                     data1[i].isVirtualBooking!!,
                                                                     data1[i].profilePic!!,
                                                                     data1[i].providerName!!,
                                                                     data1[i].expierence!!,
                                                                     data1[i].previousIssue!!,
                                                                     data1[i].fee!!,
                                                                     data1[i].bookingDate!!,
                                                                     data1[i].bookingTime!!,
                                                                     data1[i].booking_type!!,
                                                                     data1[i].status!!,
                                                                     data1[i].specialityName!!,
                                                                     data1[i].is_ins!!,
                                                                     data1[i].isFeatureBooking!!,
                                                                     data1[i].bookingPeriod!!,
                                                                     data1[i].email!!,
                                                                     data1[i].DOB!!,
                                                                     data1[i].location!!,
                                                                     data1[i].bookingLatitude!!,
                                                                     data1[i].bookingLongitude!!,
                                                                     data1[i].patientId!!,
                                                                     data1[i].patientName!!,
                                                                     data1[i].ins_status!!,
                                                                     data1[i].patientAge!!,
                                                                     data1[i].mobileNumber!!,
                                                                     data1[i].countryCode!!,
                                                                     data1[i].name!!
                                                                 )
                                                             )
                                                         }else{
                                                             Log.d("swhgbxywhxg", "bnbnbbbnnbnbbn")

                                                             data!!.add(
                                                                 CurrentBookingListJson(
                                                                     data1[i].bookingId!!,
                                                                     data1[i].bookingIds!!,
                                                                     data1[i].isVirtualBooking!!,
                                                                     data1[i].profilePic!!,
                                                                     data1[i].providerName!!,
                                                                     data1[i].expierence!!,
                                                                     data1[i].previousIssue!!,
                                                                     data1[i].fee!!,
                                                                     data1[i].bookingDate!!,
                                                                     data1[i].bookingTime!!,
                                                                     data1[i].booking_type!!,
                                                                     data1[i].status!!,
                                                                     data1[i].specialityName!!,
                                                                     data1[i].is_ins!!,
                                                                     data1[i].isFeatureBooking!!,
                                                                     data1[i].bookingPeriod!!,
                                                                     data1[i].email!!,
                                                                     data1[i].DOB!!,
                                                                     data1[i].location!!,
                                                                     data1[i].bookingLatitude!!,
                                                                     data1[i].bookingLongitude!!,
                                                                     data1[i].patientId!!,
                                                                     data1[i].patientName!!,
                                                                     data1[i].ins_status!!,
                                                                     data1[i].patientAge!!,
                                                                     data1[i].mobileNumber!!,
                                                                     data1[i].countryCode!!,
                                                                     data1[i].name!!
                                                                 )
                                                             )
                                                         }

                                                     }
                                                     else{

                                                         Log.d("gvhvghvh","ghvgyesitsterue")
                                                         data!!.add(
                                                             CurrentBookingListJson(
                                                                 data1[i].bookingId!!,
                                                                 data1[i].bookingIds!!,
                                                                 data1[i].isVirtualBooking!!,
                                                                 data1[i].profilePic!!,
                                                                 data1[i].providerName!!,
                                                                 data1[i].expierence!!,
                                                                 data1[i].previousIssue!!,
                                                                 data1[i].fee!!,
                                                                 data1[i].bookingDate!!,
                                                                 data1[i].bookingTime!!,
                                                                 data1[i].booking_type!!,
                                                                 data1[i].status!!,
                                                                 data1[i].specialityName!!,
                                                                 data1[i].is_ins!!,
                                                                 data1[i].isFeatureBooking!!,
                                                                 data1[i].bookingPeriod!!,
                                                                 data1[i].email!!,
                                                                 data1[i].DOB!!,
                                                                 data1[i].location!!,
                                                                 data1[i].bookingLatitude!!,
                                                                 data1[i].bookingLongitude!!,
                                                                 data1[i].patientId!!,
                                                                 data1[i].patientName!!,
                                                                 data1[i].ins_status!!,
                                                                 data1[i].patientAge!!,
                                                                 data1[i].mobileNumber!!,
                                                                 data1[i].countryCode!!,
                                                                 data1[i].name!!
                                                             )
                                                         )
                                                     }


                                                        this.data = data

                                                   Log.d("hxdeuesuue", "true" + data!!.size)
                                                   Log.d("hxdeuesuue", "true" + data)
                                            }
                                        }

                              /*          if (BaseUtils.isConsultationTimeOver(
                                                BaseUtils.getFormatedDateUtc(
                                                    data1[i].bookingDate!!,
                                                    "yyyy-MM-dd HH:mm",
                                                    "dd/MM/yyyy hh:mm a"
                                                )!!, "dd/MM/yyyy hh:mm a"
                                                , sharedHelper?.chatTiming!!
                                            )
                                        ) {

                                            if (data1[i].booking_type == "emergency") {


                                                if (data1[i].providerName == null && data1[i].profilePic == null && data1[i].previousIssue == null && data1[i].education == null && data1[i].expierence == null && data1[i].previousIssue == null) {
                                                    data1[i].providerName = ""
                                                    data1[i].profilePic = ""
                                                    data1[i].previousIssue = "Not Mentioned"
                                                    data1[i].education = ""
                                                    data1[i].expierence = ""
                                                    data!!.add(
                                                        CurrentBookingListJson(
                                                            data1[i].bookingId!!,
                                                            data1[i].bookingIds!!,
                                                            data1[i].isVirtualBooking!!,
                                                            data1[i].profilePic!!,
                                                            data1[i].providerName!!,
                                                            data1[i].expierence!!,
                                                            data1[i].previousIssue!!,
                                                            data1[i].fee!!,
                                                            data1[i].bookingDate!!,
                                                            data1[i].bookingTime!!,
                                                            data1[i].booking_type!!,
                                                            data1[i].status!!,
                                                            data1[i].specialityName!!,
                                                            data1[i].is_ins!!,
                                                            data1[i].isFeatureBooking!!,
                                                            data1[i].bookingPeriod!!,
                                                            data1[i].email!!,
                                                            data1[i].DOB!!,
                                                            data1[i].location!!,
                                                            data1[i].bookingLatitude!!,
                                                            data1[i].bookingLongitude!!,
                                                            data1[i].patientId!!,
                                                            data1[i].patientName!!,
                                                            data1[i].ins_status!!,
                                                            data1[i].patientAge!!,
                                                            data1[i].mobileNumber!!,
                                                            data1[i].countryCode!!,
                                                            data1[i].name!!
                                                        )
                                                    )
                                                } else {
                                                    if (data1[i].previousIssue == null|| data1[i].ins_status==null) {
                                                        data1[i].previousIssue = "Not mentioned"
                                                        data1[i].ins_status = ""

                                                        data!!.add(
                                                            CurrentBookingListJson(
                                                                data1[i].bookingId!!,
                                                                data1[i].bookingIds!!,
                                                                data1[i].isVirtualBooking!!,
                                                                data1[i].profilePic!!,
                                                                data1[i].providerName!!,
                                                                data1[i].expierence!!,
                                                                data1[i].previousIssue!!,
                                                                data1[i].fee!!,
                                                                data1[i].bookingDate!!,
                                                                data1[i].bookingTime!!,
                                                                data1[i].booking_type!!,
                                                                data1[i].status!!,
                                                                data1[i].specialityName!!,
                                                                data1[i].is_ins!!,
                                                                data1[i].isFeatureBooking!!,
                                                                data1[i].bookingPeriod!!,
                                                                data1[i].email!!,
                                                                data1[i].DOB!!,
                                                                data1[i].location!!,
                                                                data1[i].bookingLatitude!!,
                                                                data1[i].bookingLongitude!!,
                                                                data1[i].patientId!!,
                                                                data1[i].patientName!!,
                                                                data1[i].ins_status!!,
                                                                data1[i].patientAge!!,
                                                                data1[i].mobileNumber!!,
                                                                data1[i].countryCode!!,
                                                                data1[i].name!!
                                                            )
                                                        )
                                                    }

                                                }
                                                Log.d("swhgbxywhxg", "whsuitrueeeee")
                                            } else {



                                             *//*   this.data = data

                                                Log.d("hxdeuesuue", "true" + data!!.size)
                                                Log.d("hxdeuesuue", "true" + data)*//*
                                            }
                                        }else{


                                            if (data1[i].previousIssue == null|| data1[i].ins_status==null) {
                                                //data1[i].previousIssue = "Not mentioned"
                                                data1[i].ins_status = ""

                                                data!!.add(
                                                    CurrentBookingListJson(
                                                        data1[i].bookingId!!,
                                                        data1[i].bookingIds!!,
                                                        data1[i].isVirtualBooking!!,
                                                        data1[i].profilePic!!,
                                                        data1[i].providerName!!,
                                                        data1[i].expierence!!,
                                                        data1[i].previousIssue!!,
                                                        data1[i].fee!!,
                                                        data1[i].bookingDate!!,
                                                        data1[i].bookingTime!!,
                                                        data1[i].booking_type!!,
                                                        data1[i].status!!,
                                                        data1[i].specialityName!!,
                                                        data1[i].is_ins!!,
                                                        data1[i].isFeatureBooking!!,
                                                        data1[i].bookingPeriod!!,
                                                        data1[i].email!!,
                                                        data1[i].DOB!!,
                                                        data1[i].location!!,
                                                        data1[i].bookingLatitude!!,
                                                        data1[i].bookingLongitude!!,
                                                        data1[i].patientId!!,
                                                        data1[i].patientName!!,
                                                        data1[i].ins_status!!,
                                                        data1[i].patientAge!!,
                                                        data1[i].mobileNumber!!,
                                                        data1[i].countryCode!!,
                                                        data1[i].name!!
                                                    )
                                                )
                                            }

                                        }
*/
                                    }

                                    handleResponse(data!!)
                                }
                            }
                        }
                    }

                }
            })
    }

    private fun handleResponse(data: ArrayList<CurrentBookingListJson>) {
        adapter =
            AppoinmentApprovalListAdapter(
                requireContext(), myLat, myLng,
                object : OnClickListener {
                    override fun onClickItem(position: Int) {
                        if (data[position].status.equals("completed", true) ||
                            data[position].status.equals("cancelledbyprovider", true) ||
                            data[position].status.equals("cancelledbypatient", true) ||
                            data[position].status.equals("paymentverified", true)
                        ) {

                        } else {
                            Log.d("wertyhujikolp;",""+    data[position].bookingIds)

                            startActivity(

                                Intent(requireContext(), DoctorTrackingActivity::class.java)
                                    .putExtra(
                                        Constants.IntentKeys.BOOKINGID,
                                        data[position].bookingId
                                    )
                                    .putExtra(
                                        "bookingids",
                                        data[position].bookingIds
                                    )
                                    .putExtra(
                                        Constants.IntentKeys.PATIENTID,
                                        data[position].patientId
                                    )
                            )

                        }


                    }
                }, data, object : OnClickListener {
                    override fun onClickItem(position: Int) {
                        rejectBooking(position, data)
                    }

                }, object : OnClickListener {
                    override fun onClickItem(position: Int) {
                          Log.d("sdfghjkl;",""+data[position].bookingId)
                          Log.d("sdfghjkl;",""+data[position].patientId)
                        startActivity(
                            Intent(requireContext(), ChatActivity::class.java)
                                .putExtra(Constants.IntentKeys.DOCTORSID, data[position].patientId)
                                .putExtra(
                                    Constants.IntentKeys.DOCTORNAME,
                                    data[position].patientName
                                )
                                .putExtra(
                                    Constants.IntentKeys.DOCTORIMAGE,
                                    data[position].profilePic
                                ).putExtra(Constants.IntentKeys.BOOKINGID,data[position].bookingId)

                        )
                    }

                })
        binding!!.consultationList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding!!.consultationList.adapter = adapter

    }


    private fun rejectBooking(position: Int, data: ArrayList<CurrentBookingListJson>) {

        startActivity(
            Intent(requireContext(), CancelationReasonActivity::class.java)
                .putExtra(Constants.IntentKeys.BOOKINGID, data[position].bookingId)
        )


    }


    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()

                    }
                }
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireContext().checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }


    private fun getUserLocation() {
        GpsUtils(requireActivity()).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }

    private fun getLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {

                myLat = location.latitude
                myLng = location.longitude

                adapter?.setLatLng(myLat!!, myLng!!)

            } else {

                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )

            }
        }

    }

    fun onActivityResults(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!isDetached)
            if (requestCode == Constants.RequestCode.GPS_REQUEST) {
                if (resultCode == Activity.RESULT_OK)
                    getLastKnownLocation()
            }
    }

}