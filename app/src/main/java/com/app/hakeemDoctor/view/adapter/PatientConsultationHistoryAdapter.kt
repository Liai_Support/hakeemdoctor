package com.app.hakeemDoctor.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ChildConsultationHistoryBinding
import com.app.hakeemDoctor.models.ConsultationHistoryData
import com.app.hakeemDoctor.models.MedData
import com.app.hakeemDoctor.models.SlabData
import com.app.hakeemDoctor.models.TestData
import com.app.hakeemDoctor.utils.BaseUtils
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.view.activity.InvoiceDetailsActivity
import java.util.*

class PatientConsultationHistoryAdapter(
    var context: Context,
    var data: ArrayList<ConsultationHistoryData>,
    var latitude: String,
    var longitude: String
) :
    RecyclerView.Adapter<PatientConsultationHistoryAdapter.ViewHolder>() {


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildConsultationHistoryBinding = ChildConsultationHistoryBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_consultation_history,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(data[position].is_ins == 1){
            if(data[position].ins_status!= null && data[position].ins_status!!.equals("paid")){


                if ( data[position].booking_type=="home_visit"){
                    holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.binding!!.textViewststuss.text =" Home visit "

                }else if ( data[position].booking_type=="virtual"){

                    holder.binding!!.textViewststuss.text =" Virtual Consult"
                    holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                }else if ( data[position].booking_type=="emergency"){
                    holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                    holder.binding!!.textViewststuss.text =" Emergency"

                }
                    //    holder.binding?.bookingid!!.text = "" + context.getString(R.string.bookingid) + "" + data[position].bookingIds.toString()
                      holder.binding?.bookingid1!!.text = data[position].bookingIds.toString()
                data[position].isFeatureBooking?.let {
                    if (it == "true") {
                        data[position].patientName?.let { holder.binding.patientName.text = it }
                        data[position].patientAge?.let {
                            holder.binding.yearsOld.text = "$it years old"
                        }
                       // data[position].previousIssue?.let { holder.binding.healthIssue.text = it }
                        holder.binding.healthIssueText.visibility = View.VISIBLE
                    } else {
                        holder.binding.healthIssueText.visibility = View.VISIBLE
                       holder.binding.healthIssue.text = context.getString(R.string.notmentioned)
                        data[position].name?.let { holder.binding.patientName.text = it }
                        data[position].DOB?.let {
                            holder.binding.yearsOld.text =
                                "${BaseUtils.getAgeCalculation(it)} years old"
                        }
                    }
                }

                if(data[position].previousIssue.equals("not mentioned",true)){
                    holder.binding?.healthIssue?.text = "Not mentioned"
                }else  if (data[position].previousIssue!!.isBlank()||data[position].previousIssue!!.isEmpty()){
                    holder.binding!!.healthIssue.text = "Not mentioned"

                }else if(data[position].previousIssue==""||data[position].previousIssue!!.length==0) {
                    holder.binding!!.healthIssue.text = "Not mentioned"

                }else{
                    holder.binding?.healthIssue?.text = data[position].previousIssue
                }
                data[position].fee?.let {
                    if (data[position].extraFee != null) {
                        holder.binding.fees.text =
                            "${it.toInt() + data[position].extraFee!!.toInt()} SAR"
                    } else {
                        holder.binding.fees.text = "$it SAR"
                    }
                }
                data[position].location?.let { holder.binding.location.text = it }
                data[position].bookingTime?.let { time ->
                    data[position].bookingPeriod?.let { period ->
                        holder.binding.time.text =
                            "${BaseUtils.getFormatedDate(time, "HH:mm:ss", "hh:mm")} $period"
                    }
                }
                data[position].bookingLatitude?.let { bkLat ->
                    data[position].bookingLongitude?.let { bkLng ->
                        latitude.let { lat ->
                            longitude.let { lng ->
                                holder.binding.kmAway.text =
                                    "${
                                        BaseUtils.calculateDistance(
                                            bkLat.toDouble(),
                                            bkLng.toDouble(),
                                            lat.toDouble(),
                                            lng.toDouble()
                                        )
                                    } km away"
                            }
                        }
                    }
                }
                data[position].bookingDate?.let {
                    holder.binding.date.text =
                        BaseUtils.getFormatedDate(it, "yyyy-MM-dd", "dd,MMMM,yyyy")
                }

                holder.binding.root.setOnClickListener {
                    if (data[position].status.equals("completed", true) ||
                        data[position].status.equals("cancelledbyprovider", true) ||
                        data[position].status.equals("cancelledbypatient", true) ||
                        data[position].status.equals("paymentverified", true)
                    ) {
                        context.startActivity(
                            Intent(context, InvoiceDetailsActivity::class.java)
                                .putExtra(Constants.IntentKeys.PAYMENTDETAIL, data[position] as Parcelable)
                                .putExtra("lab",data[position].suggested_labs as ArrayList<SlabData>)
                                .putExtra("med",data[position].medicine as ArrayList<MedData>)
                                .putExtra("test",data[position].test_to_take as ArrayList<TestData>)
                        )

                    }
                }
            }
            else{
                holder.binding!!.main.visibility = View.GONE
            }
        }
        else {

            if ( data[position].booking_type=="home_visit"){
                holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                holder.binding!!.textViewststuss.text =" Home visit "

            }else if ( data[position].booking_type=="virtual"){

                holder.binding!!.textViewststuss.text =" Virtual Consult"
                holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

            }else if ( data[position].booking_type=="emergency"){
                holder.binding?.fee?.text = " : "+data[position].fee + " SAR"

                holder.binding!!.textViewststuss.text =" Emergency"

            }
            if(data[position].previousIssue.equals("not mentioned",true)){
                holder.binding?.healthIssue?.text = "Not mentioned"
            }else if (data[position].previousIssue.equals("")){
                holder.binding?.healthIssue?.text = "Not mentioned"

            }else{
                data[position].previousIssue?.let { holder.binding.healthIssue.text = it }
            }

            holder.binding?.bookingid1!!.text = data[position].bookingIds.toString()

            //  holder.binding?.bookingid!!.text = "" + context.getString(R.string.bookingid) + "" + data[position].bookingIds.toString()
            data[position].isFeatureBooking?.let {
                if (it == "true") {
                    data[position].patientName?.let { holder.binding.patientName.text = it }
                    data[position].patientAge?.let {
                        holder.binding.yearsOld.text = "$it years old"
                    }
                  //  data[position].previousIssue?.let { holder.binding.healthIssue.text = it }
                    holder.binding.healthIssueText.visibility = View.VISIBLE
                } else {
                    holder.binding.healthIssueText.visibility = View.VISIBLE
                    holder.binding.healthIssue.text = context.getString(R.string.notmentioned)
                    data[position].name?.let { holder.binding.patientName.text = it }
                    data[position].DOB?.let {
                        holder.binding.yearsOld.text =
                            "${BaseUtils.getAgeCalculation(it)} years old"
                    }
                }
            }
            data[position].fee?.let {
                if (data[position].extraFee != null) {
                    holder.binding.fees.text =
                        "${it.toInt() + data[position].extraFee!!.toInt()} SAR"
                } else {
                    holder.binding.fees.text = "$it SAR"
                }
            }
            data[position].location?.let { holder.binding.location.text = it }
            data[position].bookingTime?.let { time ->
                data[position].bookingPeriod?.let { period ->
                    holder.binding.time.text =
                        "${BaseUtils.getFormatedDate(time, "HH:mm:ss", "hh:mm")} $period"
                }
            }
            data[position].bookingLatitude?.let { bkLat ->
                data[position].bookingLongitude?.let { bkLng ->
                    latitude.let { lat ->
                        longitude.let { lng ->
                            holder.binding.kmAway.text =
                                "${
                                    BaseUtils.calculateDistance(
                                        bkLat.toDouble(),
                                        bkLng.toDouble(),
                                        lat.toDouble(),
                                        lng.toDouble()
                                    )
                                } km away"
                        }
                    }
                }
            }
            data[position].bookingDate?.let {
                holder.binding.date.text =
                    BaseUtils.getFormatedDate(it, "yyyy-MM-dd", "dd,MMMM,yyyy")
            }

            holder.binding.root.setOnClickListener {
                if (data[position].status.equals("completed", true) ||
                    data[position].status.equals("cancelledbyprovider", true) ||
                    data[position].status.equals("cancelledbypatient", true) ||
                    data[position].status.equals("paymentverified", true)
                ) {
                    context.startActivity(
                        Intent(context, InvoiceDetailsActivity::class.java)
                            .putExtra(Constants.IntentKeys.PAYMENTDETAIL, data[position] as Parcelable)
                            .putExtra("lab",data[position].suggested_labs as ArrayList<SlabData>)
                            .putExtra("med",data[position].medicine as ArrayList<MedData>)
                            .putExtra("test",data[position].test_to_take as ArrayList<TestData>)
                    )

                }
            }
        }
    }
}