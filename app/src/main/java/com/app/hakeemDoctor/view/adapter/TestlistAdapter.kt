package com.app.hakeemDoctor.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ChildTestListBinding
import com.app.hakeemDoctor.models.TestDetails
import com.app.hakeemDoctor.view.activity.EnterNotesActivity
import java.util.*

class TestlistAdapter(var enterNotesActivity: EnterNotesActivity, var context: Context, var list: ArrayList<TestDetails>) : RecyclerView.Adapter<TestlistAdapter.MyViweHolder>() {

    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {
        var childPatientListBinding: ChildTestListBinding = ChildTestListBinding.bind(view)

    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int): TestlistAdapter.MyViweHolder {

        return MyViweHolder(
                LayoutInflater.from(context).inflate(R.layout.child_test_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TestlistAdapter.MyViweHolder, position: Int) {

        holder.childPatientListBinding.test.text = list[position].ename


        holder.childPatientListBinding.test.setChecked(list.get(position).isseleted!!)

        holder.childPatientListBinding.test.setTag(position);

        holder.childPatientListBinding.test.setOnClickListener(View.OnClickListener {
            val pos = holder.childPatientListBinding.test.getTag() as Int
            // Toast.makeText(context, list.get(pos).ename.toString() + " clicked!", Toast.LENGTH_SHORT).show()
            if (list.get(pos).isseleted!!) {
                list.get(pos).isseleted = false
                enterNotesActivity.testidarray.remove(list[position].id!!)
            }
            else {
                list.get(pos).isseleted = true
                enterNotesActivity.testidarray.add(list[position].id!!)
            }

        })



    }



}








