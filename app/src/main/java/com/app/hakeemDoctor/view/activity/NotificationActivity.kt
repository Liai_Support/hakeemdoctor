package com.app.hakeemDoctor.view.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityNotificationBinding
import com.app.hakeemDoctor.databinding.ActivitySocialLoginDetailsBinding
import com.app.hakeemDoctor.interfaces.DialogCallBack
import com.app.hakeemDoctor.models.NotificationListData
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.SharedHelper
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.view.adapter.NotificationAdapter
import com.app.hakeemDoctor.viewmodel.BookingViewModel

class NotificationActivity : BaseActivity() {

    var binding: ActivityNotificationBinding? = null

    var viewModel: BookingViewModel? = null
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_notification)

        sharedHelper = SharedHelper(this)

        viewModel = ViewModelProvider(this).get(BookingViewModel::class.java)

        getNotification()

        if (sharedHelper!!.language.equals("ar")){
            binding!!.imageView14.rotation=180F
        }
    }

    private fun getNotification() {
        DialogUtils.showLoader(this)
        viewModel?.getNotificationData(this)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data)
                        }
                    }
                }

            }
        })
    }

    private fun handleResponse(data: ArrayList<NotificationListData>?) {

        data?.let {
            if (it.size != 0) {

                var adapter = NotificationAdapter(this, it)
                binding!!.listView.layoutManager = LinearLayoutManager(this)
                binding!!.listView.adapter = adapter
            }
        }
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    fun onClearClicked(view: View) {


        DialogUtils.showAlertDialog(this,
            getString(R.string.clear_notification),
            getString(R.string.confirm),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {
                    DialogUtils.showLoader(this@NotificationActivity)
                    viewModel?.clearNotification(this@NotificationActivity)
                        ?.observe(this@NotificationActivity, Observer {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->
                                            UiUtils.showSnack(binding!!.root, msg)
                                        }
                                    } else {
                                        var adapter =
                                            NotificationAdapter(
                                                this@NotificationActivity,
                                                ArrayList()
                                            )
                                        binding!!.listView.layoutManager =
                                            LinearLayoutManager(this@NotificationActivity)
                                        binding!!.listView.adapter = adapter
                                    }
                                }

                            }
                        })
                }

                override fun onNegativeClick() {

                }

            })

    }

}
