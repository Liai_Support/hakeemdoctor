package com.app.hakeemDoctor.view.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.TextView.OnEditorActionListener
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityEnterNotesBinding
import com.app.hakeemDoctor.databinding.ActivitySocialLoginDetailsBinding
import com.app.hakeemDoctor.interfaces.SingleTapListener
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.view.adapter.LablistAdapter
import com.app.hakeemDoctor.view.adapter.TestlistAdapter
import com.app.hakeemDoctor.viewmodel.BookingViewModel
import org.json.JSONArray
import java.util.*
import kotlin.math.roundToInt


class EnterNotesActivity : BaseActivity() {
    var binding: ActivityEnterNotesBinding? = null

    var bookingViewModel: BookingViewModel? = null
    var bookingID = 0
    var bookingIDs :String=""
    var patientId = 0
    var textIn: AutoCompleteTextView? = null
    var textIn1: AutoCompleteTextView? = null

    var medicinebuttonAdd: Button? = null
    var container: LinearLayout? = null
    var reList: TextView? = null
    var testidarray: ArrayList<Int> = ArrayList()
    var labidarray: ArrayList<Int> = ArrayList()
    var info: TextView? = null
    var medicinearray: ArrayList<String> = ArrayList()
    var medicinedescarray: ArrayList<String> = ArrayList()
    private val NUMBER = arrayOf(
        "One", "Two", "Three", "Four", "Five",
        "Six", "Seven", "Eight", "Nine", "Ten"
    )
    var adapter: ArrayAdapter<String>? = null
    var adapter1: ArrayAdapter<String>? = null
    var settestcount: Int? = 0
    var testidjsonarray = JSONArray()
    var labidjsonarray = JSONArray()
    var medicinejsonarray = JSONArray()
    var medicinedescjsonarray = JSONArray()
     var issubmitted:Boolean=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEnterNotesBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_enter_notes)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)


        binding!!.comment.setOnEditorActionListener(
            OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event != null && event.action === KeyEvent.ACTION_DOWN && event.keyCode === KeyEvent.KEYCODE_ENTER) {
                    if (event == null || !event.isShiftPressed) {
                        // the user is done typing.
                        val imm: InputMethodManager = v.getContext()
                            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                        return@OnEditorActionListener true // consume.
                    }
                }
                false // pass on to other listeners.
            }
        )





        intent.extras?.let {
            bookingID = it.getInt(Constants.IntentKeys.BOOKINGID)
            patientId = it.getInt(Constants.IntentKeys.PATIENTID)
            bookingIDs = it.getString("bookingids")!!
        }

        adapter = ArrayAdapter(
            this,
            android.R.layout.simple_dropdown_item_1line, NUMBER
        )

        textIn = findViewById<View>(R.id.textin) as AutoCompleteTextView
        textIn!!.setAdapter<ArrayAdapter<String>>(adapter)
        textIn1 = findViewById<View>(R.id.textin1) as AutoCompleteTextView
        textIn1!!.setAdapter<ArrayAdapter<String>>(adapter)

        medicinebuttonAdd = findViewById<View>(R.id.add) as Button
        container = findViewById<View>(R.id.container) as LinearLayout
        // reList = findViewById<View>(R.id.relist) as TextView
        // reList!!.movementMethod = ScrollingMovementMethod()
        // info = findViewById<View>(R.id.info) as TextView
        // info!!.movementMethod = ScrollingMovementMethod()
        medicinebuttonAdd!!.setOnClickListener {
            if (textIn!!.text.toString().equals("") || textIn1!!.text.toString().equals("")) {

            } else {
                medicinearray.clear()
                medicinedescarray.clear()
                val layoutInflater =
                    baseContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val addView = layoutInflater.inflate(R.layout.row, null)
                val textOut = addView.findViewById<View>(R.id.textout) as AutoCompleteTextView
                val textOut1 = addView.findViewById<View>(R.id.textout1) as AutoCompleteTextView
                textOut.setAdapter<ArrayAdapter<String>>(adapter)
                textOut1.setAdapter<ArrayAdapter<String>>(adapter1)
                textOut.setText(textIn!!.text.toString())
                textOut1.setText(textIn1!!.text.toString())

                textIn!!.text.clear()
                textIn1!!.text.clear()
                val buttonRemove = addView.findViewById<View>(R.id.remove) as Button
                val buttonEdit = addView.findViewById<View>(R.id.edit) as Button
                val thisListener: View.OnClickListener = object : View.OnClickListener {
                    override fun onClick(v: View) {
                        medicinearray.clear()
                        medicinedescarray.clear()
                        //   info!!.append("thisListener called:\t$this\n")
                        //  info!!.append("Remove addView: $addView\n\n")
                        (addView.parent as LinearLayout).removeView(addView)
                        listAllMedicineAddView()
                    }
                }
                buttonRemove.setOnClickListener(thisListener)
                val thisListener1: View.OnClickListener = object : View.OnClickListener {
                    override fun onClick(v: View) {
                        if (buttonEdit.text.equals("Edit")) {
                            textOut.isEnabled = true
                            buttonEdit.text = "Save"
                        } else {
                            buttonEdit.text = "Edit"
                            textOut.isEnabled = false
                            medicinearray.clear()
                            medicinedescarray.clear()
                            //   info!!.append("thisListener called:\t$this\n")
                            //  info!!.append("Remove addView: $addView\n\n")
                            //(addView.parent as LinearLayout).removeView(addView)
                            listAllMedicineAddView()
                        }
                    }
                }
                buttonEdit.setOnClickListener(thisListener1)
                container!!.addView(addView)
                // info!!.append("""thisListener:$thisListener addView:$addView""".trimIndent())
                listAllMedicineAddView()
            }
        }

        binding!!.medicineTxt.setOnClickListener {
            if (binding!!.mediLinear.visibility == View.VISIBLE) {
                binding!!.mediLinear.visibility = View.GONE
            } else {
                binding!!.mediLinear.visibility = View.VISIBLE
            }
        }

        binding!!.labTxt.setOnClickListener {
            if (binding!!.labLin.visibility == View.VISIBLE) {
                binding!!.labLin.visibility = View.GONE
            } else {
                binding!!.labLin.visibility = View.VISIBLE
            }
        }


        gettestlist()

        binding!!.getlablist.setOnClickListener(View.OnClickListener { view ->
            if (testidarray.size > 0) {
                labidarray.clear()
                DialogUtils.showLoader(this)
                testidjsonarray = JSONArray()
                for (i in 0 until testidarray.size) {
                    testidjsonarray.put(testidarray[i])
                }
                bookingViewModel?.getlablist(this, testidjsonarray, bookingID)
                    ?.observe(this, Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        binding!!.labList.visibility = View.GONE
                                        binding!!.labtxt.visibility = View.GONE
                                        UiUtils.showSnack(binding!!.rootNotes, msg)
                                    }
                                } else {
                                    it.data?.let { data ->
                                        data?.let { list ->
                                            if (list.isEmpty()) {
                                                binding!!.labList.visibility = View.GONE
                                                binding!!.labtxt.visibility = View.GONE
                                                UiUtils.showSnack(
                                                    binding!!.rootNotes,
                                                    getString(R.string.labnotavailable)
                                                )
                                            } else {
                                                binding!!.labList.visibility = View.VISIBLE
                                                binding!!.labtxt.visibility = View.VISIBLE
                                                settestcount = 0
                                                binding!!.labList.layoutManager =
                                                    LinearLayoutManager(this)
                                                binding!!.labList.adapter = LablistAdapter(
                                                    this@EnterNotesActivity,
                                                    this,
                                                    list
                                                )

                                            }
                                        }

                                    }

                                }
                            }


                        }

                    })
            } else {
                UiUtils.showSnack(binding!!.rootNotes, getString(R.string.pleasechoosetestfirst))
            }
        })


    }

    private fun gettestlist() {
        DialogUtils.showLoader(this)
        bookingViewModel?.gettestlistDetails(this)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.rootNotes, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data?.let { list ->
                                    binding!!.testList.layoutManager = LinearLayoutManager(this)
                                    binding!!.testList.adapter =
                                        TestlistAdapter(this@EnterNotesActivity, this, list)

                                }

                            }
                        }


                    }

                }
            })

    }


    fun onSubmitClicked(view: View) {

        if (binding!!.comment.text.toString().trim()
                .isNotEmpty() && medicinearray.size > 0 && testidarray.size > 0 && labidarray.size > 0 && testidarray.size == settestcount
        ) {
            addnote()
        } else if (testidarray.size > 0 && labidarray.size == 0) {
            DialogUtils.showAlertHeader(this, object : SingleTapListener {
                override fun singleTap() {

                }
            }, getString(R.string.oops), getString(R.string.pleasechooselab))
        } else if (binding!!.comment.text.toString().equals("")) {
            DialogUtils.showAlertHeader(this, object : SingleTapListener {
                override fun singleTap() {

                }
            }, getString(R.string.oops), getString(R.string.please_enter_notes))
        } else if (binding!!.comment.text.toString().trim().isNotEmpty() && testidarray.size == 0) {
            showAlertHeaderr(this, object : SingleTapListener {


                override fun singleTap() {
                    addnote()

                }
            }, getString(R.string.oops), getString(R.string.areyousurecontinuewithoutlab))
        } else if (testidarray.size != settestcount) {
            UiUtils.showSnack(binding!!.rootNotes, "choose lab correctly")
        }
    }

   fun onCancelClicked(view: View){
        DialogUtils.showSuccessDialog(this, object : SingleTapListener {
            override fun singleTap() {
             //   finish()

                val intent = Intent(this@EnterNotesActivity, DashBoardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }
        }, getString(R.string.thanks_for_your_consultation))

    }




    fun addnote() {
        DialogUtils.showLoader(this)
        testidjsonarray = JSONArray()
        medicinejsonarray = JSONArray()
        medicinedescjsonarray = JSONArray()
        labidjsonarray = JSONArray()
        for (i in 0 until testidarray.size) {
            testidjsonarray.put(testidarray[i])
        }
        for (i in 0 until medicinearray.size) {
            medicinejsonarray.put(medicinearray[i])
        }
        for (i in 0 until medicinedescarray.size) {
            medicinedescjsonarray.put(medicinedescarray[i])
        }
        for (i in 0 until labidarray.size) {
            labidjsonarray.put(labidarray[i])
        }

        bookingViewModel?.addNotes(
            this,
            bookingIDs,
            bookingID,
            patientId,
            binding!!.comment.text.toString().trim(),
            medicinejsonarray,
            medicinedescjsonarray,
            testidjsonarray,
            labidjsonarray
        )
            ?.observe(this,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.rootNotes, msg)
                                }
                            } else {

                                DialogUtils.showSuccessDialog(this, object : SingleTapListener {
                                    override fun singleTap() {
                                       // finish()


                                        val intent = Intent(this@EnterNotesActivity, DashBoardActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                        finish()
                                    }
                                }, getString(R.string.thanks_for_your_consultation))


                            }
                        }
                    }
                })
    }

    private fun listAllMedicineAddView() {
        // reList!!.text = ""
        val childCount = container!!.childCount
        for (i in 0 until childCount) {
            val thisChild = container!!.getChildAt(i)
            //  reList!!.append("""$thisChild""".trimIndent())
            val childTextView = thisChild.findViewById<View>(R.id.textout) as AutoCompleteTextView
            val childTextView1 = thisChild.findViewById<View>(R.id.textout1) as AutoCompleteTextView
            childTextView.setText(childTextView.text.toString())
            childTextView1.setText(childTextView1.text.toString())
            val no = thisChild.findViewById<View>(R.id.no) as TextView
            no.setTextColor(Color.WHITE)
            no.setText("" + (i + 1) + ")")
            val childTextViewValue = childTextView.text.toString()
            // reList!!.append("= $childTextViewValue\n")

            medicinearray.add(childTextView!!.text.toString())
            medicinedescarray.add(childTextView1!!.text.toString())

        }

    }


  /*  override fun onBackPressed() {

        if (issubmitted==true){

            Log.d("sxdfvgb","sderftgyh")
        }else{
            DialogUtils.showAlertHeader(this, object : SingleTapListener {
                override fun singleTap() {

                }
            }, getString(R.string.oops), getString(R.string.beforeleavingthepagepleasesubmitit))
        }


    }*/


    fun showAlertHeaderr(
        context: Context,
        singleTapListener: SingleTapListener,
        headerString: String,
        content: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var contentView = dialog.findViewById<TextView>(R.id.content)
        var header = dialog.findViewById<TextView>(R.id.header)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content
        header.text = headerString

        ok.setOnClickListener {
            addnote()
            dialog.dismiss()
        }

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        dialog.show()

    }

}