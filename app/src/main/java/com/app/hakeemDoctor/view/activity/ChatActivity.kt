package com.app.hakeemDoctor.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityChatBinding
import com.app.hakeemDoctor.models.ChatData
import com.app.hakeemDoctor.network.UrlHelper
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.SharedHelper
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.view.adapter.ChatAdapter
import com.app.hakeemDoctor.viewmodel.CallViewModel
import io.socket.client.Ack
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import java.net.URISyntaxException

class ChatActivity : BaseActivity() {

    var binding: ActivityChatBinding? = null
    var viewmodel: CallViewModel? = null
    var sharedHelper = SharedHelper(this)
    private var socket: Socket? = null
    private var reciverId = ""
    private var docImage = ""
    private var Bookingid = ""

    var data: ArrayList<ChatData> = ArrayList()
    var chatAdapter: ChatAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_chat)
        viewmodel = ViewModelProvider(this).get(CallViewModel::class.java)
        getIntentValues()
        setAdapter()
        initListener()

    }

    override fun onResume() {
        super.onResume()
        initSockets()
        initobserver(reciverId)
    }

    private fun setAdapter() {
        chatAdapter = ChatAdapter(this, data, sharedHelper.userImage, docImage)
        binding!!.chatList.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, true)
        binding!!.chatList.adapter = chatAdapter
    }

    private fun initListener() {

        UiUtils.loadImage(
            binding!!.userImage,
            sharedHelper.userImage,
            ContextCompat.getDrawable(this, R.drawable.place_holder_doctor)!!
        )

        binding!!.sendMessage.setOnClickListener {
            if (binding!!.message.text.toString().trim().isNotEmpty()) {

                sendTextMessage(binding!!.message.text.toString().trim())

            }
        }
    }

    private fun sendTextMessage(msg: String) {

        var time: String = System.currentTimeMillis().toString()
        Log.d("knaika",""+sharedHelper.id)
        Log.d("knaika",""+reciverId)
        Log.d("knaika",""+msg)
        Log.d("knaika",""+time)
        Log.d("knaika",""+Bookingid)
        var jsonObject = JSONObject()
        jsonObject.put(Constants.SocketKey.SENDERID, sharedHelper.id)
        jsonObject.put(Constants.SocketKey.RECIVERID, reciverId)
        jsonObject.put(Constants.SocketKey.CONTENT, msg)
        jsonObject.put(Constants.SocketKey.CONTENTTYPE, "text")
        jsonObject.put(Constants.SocketKey.TIME, time)
        jsonObject.put(Constants.SocketKey.SENDERTYPE, "provider")
        jsonObject.put(Constants.SocketKey.Bookingid, Bookingid)

        Log.d("kanikashanmugam",""+jsonObject)

        socket?.emit(UrlHelper.SENDMESSAGE, jsonObject, object : Ack {
            override fun call(vararg args: Any?) {

            }
        })

        var chatdata = ChatData()
        chatdata.id = sharedHelper.id
        chatdata.content = msg
        chatdata.contentType = "text"
        chatdata.receiverID = reciverId
        chatdata.senderID = sharedHelper.id
        chatdata.senderType = "provider"
        chatdata.time = time

        data.add(0, chatdata)
        binding!!.message.setText("")

        chatAdapter?.notifydataSet()

    }


    private fun getIntentValues() {
        intent.extras?.let {


            reciverId = it.getString(Constants.IntentKeys.DOCTORSID).toString()
            Log.d("bnnbbnnbnbnb","")
            Log.d("bvvvvb",""+reciverId)
            Bookingid = it.getInt(Constants.IntentKeys.BOOKINGID).toString()

            /*    if (it.getString(Constants.IntentKeys.BOOKINGID)!=null){
                    Bookingid = it.get(Constants.IntentKeys.BOOKINGID)!!

                }else{
                    Log.d("bnnbbnnbnbnb",""+it.getInt(Constants.IntentKeys.BOOKINGID))

                }*/

            binding!!.docName.text = it.getString(Constants.IntentKeys.DOCTORNAME)
            it.getString(Constants.IntentKeys.DOCTORIMAGE)?.let { value ->
                docImage = value
                UiUtils.loadImage(
                    binding!!.doctorImage,
                    value,
                    ContextCompat.getDrawable(this, R.drawable.place_holder_doctor)!!
                )
            }

        }
    }

    private fun initobserver(id: String?) {
        id?.let {
            viewmodel?.getChatDetails(it,Bookingid)?.observe(this, Observer { value ->

                if (value.error == "true") {

                    UiUtils.showSnack(findViewById(android.R.id.content), value.message)

                } else if (value.error == "false") {

                    data.clear()
                    data.addAll(value.data.reversed())
                    chatAdapter?.notifydataSet()

                }

            })


        }

    }

    private fun initSockets() {
        val opts = IO.Options()
        opts.forceNew = true
        opts.reconnection = false
        try {
            socket = IO.socket(UrlHelper.SOCKETURL, opts)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        socket?.on(Socket.EVENT_DISCONNECT) {
            Log.d(" Socket ", " DisConnected")
            initSockets()
        }

        socket?.on(Socket.EVENT_CONNECT) {
            Log.d(" Socket ", " Connected")
//            initSockets()
        }

        socket?.let {
            if (!it.connected())
                socket?.connect()
        }


        var jsonObject = JSONObject()
        jsonObject.put(Constants.SocketKey.ID, sharedHelper.id)
        jsonObject.put(Constants.SocketKey.TYPE, "providers")

        socket?.emit(UrlHelper.GET_ONLINE, jsonObject, object : Ack {
            override fun call(vararg args: Any?) {

            }
        })

        socket?.on(UrlHelper.GET_ONLINE + "_providers_" + sharedHelper.id + "_ack") {
            UiUtils.showLog(" listener ", it[0].toString())
        }


        var socletListner = UrlHelper.RECIVE_MESSAGE + "_" + sharedHelper.id

        socket?.on(UrlHelper.RECIVE_MESSAGE) {

            UiUtils.showLog(" REcivedMessage ", it[0].toString())


            runOnUiThread {
                val chatDataValue = JSONObject(it[0].toString())

                var chatdata = ChatData()
                chatdata.id = sharedHelper.id
                chatdata.content = chatDataValue.optString("content")
                chatdata.contentType = chatDataValue.optString("content_type")
                chatdata.receiverID = chatDataValue.optString("receiverID")
                chatdata.senderID = sharedHelper.id
                chatdata.senderType = chatDataValue.optString("senderType")
                chatdata.time = chatDataValue.optString("time")

                data.add(0, chatdata)
                chatAdapter?.notifydataSet()
            }


        }


    }


    fun onBackClicked(view: View) {
        onBackPressed()
    }

    override fun onBackPressed() {

       /* val intent =
            Intent(this, DashBoardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)*/
        finish()
    }


}