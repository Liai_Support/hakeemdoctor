package com.app.hakeemDoctor.view.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.models.ChatData
import com.app.hakeemDoctor.utils.BaseUtils
import com.app.hakeemDoctor.utils.UiUtils


class ChatAdapter(val context: Context, var data: ArrayList<ChatData>, var userImage: String?, var docImage: String) : RecyclerView.Adapter<ChatAdapter.ChatViewHolder>() {

    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatAdapter.ChatViewHolder {
        return when (viewType) {
            0 -> {
                ChatViewHolder(LayoutInflater.from(context).inflate(R.layout.child_outgoing_message, parent, false))
            }
            1 -> {
                ChatViewHolder(LayoutInflater.from(context).inflate(R.layout.child_incoming_message, parent, false))
            }
            else -> {
                ChatViewHolder(LayoutInflater.from(context).inflate(R.layout.child_incoming_message, parent, false))
            }
        }
    }

    override fun onBindViewHolder(holder: ChatAdapter.ChatViewHolder, position: Int) {

//        if (data[position].contentType == "text")
        holder.message.text = data[position].content


        data[position].time?.let { value ->
            holder.time.text = BaseUtils.getTimeFromTimeStamp(value)
        }

        if (data[position].senderType.equals("provider", true)) {
            userImage?.let {
                UiUtils.loadImage(holder.image, userImage, ContextCompat.getDrawable(context, R.drawable.place_holder_doctor)!!)
            }
        } else {
            UiUtils.loadImage(holder.image, docImage, ContextCompat.getDrawable(context, R.drawable.place_holder_doctor)!!)
        }


        setAnimation(holder.itemView, position, getItemViewType(position))


    }


    override fun onViewDetachedFromWindow(holder: ChatViewHolder) {
        holder.clearAnimation()
    }


    inner class ChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var message: TextView = itemView.findViewById(R.id.message)
        var time: TextView = itemView.findViewById(R.id.time)
        var image: ImageView = itemView.findViewById(R.id.image)

        fun clearAnimation() {
            itemView.clearAnimation()
        }

    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position].senderType == "provider") {
            0
        } else {
            if (data[position].contentType == "text") {
                1
            } else {
                2
            }
        }
    }

    fun notifydataSet() {
        notifyDataSetChanged()
    }


    private fun setAnimation(viewToAnimate: View, position: Int, itemViewType: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        val animation: Animation
        if (position > lastPosition) {
            animation = if (itemViewType == 0) {
                AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
            } else {
                AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            }
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}