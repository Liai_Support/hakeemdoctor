package com.app.hakeemDoctor.view.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ChildNotificationBinding
import com.app.hakeemDoctor.models.NotificationListData
import com.app.hakeemDoctor.utils.BaseUtils
import com.app.hakeemDoctor.view.activity.DashBoardActivity

import java.util.*

class NotificationAdapter(
    var context: Activity,
    var list: ArrayList<NotificationListData>
) :
    RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildNotificationBinding = ChildNotificationBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_notification,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.header.text = list[position].body
        holder.binding.content.text = list[position].title

        holder.binding.date.text =
            BaseUtils.getFormatedDate(list[position].createdAt!!, "yyyy-MM-dd'T'HH:mm:ss","dd/MM/yyyy hh:mm a")

        holder.binding.root.setOnClickListener {
            if(list[position].type == "new_booking"){
                val intent = Intent(context, DashBoardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(intent)
                context.finish()
            }else{
                context.finish()
            }
        }
    }

}