package com.app.hakeemDoctor.view.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.barchart.utils.YearXAxisFormatter
import com.app.hakeemDoctor.databinding.FragmentAppoinmentApprovalListBinding
import com.app.hakeemDoctor.databinding.FragmentCalenderBinding
import com.app.hakeemDoctor.interfaces.OnClickListener
import com.app.hakeemDoctor.models.BookingData
import com.app.hakeemDoctor.models.StatData
import com.app.hakeemDoctor.utils.*
import com.app.hakeemDoctor.view.activity.CancelationReasonActivity
import com.app.hakeemDoctor.view.activity.ChatActivity
import com.app.hakeemDoctor.view.activity.DashBoardActivity
import com.app.hakeemDoctor.view.activity.DoctorTrackingActivity
import com.app.hakeemDoctor.view.adapter.AppoinmentListAdapter
import com.app.hakeemDoctor.viewmodel.BookingViewModel
import com.app.hakeemUser.rxbus.RxBusNotification
import com.google.android.gms.location.*
import com.paypal.android.sdk.payments.PayPalConfiguration
import io.reactivex.disposables.Disposable
import java.util.ArrayList

class CalenderFragment : Fragment(R.layout.fragment_calender) {

    var binding: FragmentCalenderBinding? = null

    var viewModel: BookingViewModel? = null

    var adapter: AppoinmentListAdapter? = null

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null

    private var myLat: Double? = null
    private var myLng: Double? = null

    private var dateApi = ""
    private lateinit var notificationEventListner: Disposable

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCalenderBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())

        locationListner()
        askLocationPermission()
        dateApi = BaseUtils.getCurrentDate("yyyy-MM-dd")
        initListener()
        listernNotification()
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // isEnabled = false
                // activity?.onBackPressed()
                (activity as DashBoardActivity?)?.setAdapter(0)
            }
        })

        return  view
    }

    override fun onResume() {
        super.onResume()
        getDetails()
    }

    private fun listernNotification() {

        notificationEventListner =
            RxBusNotification.listen(String::class.java).doOnError {
            }.subscribe {
                ThreadUtils.runOnUiThread {
                    dateApi = BaseUtils.getCurrentDate("yyyy-MM-dd")
                    getDetails()
                }
            }
    }


    private fun initListener() {

        binding!!.calender.setOnDayClickListener {
            dateApi = BaseUtils.getFormatCurrentDate("yyyy-MM-dd", it.calendar.time)
            getDetails()

        }
    }

    private fun getDetails() {

        DialogUtils.showLoader(requireContext())
        viewModel?.getBookingList(requireContext(), dateApi)?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data)




                            Log.d("fdgvfvgfvgfvgffrr", "shanmuudhhduhdsu")
                        }
                    }
                }

            }
        })

    }

    private fun handleResponse(data: ArrayList<BookingData>) {

        adapter = AppoinmentListAdapter(requireContext(), myLat, myLng, object : OnClickListener {
            override fun onClickItem(position: Int) {


                if (data[position].status.equals("completed", true) ||
                    data[position].status.equals("cancelledbyprovider", true) ||
                    data[position].status.equals("cancelledbypatient", true) ||
                    data[position].status.equals("paymentverified", true)
                ) {

                } else {

                    startActivity(
                        Intent(requireContext(), DoctorTrackingActivity::class.java)
                            .putExtra(
                                Constants.IntentKeys.BOOKINGID,
                                data[position].bookingId
                            ).putExtra(
                                "bookingids",
                                data[position].bookingIds
                            )
                            .putExtra(
                                Constants.IntentKeys.PATIENTID,
                                data[position].patientId
                            )
                    )
                }

            }

        }, data, object : OnClickListener {
            override fun onClickItem(position: Int) {

                cancelbooking(position, data)

            }

        }, object : OnClickListener {
            override fun onClickItem(position: Int) {

                startActivity(
                    Intent(requireActivity(), ChatActivity::class.java)
                        .putExtra(Constants.IntentKeys.DOCTORSID, data[position].patientId)
                        .putExtra(
                            Constants.IntentKeys.DOCTORNAME,
                            data[position].patientName
                        )
                        .putExtra(
                            Constants.IntentKeys.DOCTORIMAGE,
                            data[position].profilePic
                        )
                )
            }

        })

        binding!!.scheduledAppoinment.layoutManager = LinearLayoutManager(requireContext())
        binding!!.scheduledAppoinment.adapter = adapter

    }

    private fun cancelbooking(position: Int, data: ArrayList<BookingData>) {

        startActivity(
            Intent(requireContext(), CancelationReasonActivity::class.java)
                .putExtra(Constants.IntentKeys.BOOKINGID, data[position].bookingId)
        )

    }

    fun onBackPressed(view: View) {
        //onBackPressed()
    }


    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()

                    }
                }
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireActivity().checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }


    private fun getUserLocation() {
        GpsUtils(requireActivity()).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {

        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {

                myLat = location.latitude
                myLng = location.longitude

                adapter?.setLatLng(myLat!!, myLng!!)

            } else {

                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )

            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestCode.GPS_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                getLastKnownLocation()
        }
    }

}