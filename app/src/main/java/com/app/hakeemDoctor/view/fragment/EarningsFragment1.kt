package com.app.hakeemDoctor.view.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.barchart.utils.YearXAxisFormatter
import com.app.hakeemDoctor.databinding.FragmentAppoinmentApprovalListBinding
import com.app.hakeemDoctor.databinding.FragmentEarnings1Binding
import com.app.hakeemDoctor.interfaces.ExtraFeeCallBack
import com.app.hakeemDoctor.models.StatData
import com.app.hakeemDoctor.models.TransactionData
import com.app.hakeemDoctor.utils.*
import com.app.hakeemDoctor.view.activity.DashBoardActivity
import com.app.hakeemDoctor.view.adapter.TransactionAdapter
import com.app.hakeemDoctor.viewmodel.BookingViewModel
import com.paypal.android.sdk.payments.*
import org.json.JSONException
import org.json.JSONObject
import java.math.BigDecimal
import java.util.*
import kotlin.collections.ArrayList

class EarningsFragment1 : Fragment(R.layout.fragment_earnings1) {

    var binding: FragmentEarnings1Binding? = null
    var sharedHelper: SharedHelper? = null
    private var xAxisFormatter: YearXAxisFormatter? = null

    var bookingViewModel: BookingViewModel? = null
    var chartData: ArrayList<StatData>? = null
    var config: PayPalConfiguration? = null
    var paymentAmount = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentEarnings1Binding.inflate(inflater, container, false)
        val view = binding!!.root
        sharedHelper = SharedHelper(requireContext())
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // isEnabled = false
                // activity?.onBackPressed()
                (activity as DashBoardActivity?)?.setAdapter(0)
            }
        })
        getTransaction()
        config = PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(getString(R.string.paypal_client_id))

        startServicePaypal()
        binding!!.cardView3.setOnClickListener(){
            onSendMoneyClicked(view)
        }
        return view
    }

    private fun startServicePaypal() {

        /*val intent = Intent (getActivity(), PayPalService::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        getActivity()?.startActivity(intent)*/

        val intent = Intent(getActivity(), PayPalService::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        getActivity()?.startService(intent)

    }

    private fun processPayment(amount: Int) {

        val payPalPayment = PayPalPayment(
            BigDecimal(amount.toString()), "USD",
            "Purchase Goods", PayPalPayment.PAYMENT_INTENT_SALE
        )

        val intent = Intent(getActivity(), PaymentActivity::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment)
        startActivityForResult(intent, Constants.RequestCode.PAYPAL_REQUEST_CODE)
    }

    override fun onDestroy() {
        super.onDestroy()
       // stopService(Intent(getActivity(), PayPalService::class.java))
    }


    private fun getTransaction() {
        DialogUtils.showLoader(requireContext())
        bookingViewModel?.getTransactionList(requireContext())?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data)
                        }
                    }
                }
            }
        })
    }

    private fun handleResponse(data: TransactionData) {

        data.availableCredits?.let { credits ->
            binding!!.availableCredits.text = "$credits SAR"
        }

        data.availableEarnings?.let { earnings ->
            binding!!.yourShare.text = "$earnings SAR"
        }



        data.history?.let {
            var adapter = getActivity()?.let { it1 -> TransactionAdapter(it1, it) }
            binding!!.transactionList.layoutManager = LinearLayoutManager(getActivity())
            binding!!.transactionList.adapter = adapter
        }

    }

    fun onBackPressed(view: View) {
       // onBackPressed()
    }

    fun onSendMoneyClicked(view: View) {

        getActivity()?.let {
            DialogUtils.showSendMoneyDialog(
                it,
                resources.getString(R.string.ok),
                resources.getString(R.string.cancel),
                object : ExtraFeeCallBack {
                    override fun onPositiveClick(value: Int) {
                        paymentAmount = value
                        processPayment(value)
                    }

                    override fun onNegativeClick() {

                    }

                })
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestCode.PAYPAL_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.let {
                val confirmation: PaymentConfirmation =
                    it.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)!!
                if (confirmation != null) {
                    try {
                        val paymentDetails = confirmation.toJSONObject().toString(4)

                        try {
                            var paymentDeatils: JSONObject =
                                JSONObject(paymentDetails).get("response") as JSONObject
                            if (paymentDeatils.get("state").toString().equals("approved", true)) {
                                payAdmin(paymentDeatils.get("id").toString())
                            }
                        } catch (e: Exception) {

                        }


                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        }

    }

    private fun payAdmin(tnx_id: String) {

        DialogUtils.showLoader(requireContext())
        bookingViewModel?.payAdmin(requireContext(), paymentAmount, tnx_id)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        getTransaction()
                    }
                }
            }
        })

    }



}

