package com.app.hakeemDoctor.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.FragmentEarnings1Binding
import com.app.hakeemDoctor.interfaces.ExtraFeeCallBack
import com.app.hakeemDoctor.models.TransactionData
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.SharedHelper
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.view.adapter.TransactionAdapter
import com.app.hakeemDoctor.viewmodel.BookingViewModel
import com.paypal.android.sdk.payments.*
import org.json.JSONException
import org.json.JSONObject
import java.math.BigDecimal

class EarningsActivity : BaseActivity() {

    var binding: FragmentEarnings1Binding? = null
    var sharedHelper: SharedHelper? = null

    var bookingViewModel: BookingViewModel? = null
    var config: PayPalConfiguration? = null
    var paymentAmount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentEarnings1Binding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.fragment_earnings1)
        sharedHelper = SharedHelper(this)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)

        getTransaction()
        config = PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(getString(R.string.paypal_client_id))

        startServicePaypal()
    }

    private fun startServicePaypal() {

        val intent = Intent(this, PayPalService::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        startService(intent)

    }

    private fun processPayment(amount: Int) {

        val payPalPayment = PayPalPayment(
            BigDecimal(amount.toString()), "USD",
            "Purchase Goods", PayPalPayment.PAYMENT_INTENT_SALE
        )

        val intent = Intent(this, PaymentActivity::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment)
        startActivityForResult(intent, Constants.RequestCode.PAYPAL_REQUEST_CODE)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopService(Intent(this, PayPalService::class.java))
    }


    private fun getTransaction() {

        DialogUtils.showLoader(this)
        bookingViewModel?.getTransactionList(this)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data)
                        }
                    }
                }
            }
        })
    }

    private fun handleResponse(data: TransactionData) {

        data.availableCredits?.let { credits ->
            binding!!.availableCredits.text = "$credits SAR"
        }

        data.availableEarnings?.let { earnings ->
            binding!!.yourShare.text = "$earnings SAR"
        }



        data.history?.let {
            var adapter = TransactionAdapter(this, it)
            binding!!.transactionList.layoutManager = LinearLayoutManager(this)
            binding!!.transactionList.adapter = adapter
        }

    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    fun onSendMoneyClicked(view: View) {

        DialogUtils.showSendMoneyDialog(
            this,
            resources.getString(R.string.ok),
            resources.getString(R.string.cancel),
            object : ExtraFeeCallBack {
                override fun onPositiveClick(value: Int) {
                    paymentAmount = value
                    processPayment(value)
                }

                override fun onNegativeClick() {

                }

            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestCode.PAYPAL_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.let {
                val confirmation: PaymentConfirmation =
                    it.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)!!
                if (confirmation != null) {
                    try {
                        val paymentDetails = confirmation.toJSONObject().toString(4)

                        try {
                            var paymentDeatils: JSONObject =
                                JSONObject(paymentDetails).get("response") as JSONObject
                            if (paymentDeatils.get("state").toString().equals("approved", true)) {
                                payAdmin(paymentDeatils.get("id").toString())
                            }
                        } catch (e: Exception) {

                        }


                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        }

    }

    private fun payAdmin(tnx_id: String) {

        DialogUtils.showLoader(this)
        bookingViewModel?.payAdmin(this, paymentAmount, tnx_id)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        getTransaction()
                    }
                }
            }
        })

    }

}