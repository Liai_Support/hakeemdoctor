package com.app.hakeemDoctor.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ChildTransactionBinding
import com.app.hakeemDoctor.databinding.ChildTransaction1Binding
import com.app.hakeemDoctor.models.TransactionListData
import com.app.hakeemDoctor.utils.BaseUtils

class TransactionAdapter(
    var context: Context,
    var transactionListData: ArrayList<TransactionListData>
) : RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    inner class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = ChildTransaction1Binding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(
            LayoutInflater.from(context).inflate(R.layout.child_transaction1, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return transactionListData.size
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {

        transactionListData[position].id?.let {
            holder.binding.id.text = "ID : $it"
        }

        transactionListData[position].amount?.let {
            if (transactionListData[position].type == "credit") {
                holder.binding.amount.text = "+$it SAR"
                holder.binding.amount.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorPrimary
                    )
                )
            } else {
                holder.binding.amount.text = "-$it SAR"
                holder.binding.amount.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.google_color
                    )
                )
            }

        }

        transactionListData[position].createdAt?.let {
            holder.binding.date.text =
                BaseUtils.getFormatedDateUtc(it, "yyyy-MM-dd'T'HH:mm:ss", "dd/MM/yyyy hh:mm a")
        }

    }


}