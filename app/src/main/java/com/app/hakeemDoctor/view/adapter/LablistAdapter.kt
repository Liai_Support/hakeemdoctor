package com.app.hakeemDoctor.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ChildTestListBinding
import com.app.hakeemDoctor.models.LabData
import com.app.hakeemDoctor.models.TestDetails
import com.app.hakeemDoctor.view.activity.EnterNotesActivity
import java.util.*

class LablistAdapter(var enterNotesActivity: EnterNotesActivity, var context: Context, var list: ArrayList<LabData>) : RecyclerView.Adapter<LablistAdapter.MyViweHolder>() {

    var count: Int? = 0
    var testnamearray: ArrayList<String> = ArrayList()
    var ha: String? = ""

    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {
        var childPatientListBinding: ChildTestListBinding = ChildTestListBinding.bind(view)

    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int): LablistAdapter.MyViweHolder {

        return MyViweHolder(
                LayoutInflater.from(context).inflate(R.layout.child_test_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: LablistAdapter.MyViweHolder, position: Int) {
    /*    Log.d("hcv",""+enterNotesActivity.testidarray)
        count = 0
        for (i in 0 until enterNotesActivity.testidarray.size) {
            for (j in 0 until list[position].available_test!!.size){
                Log.d("fdhfvv",""+enterNotesActivity.testidarray[i]+"==="+list[position].available_test!![j].test_name_id)
                if(enterNotesActivity.testidarray[i] == list[position].available_test!![j].test_name_id){
                    holder.childPatientListBinding.test.text = list[position].lab_name+"("+list[position].available_test!![j].Test_name
                    count = count!!+1
                    testnamearray.add(list[position].available_test!![j].Test_name!!)
                }
            }
        }
        Log.d("djbhv",""+count)
        ha = ""
        for(i in 0 until testnamearray.size){
            var h = i+1
            if(h == testnamearray.size){
                ha = ha+testnamearray[i]
            }
            else{
                ha = ha+testnamearray[i]+","
            }
        }
        holder.childPatientListBinding.test.text = list[position].lab_name+"("+ha+")"
*/

    /*    holder.childPatientListBinding.test.setOnClickListener {

            if(holder.childPatientListBinding.test.isChecked == true){
                holder.childPatientListBinding.test.isChecked = true
                enterNotesActivity.labidarray.add(list[position].id!!)
                enterNotesActivity.settestcount = enterNotesActivity.settestcount!! + list[position].matched_test!!.size
            }
            else if(holder.childPatientListBinding.test.isChecked == false){
                holder.childPatientListBinding.test.isChecked = false
                enterNotesActivity.labidarray.remove(list[position].id!!)
                enterNotesActivity.settestcount = enterNotesActivity.settestcount!! - list[position].matched_test!!.size
            }

            Log.d("ngv",""+enterNotesActivity.labidarray)
            Log.d("ndddgv",""+enterNotesActivity.settestcount)





        }*/

        holder.childPatientListBinding.test.setChecked(list.get(position).isseleted!!)

        holder.childPatientListBinding.test.setTag(position);

        holder.childPatientListBinding.test.setOnClickListener(View.OnClickListener {
            val pos = holder.childPatientListBinding.test.getTag() as Int
            // Toast.makeText(context, list.get(pos).ename.toString() + " clicked!", Toast.LENGTH_SHORT).show()
            if (list.get(pos).isseleted!!) {
                list.get(pos).isseleted = false
                enterNotesActivity.labidarray.remove(list[position].id!!)
                enterNotesActivity.settestcount = enterNotesActivity.settestcount!! - list[position].matched_test!!.size
            }
            else {
                list.get(pos).isseleted = true
                enterNotesActivity.labidarray.add(list[position].id!!)
                enterNotesActivity.settestcount = enterNotesActivity.settestcount!! + list[position].matched_test!!.size
            }



        })

        testnamearray.clear()
        for (i in 0 until list[position].matched_test!!.size) {
            testnamearray.add(list[position].matched_test!![i].Test_name!!)
        }

        ha = ""
        for(i in 0 until testnamearray.size){
            var h = i+1
            if(h == testnamearray.size){
                ha = ha+testnamearray[i]
            }
            else{
                ha = ha+testnamearray[i]+","
            }
        }

        holder.childPatientListBinding.test.text = list[position].lab_name+"("+ha+")"

    }



}








