package com.app.hakeemDoctor.view.activity

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.background.LocationEmitter
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityTrackDoctorBinding
import com.app.hakeemDoctor.interfaces.DialogCallBack
import com.app.hakeemDoctor.interfaces.ExtraFeeCallBack
import com.app.hakeemDoctor.models.BookingData
import com.app.hakeemDoctor.network.UrlHelper
import com.app.hakeemDoctor.utils.*
import com.app.hakeemDoctor.viewmodel.BookingViewModel
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import java.net.URISyntaxException


class DoctorTrackingActivity : BaseActivity(), OnMapReadyCallback {

    var binding: ActivityTrackDoctorBinding? = null

    var bookingViewModel: BookingViewModel? = null

    private var currentRoute: ArrayList<LatLng> = ArrayList()
    private val expectedTextValue = ""
    private var map: GoogleMap? = null

    private var status = ""
    private var reachedDestination: String? = ""

    private var blackPolyline: Polyline? = null
    private var blackTrackingPolyline: Polyline? = null
    var animationHelper: AnimationHelper? = null
    var mapFragment: MapFragment? = null

    var bookingID: Int? = null
    var bookingIDs: String? = null
    var patientID: Int? = null
    var sharedHelper: SharedHelper? = null


    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null

    private var myLat: Double? = null
    private var myLng: Double? = null

    private var source: LatLng? = null
    private var destination: LatLng? = null

    var drawPolyLine = true

    var myVehicleMarker: Marker? = null

    private var trackingLat: Double? = null
    private var trackingLng: Double? = null

    var handler = Handler()
    lateinit var runnable: Runnable
    var socket: Socket? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrackDoctorBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_track_doctor)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        animationHelper = AnimationHelper()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mapFragment = fragmentManager.findFragmentById(R.id.map) as MapFragment
        mapFragment?.getMapAsync(this)
        sharedHelper = SharedHelper(this)
        initSockets()
        locationListner()
        askLocationPermission()


    }

    private fun initSockets() {
        val opts = IO.Options()
        opts.forceNew = true
        opts.reconnection = false
//        opts.query = Constants.ApiKeys.AUTHORIZATION + "=" + sharedHelper.token
        try {
            socket = IO.socket(UrlHelper.SOCKETURL, opts)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        socket?.on(Socket.EVENT_DISCONNECT) {
            Log.d(" Socket ", " DisConnected")
            initSockets()
        }

        socket?.on(Socket.EVENT_CONNECT) {
            Log.d(" Socket ", " Connected")
//            initSockets()
        }

        socket?.let {
            if (!it.connected())
                socket?.connect()
        }
    }

    private fun setTrackingMarker() {

        sharedHelper?.latitude?.let {
            trackingLat = sharedHelper?.latitude!!.toDouble()

            trackingLng = sharedHelper?.longitude!!.toDouble()
        }

        if (trackingLat != null && trackingLng != null) {
            val markerOptions = MarkerOptions()
            markerOptions.position(LatLng(trackingLat!!, trackingLng!!)).flat(true)
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.track))
            myVehicleMarker = map?.addMarker(markerOptions)
        }



        runnable = Runnable {

            sharedHelper?.latitude?.let {
                trackingLat = sharedHelper?.latitude!!.toDouble()
                trackingLng = sharedHelper?.longitude!!.toDouble()
            }
            if (trackingLat != null && trackingLng != null)
                animateMarker(LatLng(trackingLat!!, trackingLng!!))

            handler.postDelayed(runnable, 4000)

            var jsonObject = JSONObject()
            jsonObject.put(Constants.SocketKey.ID, sharedHelper?.id!!)
            jsonObject.put(
                Constants.SocketKey.LATUTUDE,
                BaseUtils.getRoundedOffSixDigits(sharedHelper?.latitude!!.toDouble(), "%6f")
            )
            jsonObject.put(
                Constants.SocketKey.LONGITUDE,
                BaseUtils.getRoundedOffSixDigits(sharedHelper?.longitude!!.toDouble(), "%6f")
            )

            socket?.emit(UrlHelper.UPDATEPROVIDERLOCATION, jsonObject)


        }


        handler.removeCallbacks(runnable)
        handler.postDelayed(runnable, 4000)


    }

    override fun onMapReady(map0: GoogleMap?) {

        map0?.let { map = map0 }
        getIntentValues()
        setTrackingMarker()

    }


    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (10 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()

                    }
                }
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }


    private fun getUserLocation() {
        GpsUtils(this).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }

    private fun getLastKnownLocation() {

        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {


                myLat = location.latitude
                myLng = location.longitude


                sharedHelper?.latitude = myLat.toString()
                sharedHelper?.longitude = myLng.toString()

                if (drawPolyLine)
                    getPolyLine()

                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return@addOnSuccessListener
                }
                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )

            } else {

                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )

            }
        }

    }


    private fun getIntentValues() {


        intent.extras?.let {
            getDetails(
                it.getInt(Constants.IntentKeys.BOOKINGID),
                it.getInt(Constants.IntentKeys.PATIENTID)
            )

            bookingID = it.getInt(Constants.IntentKeys.BOOKINGID)
            bookingIDs = it.getString("bookingids")

            Log.d("wertyuio",""+bookingIDs)
            patientID = it.getInt(Constants.IntentKeys.PATIENTID)
        }

    }

    private fun getDetails(bookingID: Int, patientId: Int) {

        DialogUtils.showLoader(this)
        bookingViewModel?.getBookingDetail(this, bookingID, patientId)?.observe(this, Observer {

            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            if (data.size != 0)
                                handleResponse(data, 0)
                        }
                    }
                }


            }


            sharedHelper?.latitude = myLat.toString()
            sharedHelper?.longitude = myLng.toString()
            sharedHelper?.bookingId = bookingID.toString()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(Intent(this, LocationEmitter::class.java))
            } else {
                startService(Intent(this, LocationEmitter::class.java))
            }

        })

    }

    private fun handleResponse(data: ArrayList<BookingData>, position: Int) {


        data[position].isFeatureBooking?.let { value ->
            if (value) {
                data[position].patientName?.let { binding!!.patientName.text = it }
              //  data[position].patientAge?.let { binding!!.yearsOld.text = "$it years old" }

                if (data[position].previousIssue.equals("")){
                    Log.d("erdrfdffg","wewewewrrrr1")

                    binding!!.healthIssue.text = getString(R.string.notmentioned)
                }else{
                    Log.d("erdrfdffg","wewewewrrrr2")

                    data[position].previousIssue?.let { binding!!.healthIssue.text = it }

                }
                binding!!.healthIssueText.visibility = View.VISIBLE
            } else {

                Log.d("erdrfdffg","wewewewrrrr3")
                binding!!.healthIssueText.visibility = View.VISIBLE
                 data[position].previousIssue?.let { binding!!.healthIssue.text = it }

                // binding!!.healthIssue.text = getString(R.string.notmentioned)
                data[position].name?.let { binding!!.patientName.text = it }
             /*   data[position].DOB?.let {
                    binding!!.yearsOld.text = "${BaseUtils.getAgeCalculation(it)} years old"
                }*/
            }
        }
    //  binding?.bookingid!!.text = " " + getString(R.string.bookingid) + " #book_" + data[position].bookingId.toString()
      binding?.bookingid1!!.text =" #book_" + data[position].bookingId.toString()

//        data[position].patientName?.let { patientName.text = it }
//        data[position].patientAge?.let { yearsOld.text = "$it years old" }

       /* data[position].mobileNumber?.let {
            binding!!.phoneNumber.text = "${data[position].countryCode} $it"
        }*/
    //    data[position].email?.let { binding!!.mailId.text = it }
        data[position].location?.let { binding!!.location.text = it }
        data[position].status?.let {
            status = it
            reachedDestination = data[position].reachedDestination
            when {
                status.equals("pending", true) -> {
                    binding!!.textButton.text = resources.getString(R.string.start_visit)
                }
                status.equals("startvisit", true) -> {
                    if (data[position].reachedDestination == "0") {
                        binding!!.textButton.text = resources.getString(R.string.destination_reached)
                    } else {
                        binding!!.textButton.text = resources.getString(R.string.end_trip)
                    }

                }
                else -> {
                    binding!!.textButton.text = resources.getString(R.string.visit_completed)
                }
            }
        }

        if (data[position].bookingLatitude != null && data[position].bookingLongitude != null)
            destination = LatLng(
                data[position].bookingLatitude?.toDouble()!!,
                data[position].bookingLongitude?.toDouble()!!
            )

//        data[position].distance?.let {
//            kmAway.text = BaseUtils.numberFormat(it.toDouble())
//        }

/*
        data[position].bookingDate?.let {
            binding!!.date.text = BaseUtils.getFormatedDate(it, "yyyy-MM-dd", "dd,MMMM,yyyy")
        }*/

        getPolyLine()

    }

    private fun getPolyLine() {

        if (myLng != null && myLat != null)
            source = LatLng(myLat!!, myLng!!)

        if (source != null && destination != null) {
            bookingViewModel?.getRoute(this, source!!, destination!!)
                ?.observe(this,
                    Observer {
                        currentRoute = ArrayList()
                        currentRoute.add(source!!)
                        currentRoute.addAll(it.points)
                        currentRoute.add(destination!!)
                        generateMarker(source!!, destination!!)
                        addRouteinMap(currentRoute, getLatLngBounds(source!!, destination!!))
                    })
        }
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    fun onVisitCompletedClicked(view: View) {

        if (status.equals("pending", true))
            updateBookingStartVisitStatus()
        else if (status.equals("startvisit", true)) {

            reachedDestination?.let {
                if (it == "0") {

                    updateBookingDestination()

                } else {

                  /*  DialogUtils.showExtraFeeDialog(this, getString(R.string.ok), getString(R.string.cancel), object : ExtraFeeCallBack {
                            override fun onPositiveClick(value: Int) {
                                DialogUtils.showAlertDialog(this@DoctorTrackingActivity,
                                    "You have charged extra fee amount $value SAR.Would like to confirm?",
                                    getString(R.string.confirm),
                                    getString(R.string.ok),
                                    getString(R.string.cancel)
                                    ,
                                    object : DialogCallBack {
                                        override fun onPositiveClick() {

                                            stopService(
                                                Intent(
                                                    this@DoctorTrackingActivity,
                                                    LocationEmitter::class.java
                                                )
                                            )
                                            updateBookingCompleteVisitStatus(value)
                                        }

                                        override fun onNegativeClick() {

                                        }

                                    })

                            }

                            override fun onNegativeClick() {

                            }
                        })*/

                    DialogUtils.showAlertDialog(this@DoctorTrackingActivity, "Are you sure to confirm?", getString(R.string.confirm), getString(R.string.ok), getString(R.string.cancel),object : DialogCallBack {
                        override fun onPositiveClick() {
                            stopService(
                                Intent(
                                    this@DoctorTrackingActivity,
                                    LocationEmitter::class.java
                                )
                            )
                            updateBookingCompleteVisitStatus(0)
                        }

                        override fun onNegativeClick() {

                        }

                    })

                }
            }


        }


    }

    private fun updateBookingDestination() {

        bookingID?.let { it ->
            DialogUtils.showLoader(this)
            bookingViewModel?.updateBookingDestination(this, it)
                ?.observe(this, Observer {

                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.root, msg)
                                }
                            } else {

                                reachedDestination = "1"
                                binding!!.textButton.setText(resources.getString(R.string.end_trip))

                            }
                        }


                    }

                })
        }
    }

    private fun updateBookingCompleteVisitStatus(value: Int) {
        bookingID?.let { it ->
            DialogUtils.showLoader(this)
            bookingViewModel?.updateBookingCompleteVisitStatus(this, it, value)
                ?.observe(this, Observer {

                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.root, msg)
                                }
                            } else {

                                startActivity(
                                    Intent(this, EnterNotesActivity::class.java)
                                        .putExtra(Constants.IntentKeys.BOOKINGID, bookingID!!)
                                        .putExtra("bookingids", bookingIDs!!)
                                        .putExtra(Constants.IntentKeys.PATIENTID, patientID!!)
                                )
                                finish()

                            }
                        }


                    }

                })
        }

    }

    private fun updateBookingStartVisitStatus() {

        bookingID?.let { it ->
            DialogUtils.showLoader(this)

            bookingViewModel?.updateBookingStartVisitStatus(this, it)?.observe(this, Observer {

                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            status = "startvisit"
                            reachedDestination = "0"
                            binding!!.textButton.setText(resources.getString(R.string.destination_reached))
                        }
                    }


                }

            })
        }
    }

    fun onCancelClicked(view: View) {
        finish()
    }

    private fun generateMarker(
        sourceLatLng: LatLng,
        destinationLatLng: LatLng
    ) {

        val markerOptions = MarkerOptions()
        markerOptions.title(sourceLatLng.latitude.toString() + " : " + sourceLatLng.longitude)

        map?.addMarker(
            MarkerOptions()
                .position(LatLng(sourceLatLng.latitude, sourceLatLng.longitude))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
        )

        map?.addMarker(
            MarkerOptions()
                .position(LatLng(destinationLatLng.latitude, destinationLatLng.longitude))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.destmarker))
        )


    }


    private fun addRouteinMap(
        route: List<LatLng>,
        bounds: LatLngBounds
    ) {

        drawPolyLine = false
        if (blackPolyline != null) {
            if (blackPolyline?.points?.size == 0) {
                generateBlackPolyline(route)
            } else {
                blackPolyline?.points = route
            }
        } else {
            generateBlackPolyline(route)
            drawPolyLine = false
        }
//        generateGreyPolyline(route)
        runOnUiThread {
            map?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 340))
        }
        animatePolyLine(route)
    }


    private fun animatePolyLine(route: List<LatLng>) {
//        animationHelper?.animatePolyLine(blackPolyline, route)
    }


    private fun generateBlackPolyline(route: List<LatLng>) {
        val lineOptions = PolylineOptions()
        lineOptions.width(9f)
        lineOptions.color(ContextCompat.getColor(this, R.color.colorPrimary))
        lineOptions.startCap(SquareCap())
        lineOptions.endCap(SquareCap())
        lineOptions.jointType(JointType.ROUND)
        lineOptions.addAll(route)
        blackPolyline = map?.addPolyline(lineOptions)
        blackPolyline?.zIndex = 2f
    }


    private fun getLatLngBounds(
        sourceLatLng: LatLng,
        destinationLatLng: LatLng
    ): LatLngBounds {
        val builder = LatLngBounds.Builder()
        builder.include(sourceLatLng)
        builder.include(destinationLatLng)
        return builder.build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestCode.GPS_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                getLastKnownLocation()
        }
    }

    fun animateMarker(toPosition: LatLng?) {

        myVehicleMarker?.let {
            val startPosition: LatLng = it.position
            val latLngInterpolator: AnimationHelper.LatLngInterpolatorNew =
                AnimationHelper.LatLngInterpolatorNew.LinearFixed()
            val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
            valueAnimator.duration = 1000 // duration 3 second
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction
                    val newPosition: LatLng =
                        latLngInterpolator.interpolate(v, startPosition, toPosition)
                    it.position = newPosition
                    it.setAnchor(0.5f, 0.5f)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                }
            })
            valueAnimator.start()
            //            }
        }
    }

}