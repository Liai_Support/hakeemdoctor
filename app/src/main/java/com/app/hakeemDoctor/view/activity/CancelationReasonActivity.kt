package com.app.hakeemDoctor.view.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityCancellationReasonBinding
import com.app.hakeemDoctor.interfaces.SingleTapListener
import com.app.hakeemDoctor.utils.Constants
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.viewmodel.BookingViewModel

class CancelationReasonActivity : BaseActivity() {

    var binding: ActivityCancellationReasonBinding? = null
    var bookingId: Int? = null
    var viewModel: BookingViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCancellationReasonBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_cancellation_reason)

        viewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        getIntentValues()
    }

    private fun getIntentValues() {

        intent.extras?.let {
            bookingId = it.getInt(Constants.IntentKeys.BOOKINGID)
        }

    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    fun onSubmitClicked(view: View) {

        if (binding!!.comment.text.toString().trim().isNotEmpty()) {

            DialogUtils.showLoader(this)
            viewModel?.cancelBooking(this, bookingId, binding!!.comment.text.toString().trim())
                ?.observe(this, Observer {
                    DialogUtils.dismissLoader()
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            finish()
                        }
                    }
                })
        } else {

            DialogUtils.showAlertHeader(this,object : SingleTapListener{
                override fun singleTap() {


                }

            },getString(R.string.oops),getString(R.string.cancel_reason))

        }
    }

}
