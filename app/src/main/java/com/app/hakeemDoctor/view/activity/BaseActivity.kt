package com.app.hakeemDoctor.view.activity

import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import androidx.appcompat.app.AppCompatActivity
import com.app.hakeemDoctor.utils.SharedHelper
import java.util.*

abstract class BaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setPhoneLanguage()
    }


    private fun setPhoneLanguage() {
        var sharedHelper = SharedHelper(this)
        val res = resources
        val conf = res.configuration
        val locale =
            Locale(sharedHelper?.language?.toLowerCase()!!)
        Locale.setDefault(locale)
        conf.setLocale(locale)
        applicationContext.createConfigurationContext(conf)
        val dm = res.displayMetrics
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(LocaleList(locale))
        } else {
            conf.locale = locale
        }
        res.updateConfiguration(conf, dm)
    }
}