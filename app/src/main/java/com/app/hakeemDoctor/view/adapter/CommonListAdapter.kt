package com.app.hakeemDoctor.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ChildCommonListBinding
import com.app.hakeemDoctor.interfaces.OnClickListener

class CommonListAdapter(
    var context: Context,
    var list: ArrayList<String>,
    var listener: OnClickListener
) :
    RecyclerView.Adapter<CommonListAdapter.HomeHeaderViewHolder>() {

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildCommonListBinding = ChildCommonListBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_common_list,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        holder.binding.text.text = list[position]

        holder.binding.root.setOnClickListener {
            listener.onClickItem(position)
        }

    }
}