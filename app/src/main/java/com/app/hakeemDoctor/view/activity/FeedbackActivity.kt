package com.app.hakeemDoctor.view.activity

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemDoctor.R
import com.app.hakeemDoctor.databinding.ActivityCalenderBinding
import com.app.hakeemDoctor.databinding.ActivityFeedbackBinding
import com.app.hakeemDoctor.models.ReviewData
import com.app.hakeemDoctor.utils.DialogUtils
import com.app.hakeemDoctor.utils.UiUtils
import com.app.hakeemDoctor.view.adapter.FeedbackAdapter
import com.app.hakeemDoctor.viewmodel.BookingViewModel
import java.util.*

class FeedbackActivity : BaseActivity() {

    var binding: ActivityFeedbackBinding? = null

    var viewmodel: BookingViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFeedbackBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        viewmodel = ViewModelProvider(this).get(BookingViewModel::class.java)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback)
        initObservers()
    }

    private fun initObservers() {
        DialogUtils.showLoader(this)
        viewmodel?.getRating(this)?.observe(this, Observer {

            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            if (data.size != 0)
                                handleResponse(data)
                        }
                    }
                }


            }

        })
    }

    private fun handleResponse(data: ArrayList<ReviewData>) {
        var adapter = FeedbackAdapter(this,data)
        binding?.feedbackListView?.layoutManager = LinearLayoutManager(this)
        binding?.feedbackListView?.adapter = adapter

    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

}